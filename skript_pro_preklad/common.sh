#!/bin/bash
# Where the .java sources are
SRC="./src"
# Where the .class files will be
BIN="./build"
# Libraries
CLASSPATH="lib/activation.jar:lib/jaxb-api.jar:lib/jaxb-impl.jar:jsr173_1.0_api.jar:lib/jre1.5/charsets.jar:lib/jre1.5/javaws.jar:lib/jre1.5/jsse.jar:lib/jre1.5/rt.jar:lib/jre1.5/deploy.jar:lib/jre1.5/jce.jar:lib/jre1.5/plugin.jar"
