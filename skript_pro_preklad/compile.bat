@echo off

:init
  set ACTUAL_DIR=%CD%
  cd ..
  set COMPILE_DIR=.\build
  set SOURCE_DIR=.\src
  set RESOURCES_DIR=.\resources
  set MAN_DIR=.\man
  set LIB_DIR=.\lib
  set SOURCE_ENCODING="utf-8"

:compile
  echo Removing %COMPILE_DIR%\...
  rmdir /S /Q %COMPILE_DIR%

  echo Search java sources in %SOURCE_DIR%\...
  dir %SOURCE_DIR%\*.java /B/S > javasrc.tmp~
  if ERRORLEVEL 1 (
    echo Cannot find Java source files in %SOURCE_DIR%\
    goto abort
  )
  
  echo Search jar libraries in %LIB_DIR%\...
  if exist %LIB_DIR% (
    for %%J IN (%LIB_DIR%\*.jar) do (
      echo|set /p=-cp %LIB_DIR%\%%~nJ.jar >> javalib.tmp~
    )  
  )
   
  echo Compile in %COMPILE_DIR%\...
  if exist %COMPILE_DIR% rmdir /S /Q %COMPILE_DIR%
  mkdir %COMPILE_DIR%
  
  if exist %LIB_DIR% (
    echo on
    @"javac.exe" -d %COMPILE_DIR% -encoding %SOURCE_ENCODING% @javalib.tmp~ @javasrc.tmp~
    @echo off
  ) else (
    echo on
    @"javac.exe" -d %COMPILE_DIR% -encoding %SOURCE_ENCODING% @javasrc.tmp~
    @echo off
  )
  
:resources
  echo Copy resources from %RESOURCES_DIR%\... to %COMPILE_DIR%\%RESOURCES_DIR%\...
  xcopy /Y /I /S %RESOURCES_DIR%\*  %COMPILE_DIR%\%RESOURCES_DIR%

:mans
  echo Copy manuals from %MAN_DIR%\... to %COMPILE_DIR%\%MAN_DIR%\...
  xcopy /Y /I /F %MAN_DIR%\*  %COMPILE_DIR%\%MAN_DIR%

:abort
  del javasrc.tmp~
  if exist %LIB_DIR% del javalib.tmp~

:destroy
  set COMPILE_DIR=
  set SOURCE_DIR=
  set RESOURCES_DIR=
  set MAN_DIR=
  set LIB_DIR=
  set SOURCE_ENCODING=

:end
  cd %ACTUAL_DIR%
  set ACTUAL_DIR=   
  echo Done.
  echo on