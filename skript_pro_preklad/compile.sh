#!/bin/bash
# Load common variables
. ./skript_pro_preklad/common.sh
# Compile
rm -rf $BIN
echo "Cleaning folder $BIN"
mkdir $BIN
find $SRC -name *.java > sources_list.txt
echo "Compiling sources"
javac -d $BIN -classpath $CLASSPATH @sources_list.txt
rm sources_list.txt
echo "Done."
