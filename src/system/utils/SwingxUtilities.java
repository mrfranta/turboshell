/** This Java file was created 1.2.2012 22:32:46. */
package system.utils;

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.MenuComponent;
import java.awt.MenuContainer;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.MenuElement;
import javax.swing.SwingUtilities;

/**
 * Class {@code SwingxUtilities} contains extended functions for 
 * <b>Swing or AWT GUI</b>. This utilities provides methods for centering and 
 * resizing <code>Windows</code>, resizing <code>Components</code>. Also enables 
 * Antialiasing for graphics object. At least provides methods for getting 
 * <code>Window</code> ancestor and searching <code>(Menu)Components</code> in 
 * <code>Window</code>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.1.6
 */
public class SwingxUtilities {
    /** Constant for canvas which serves for getting font metrics of fonts. */
    private static final Canvas CANVAS = new Canvas();
    /** Constant for default toolkit. */
    public static final Toolkit TOOLKIT = Toolkit.getDefaultToolkit();
    /** Constant for screen size. */
    public static final Dimension SCREEN_SIZE = SwingxUtilities.TOOLKIT.getScreenSize();

    /**
     * Private empty constructor serves for deny instancing this class, because 
     * this class is &quot;static&quot;.
     */
    private SwingxUtilities() {
    }

    /**
     * Returns font metrics of specified font using default toolkit.
     *
     * @param font font for which will returns font metrics.
     * @return Font metrics for specified font.
     */
    public static FontMetrics getFontMetrics(final Font font) {
        return SwingxUtilities.CANVAS.getFontMetrics(font);
    }

    /**
     * Sets position of <code>window</code> to center of screen.
     *
     * @param window window for centering.
     */
    public static void centerWindow(final Window window) {
        SwingxUtilities.centerWindow(window, null);
    }

    /**
     * Sets position of <code>window</code> to center of <code>owner</code>.
     *
     * @param window window for centering.
     * @param owner owner for centering window.
     * @throws NullPointerException if the entered window is {@code null}.
     */
    public static void centerWindow(final Window window, final Window owner) {
        if (window == null) {
            throw new NullPointerException("Undefined window");
        }

        Dimension size = window.getSize();

        int x = 0;
        int y = 0;

        if (owner == null) {
            x = (SwingxUtilities.SCREEN_SIZE.width - size.width) / 2;
            y = (SwingxUtilities.SCREEN_SIZE.height - size.height) / 2;

            window.setLocation(x, y);

            return;
        }

        Point point = owner.getLocation();
        Dimension parentSize = owner.getSize();

        x = point.x + (parentSize.width - size.width) / 2;
        y = point.y + (parentSize.height - size.height) / 2;

        window.setLocation(x, y);
    }

    /**
     * Sets size of <code>Window</code> to screen size.
     *
     * @param window <code>Window</code> to resize.
     * @throws NullPointerException if the entered window is {@code null}.
     */
    public static void fitToScreen(final Window window) {
        if (window == null) {
            throw new NullPointerException("Undefined window");
        }

        Dimension size = window.getSize();
        Point pos = window.getLocation();

        double width = size.getWidth();
        double screenW = SwingxUtilities.SCREEN_SIZE.getWidth();
        if (width > screenW) {
            width = screenW;
            pos.x = 0;
        }

        double height = size.getHeight();
        double screenH = SwingxUtilities.SCREEN_SIZE.getHeight();
        if (height > screenH) {
            height = screenH;
            pos.y = 0;
        }

        size.setSize(width, height);
        window.setSize(size);

        double endX = pos.x + width;
        if (endX > screenW) {
            pos.x -= (int) (pos.x + width - screenW);
        }

        double endY = pos.y + height;
        if (endY > screenH) {
            pos.y -= (int) (pos.y + height - screenH);
        }

        window.setLocation(pos.x, pos.y);
    }

    /**
     * Sets <code>Window</code> size to screen size with same scale for width 
     * and height. If component is square, then after resize will be still be 
     * square.
     *
     * @param window <code>Window</code> to resize.
     * @throws NullPointerException if the entered window is {@code null}.
     */
    public static void fitToScreenEqually(final Window window) {
        if (window == null) {
            throw new NullPointerException("Undefined window");
        }

        Dimension size = window.getSize();
        Point pos = window.getLocation();

        double width = size.getWidth();
        double height = size.getHeight();

        double screenW = SwingxUtilities.SCREEN_SIZE.getWidth();
        double screenH = SwingxUtilities.SCREEN_SIZE.getHeight();

        double scaleW = 1.0;
        if (width > screenW) {
            scaleW = (screenW / width);
        }

        width = width * scaleW;
        height = height * scaleW;

        double scaleH = 1.0;
        if (height > screenH) {
            scaleH = (screenH / height);
        }

        width = width * scaleH;
        height = height * scaleH;

        size.setSize(width, height);
        window.setSize(size);

        double endX = pos.x + width;
        if (endX > screenW) {
            pos.x -= (int) (pos.x + width - screenW);
        }

        double endY = pos.y + height;
        if (endY > screenH) {
            pos.y -= (int) (pos.y + height - screenH);
        }

        window.setLocation(pos.x, pos.y);
    }

    /**
     * Enables or disables antialiasing in specified graphics object for shapes.
     *
     * @param g2 graphics object to enable antialiasing for shapes.
     * @param enable {@code true} for enable antialiasing or {@code false} for 
     * disable antialiasing.
     */
    public static void setEnableAntialiasingForShapes(final Graphics2D g2,
                                                      final boolean enable) {
        Object value = RenderingHints.VALUE_ANTIALIAS_OFF;
        if (enable) {
            value = RenderingHints.VALUE_ANTIALIAS_ON;
        }

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, value);
    }

    /**
     * Enables of disables antialiasing in specified graphics object for text.
     *
     * @param g2 graphics object to enable antialiasing for text.
     * @param enable {@code true} for enable antialiasing or {@code false} for 
     * disable antialiasing.
     */
    public static void setEnableAntialiasingForText(final Graphics2D g2,
                                                    final boolean enable) {
        Object value = RenderingHints.VALUE_TEXT_ANTIALIAS_OFF;
        if (enable) {
            value = RenderingHints.VALUE_TEXT_ANTIALIAS_ON;
        }

        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, value);
    }

    /**
     * Enables or disables antialiasing in specified graphics object for shapes 
     * and for text.
     *
     * @param g2 graphics object to enable antialiasing.
     * @param enable {@code true} for enable antialiasing or {@code false} for 
     * disable antialiasing.
     */
    public static void setEnableAntialiasing(final Graphics2D g2,
                                             final boolean enable) {
        SwingxUtilities.setEnableAntialiasingForShapes(g2, enable);
        SwingxUtilities.setEnableAntialiasingForText(g2, enable);
    }

    /**
     * Searches recursively tree of components in entered 
     * <code>Container</code> for <code>Component</code> with entered name.
     *
     * @param owner container for search.
     * @param componentName component name for search.
     * @return <code>Component</code> with specified name or {@code null}.
     */
    private static Component getComponentByName(final Container owner,
                                                final String componentName) {
        if (owner.getName() != null) {
            if (owner.getName().equalsIgnoreCase(componentName)) {
                return owner;
            }
        }

        if (owner instanceof MenuElement) {
            MenuElement menuElement = (MenuElement) owner;
            MenuElement[] elements = menuElement.getSubElements();

            for (MenuElement element : elements) {
                Component comp = element.getComponent();

                if (comp.getName() != null) {
                    if (comp.getName().equalsIgnoreCase(componentName)) {
                        return comp;
                    }
                }

                if (comp instanceof Container) {
                    Container cont = (Container) comp;
                    Component finded = SwingxUtilities.getComponentByName(cont,
                            componentName);

                    if (finded != null) {
                        return finded;
                    }
                }
            }

            return null;
        }

        Component[] comps = owner.getComponents();
        for (Component comp : comps) {
            if (comp.getName() != null) {
                if (comp.getName().equalsIgnoreCase(componentName)) {
                    return comp;
                }
            }

            if (comp instanceof Container) {
                Container cont = (Container) comp;
                Component finded = SwingxUtilities.getComponentByName(cont,
                        componentName);

                if (finded != null) {
                    return finded;
                }
            }
        }

        return null;
    }

    /**
    
     * If are used names of components in GUI, it will find this component in 
     * components tree and returns this component. Is better use unique names 
     * of component for correct work this method. For set and get component 
     * name are used methods {@link java.awt.MenuComponent#setName(String name)} 
     * and {@link java.awt.MenuComponent#getName()}.
     *
     * @param menuComp important parameter for searching components, because 
     * this method will get window ancestor of this menu component and then 
     * search in component tree for component with specified name.
     * @param componentName component name for search.
     * @return Founded component in Window ancestor of <code>menuComp</code> or 
     * {@code null} (if can't find component).
     * 
     * @throws NullPointerException if the entered component name is 
     * {@code null}.
     * @throws IllegalArgumentException if the entered component name is empty.
     */
    public static Component getComponentByName(final MenuComponent menuComp,
                                               final String componentName) {
        if (menuComp == null) {
            return null;
        }

        if (componentName == null) {
            throw new NullPointerException("Undefined component name");
        }

        if (StringUtilities.isEmpty(componentName)) {
            throw new IllegalArgumentException("Component name is empty");
        }

        Window owner = SwingxUtilities.getWindowAncestor(menuComp);
        if (owner != null) {
            SwingxUtilities.getComponentByName(owner, componentName);
        }

        return null;
    }

    /**
     * If are used names of components in GUI, it will find this component in 
     * components tree and returns this component. Is better use unique names 
     * of component for correct work this method. For set and get component 
     * name are used methods {@link java.awt.Component#setName(String name)} and 
     * {@link java.awt.Component#getName()}.
     *
     * @param comp important parameter for searching components, because this 
     * method will get window ancestor of this component and then search in 
     * component tree for component with specified name.
     * @param componentName component name for search.
     * @return Founded component in Window ancestor of <code>comp</code> or 
     * {@code null} if can't find component.
     * 
     * @throws NullPointerException if the entered component name is 
     * {@code null}.
     * @throws IllegalArgumentException if the entered component name is empty.
     */
    public static Component getComponentByName(final Component comp,
                                               final String componentName) {
        if (comp == null) {
            return null;
        }

        if (componentName == null) {
            throw new NullPointerException("Undefined component name");
        }

        if (StringUtilities.isEmpty(componentName)) {
            throw new IllegalArgumentException("Component name is empty");
        }

        Window owner = SwingxUtilities.getWindowAncestor(comp);

        if (owner != null) {
            return SwingxUtilities.getComponentByName(owner, componentName);
        }

        return null;
    }

    /**
     * Returns the first <code>Window</code> ancestor of <code>menuComp</code>, 
     * or {@code null} if <code>c</code> is not contained inside a 
     * <code>Window</code>.
     *
     * @param menuComp <code>MenuComponent</code> to get <code>Window</code> 
     * ancestor of.
     * @return the first <code>Window</code> ancestor of <code>menuComp</code>, 
     * or {@code null} if <code>menuComp</code> is not contained inside a 
     * <code>Window</code>.
     */
    public static Window getWindowAncestor(final MenuComponent menuComp) {
        if (menuComp == null) {
            return null;
        }

        MenuContainer cont = menuComp.getParent();
        while (cont != null) {
            if (cont instanceof Component) {
                Component comp = (Component) cont;

                return SwingxUtilities.getWindowAncestor(comp);
            }

            if (cont instanceof MenuComponent) {
                MenuComponent comp = (MenuComponent) cont;
                cont = comp.getParent();

                continue;
            }

            return null;
        }

        return null;
    }

    /**
     * Returns the first <code>Window</code> ancestor of <code>comp</code>, or 
     * {@code null} if <code>c</code> is not contained inside a 
     * <code>Window</code>.
     *
     * @param comp <code>Component</code> to get <code>Window</code> ancestor of.
     * @return the first <code>Window</code> ancestor of <code>comp</code>, or
     * {@code null} if <code>comp</code> is not contained inside a 
     * <code>Window</code>.
     */
    public static Window getWindowAncestor(final Component comp) {
        if (comp == null) {
            return null;
        }

        Window owner = null;

        if (comp instanceof Window) {
            owner = (Window) comp;
        }
        else {
            if (comp instanceof JMenuItem && !(comp instanceof JMenu)) {
                JMenuItem menuItem = (JMenuItem) comp;

                Container cont = menuItem.getParent();
                if (cont instanceof JPopupMenu) {
                    JPopupMenu popupMenu = (JPopupMenu) menuItem.getParent();
                    Component invoker = popupMenu.getInvoker();
                    owner = SwingUtilities.getWindowAncestor(invoker);
                }
                else {
                    owner = SwingUtilities.getWindowAncestor(cont);
                }
            }
            else {
                owner = SwingUtilities.getWindowAncestor(comp);
            }
        }

        return owner;
    }
}
