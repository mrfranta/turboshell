/** This Java file was created 15.5.2012 18:56:55. */
package system.utils;

import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.swing.ImageIcon;

/**
 * Class {@code ResourcesLoader} is static class for loading files from JAR 
 * file, project or unpacked program. This class can load InputStream of XSD 
 * schema for Validator, Image and ImageIcon for GUI, buffered images, wav 
 * files as clips and TrueType fonts.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.6
 * 
 * @since 0.1.2
 */
public class ResourcesLoader {
    /** Path to loading resources from JAR file. */
    public static final String JAR_RESOURCES_PATH = "/resources/";
    /** Path to loading resources from project or unpacked program. */
    public static final String RESOURCES_PATH = "resources/";

    /** Path to loading manuals from JAR file. */
    public static final String JAR_MANUALS_PATH = "/man/";
    /** Path to loading manuals from project or unpacked program. */
    public static final String MANUALS_PATH = "man/";

    /** Name of directory for XSD schemas. */
    public static final String XSD_DIR = "xsd/";
    /** Name of directory for XML files. */
    public static final String XML_DIR = "xml/";
    /** Name of directory for image icons. */
    public static final String ICONS_DIR = "icons/";
    /** Name of directory for images. */
    public static final String IMAGES_DIR = "img/";
    /** Name of directory for sounds. */
    public static final String SOUNDS_DIR = "sounds/";
    /** Name of directory for fonts. */
    public static final String FONTS_DIR = "fonts/";

    /**
     * Private empty constructor serves for deny instancing this class, because 
     * this class is &quot;static&quot;.
     */
    private ResourcesLoader() {
    }

    /**
     * Loads resources from JAR file, project or unpacked program.
     *
     * @param path specified path to loading resource.
     * @return If resources was loaded successful, returns URL to resource, 
     * otherwise null.
     */
    public static URL getResource(final String path) {
        URL resource = ResourcesLoader.class.getResource(ResourcesLoader.JAR_RESOURCES_PATH + path);
        if (resource == null) {
            try {
                File file = new File(ResourcesLoader.RESOURCES_PATH + path);
                if (!file.exists()) {
                    return null;
                }

                return file.toURI().toURL();
            } catch (IOException exc) {
                return null;
            }
        }

        return resource;
    }

    /**
     * Creates URL to manual file.
     *
     * @param path specified path to to the manual file.
     * @return If resources was loaded successful, returns URL to resource, 
     * otherwise {@code null}.
     */
    public static URL getManual(final String path) {
        URL source = ResourcesLoader.class.getResource(ResourcesLoader.JAR_MANUALS_PATH + path);
        if (source == null) {
            try {
                File file = new File(ResourcesLoader.MANUALS_PATH + path);
                if (!file.exists()) {
                    return null;
                }

                return file.toURI().toURL();
            } catch (IOException exc) {
                return null;
            }
        }

        return source;
    }

    /**
     * Loads XSD template and open InputStream for validator.
     *
     * @param xsd name of XSD file to load.
     * @return InputStream to loaded XSD template or {@code null} if some 
     * problem occurs.
     */
    public static InputStream getXSD(final String xsd) {
        URL resource = ResourcesLoader.getResource(ResourcesLoader.XSD_DIR + xsd);
        if (resource == null) {
            return null;
        }

        try {
            return resource.openStream();
        } catch (IOException exc) {
            return null;
        }
    }

    /**
     * Loads XML file and open InputStream.
     *
     * @param xml name of XML file to load.
     * @return InputStream of loaded XML file or {@code null} if some problem 
     * occurs.
     */
    public static InputStream getXML(final String xml) {
        URL resource = ResourcesLoader.getResource(ResourcesLoader.XML_DIR + xml);
        if (resource == null) {
            return null;
        }

        try {
            return resource.openStream();
        } catch (IOException exc) {
            return null;
        }
    }

    /**
     * Loads image icon.
     *
     * @param icon name of icon file to load.
     * @param title title for image icon class.
     * @return Loaded image icon or {@code null} if some problem occurs.
     */
    public static ImageIcon getIcon(final String icon, final String title) {
        URL iconURL = ResourcesLoader.getResource(ResourcesLoader.ICONS_DIR + icon);

        if (iconURL == null) {
            return null;
        }

        return new ImageIcon(iconURL, title);
    }

    /**
     * Loads icon and returns image of loaded icon.
     *
     * @param image name of image file to load.
     * @param title title for image icon class.
     * @return Loaded image or {@code null} if some problem occurs.
     */
    public static Image getImage(final String image, final String title) {
        ImageIcon icon = ResourcesLoader.getIcon(image, title);

        if (icon == null) {
            return null;
        }

        return icon.getImage();
    }

    /**
     * Loads buffered image.
     *
     * @param image name of image file to load.
     * @return Loaded buffered image or {@code null} if some problem occurs.
     */
    public static BufferedImage getImage(final String image) {
        URL imageURL = ResourcesLoader.getResource(ResourcesLoader.IMAGES_DIR + image);

        if (imageURL == null) {
            return null;
        }

        try {
            return ImageIO.read(imageURL);
        } catch (IOException exc) {
            return null;
        }
    }

    /**
     * Loads wav file as clip.
     *
     * @param clip name of wav (clip) file to load.
     * @return Loaded wav file or {@code null} if some problem occurs.
     * 
     * @see <a href="http://en.wikipedia.org/wiki/WAV">WAV file</a>
     */
    public static Clip getClip(final String clip) {
        URL clipURL = ResourcesLoader.getResource(ResourcesLoader.SOUNDS_DIR + clip);

        if (clipURL == null) {
            return null;
        }

        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(clipURL);
            AudioFormat format = stream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);

            Clip loadedClip = (Clip) AudioSystem.getLine(info);
            loadedClip.open(stream);

            return loadedClip;
        } catch (Exception exc) {
            return null;
        }
    }

    /**
     * Loads TrueType font file as new Font.
     *
     * @param font name of font file to load.
     * @return Loaded Font from TrueType font file.
     * 
     * @see <a href="http://en.wikipedia.org/wiki/TrueType">TrueType font</a>
     */
    public static Font getTrueTypeFont(final String font) {
        URL fontURL = ResourcesLoader.getResource(ResourcesLoader.FONTS_DIR + font);

        if (fontURL == null) {
            return null;
        }

        try {
            Font loaded = Font.createFont(Font.TRUETYPE_FONT,
                    fontURL.openStream());

            return loaded;
        } catch (Exception exc) {
            return null;
        }
    }
}
