/**
 * This Java file was created 20.11.2013 23:08:58.
 */
package system.utils;

/**
 * Class {@code StringUtilities} provides utility methods for working with 
 * Strings.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.5
 * 
 * @since 0.5.0
 */
public class StringUtilities {
    /**
     * Private empty constructor serves for deny instancing this class, because 
     * this class is &quot;static&quot;.
     */
    private StringUtilities() {
    }

    /**
     * Returns string which contains n-times repeated string.
     *
     * @param str string which will be repeated.
     * @param times number of string repetitions.
     * @return {@code null} whether the {@code str} is {@code null}; empty 
     * string whether {@code times} is <= 0; string containing n-times repeated 
     * string otherwise. 
     */
    public static String repeat(final String str, final int times) {
        if (str == null) {
            return null;
        }

        if (times <= 0) {
            return "";
        }

        StringBuffer sb = new StringBuffer(str.length() * times);
        for (int i = 0; i < times; i++) {
            sb.append(str);
        }

        return sb.toString();
    }

    /**
     * Returns information whether is entered string empty. This function is 
     * important for Java version 1.5 where class {@code String} doesn't have 
     * method {@code isEmpty()}. Returns <tt>true</tt> if {@link String#length()} 
     * of entered string is <tt>0</tt>. Also returns <tt>true</tt> whether 
     * entered string is {@code null}.
     *
     * @param str string for which determines whether it is empty.
     * 
     * @return <tt>true</tt> if {@link String#length()} of entered string is 
     * <tt>0</tt>. Also returns <tt>true</tt> whether entered string is 
     * {@code null}.
     */
    public static boolean isEmpty(final String str) {
        if (str == null) {
            return true;
        }

        return str.length() == 0;
    }
}
