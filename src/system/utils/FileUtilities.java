/**
 * This Java file was created 4.12.2013 1:57:32.
 */
package system.utils;

import java.io.File;

import system.turboshell.VMM;

/**
 * Class {@code FileUtilities} provides utility methods for working with 
 * File descriptors.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.7.6
 */
public class FileUtilities {
    /**
     * Private empty constructor serves for deny instancing this class, because 
     * this class is &quot;static&quot;.
     */
    private FileUtilities() {
    }

    /**
     * Returns string value of entered file in right form.
     *
     * @param file file descriptor.
     * @return String value of entered file in right form.
     */
    public static String toString(final File file) {
        if (file == null) {
            return null;
        }

        String path = file.getPath();
        path = path.replaceAll("[/\\\\]", VMM.getProperty("file.separator"));

        return path;
    }

    /**
     * Returns string value of entered file path in right form.
     *
     * @param filePath string representing file path.
     * @return String value of entered file path in right form.
     */
    public static String toString(final String filePath) {
        if (filePath == null) {
            return null;
        }

        return filePath.replaceAll("[/\\\\]", VMM.getProperty("file.separator"));
    }
}
