/**
 * Package {@code system.automaton} contains implementation of finite automaton 
 * which can be loaded from XML.
 */
package system.automaton;

