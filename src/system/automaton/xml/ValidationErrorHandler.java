package system.automaton.xml;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Class {@code ValidationErrorHandler} is error handler for validation 
 * using SAX.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.1.5
 */
public class ValidationErrorHandler implements ErrorHandler {
    /**
     * Generates exception for warnings, errors and fatal errors.
     *
     * @param errorType type of error.
     * @param exc received exception.
     * @throws SAXException which is response to error.
     */
    private void generateError(final String errorType,
                               final SAXException exc) throws SAXException {
        SAXException error = new SAXException(errorType + ": "
                + exc.getMessage());
        error.initCause(exc);

        throw error;
    }

    /**
     * Receives notification of a error.
     *
     * @param exc warning parse exception.
     * @throws SAXException which responses to parsing error.
     */
    // JDK1.5 compatible @Override
    public void error(final SAXParseException exc) throws SAXException {
        this.generateError("Error", exc);
    }

    /**
     * Receives notification of a fatal error.
     *
     * @param exc warning parse exception.
     * @throws SAXException which responses to parsing fatal error.
     */
    // JDK1.5 compatible @Override
    public void fatalError(final SAXParseException exc) throws SAXException {
        this.generateError("Fatal error", exc);
    }

    /**
     * Receives notification of a warning.
     *
     * @param exc warning parse exception.
     * @throws SAXException which responses to parsing warning.
     */
    // JDK1.5 compatible @Override
    public void warning(final SAXParseException exc) throws SAXException {
        this.generateError("Warning", exc);
    }
}
