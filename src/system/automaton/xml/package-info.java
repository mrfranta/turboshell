/**
 * Package {@code system.automaton.xml} contains classes for work with XML 
 * files which contains configuration of finite automaton.
 */
package system.automaton.xml;

