/**
 * This Java file was created 24.2.2013 17:04:44.
 */
package system.automaton.xml;

/**
 * Class {@code XMLException} represents exception for work with XML files.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.1
 * 
 * @since 0.1.5
 */
public class XMLException extends Exception {
    /** 
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a href=
     * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
     * details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 2L;

    /** 
     * Constructs XML exception with specified message.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     */
    public XMLException(final String message) {
        super(message);
    }

    /** 
     * Constructs XML exception with specified cause.
     *
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method). (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public XMLException(final Throwable cause) {
        super(cause);
    }

    /** 
     * Constructs XML exception with specified message and cause.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method).  (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public XMLException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
