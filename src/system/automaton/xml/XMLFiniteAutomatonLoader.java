/**
 * This Java file was created 19.10.2013 1:18:09.
 */
package system.automaton.xml;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import system.automaton.ErrorState;
import system.automaton.FiniteAutomaton;
import system.automaton.FiniteAutomatonLoader;
import system.automaton.RegularExpressionTransition;
import system.automaton.State;
import system.automaton.StateListener;
import system.automaton.Transition;
import system.automaton.TransitionListener;
import system.automaton.jaxb.AutomatonType;
import system.automaton.jaxb.ErrorStateType;
import system.automaton.jaxb.ListenerType;
import system.automaton.jaxb.ObjectFactory;
import system.automaton.jaxb.SelfTransitionType;
import system.automaton.jaxb.StateType;
import system.automaton.jaxb.TransitionType;
import system.automaton.jaxb.TransitionsType;
import system.utils.ResourcesLoader;

/**
 * Class {@code XMLFiniteAutomatonLoader} serves for loading finite automaton 
 * from XML files which contains configuration of finite automaton using JAXB 
 * technology.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.1.5
 */
public class XMLFiniteAutomatonLoader implements FiniteAutomatonLoader {
    /** Validator for XML validation. */
    private final Validator validator;

    /** Name of the package which contains classes that work with JAXB XML file. */
    public static final String JAXB_PACKAGE = ObjectFactory.class.getPackage().getName();
    /** 
     * Name of the XSD file that contains definition of XML files which 
     * contains configuration of finite automaton.
     */
    public static final String XSD_FILE = "AutomatonConfiguration.xsd";

    /**
     * Constructs loader for finite automaton from XML files.
     *
     * @throws SAXException if an error occurs while loading the XSD file or creating a 
     * XML validator.
     */
    public XMLFiniteAutomatonLoader() throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(
                XMLConstants.W3C_XML_SCHEMA_NS_URI);
        // associate the schema factory with the resource resolver, which is
        // responsible for resolving the imported XSD's
        factory.setResourceResolver(new ResourceResolver());

        Schema xsdSchema = factory.newSchema(new StreamSource(
                ResourcesLoader.getXSD(XMLFiniteAutomatonLoader.XSD_FILE)));
        this.validator = xsdSchema.newValidator();
        this.validator.setErrorHandler(new ValidationErrorHandler());
    }

    /**
     * Loads finite automaton from specified XML file.
     *
     * @param name name of XML file (without {@code .xml} file extension) from 
     * which will be automaton loaded.
     * @return Loaded finite automaton from specified XML file.
     * @throws XMLException when XML file cannot be loaded or if XML file isn't 
     * valid.
     * @throws JAXBException when occurs some error during reading data from 
     * XML file.
     */
    // JDK1.5 compatible @Override
    public FiniteAutomaton loadAutomaton(final String name) throws XMLException,
                                                           JAXBException {
        InputStream xml = ResourcesLoader.getXML(name + ".xml");

        if (xml == null) {
            throw new XMLException("Cannot read XML file for automaton");
        }

        if (!this.validate(xml)) {
            throw new XMLException("XML file for automaton isn't valid");
        }

        xml = ResourcesLoader.getXML(name + ".xml");
        JAXBContext context = JAXBContext.newInstance(
                XMLFiniteAutomatonLoader.JAXB_PACKAGE);
        // XML reading
        Unmarshaller unmarshaller = context.createUnmarshaller();

        JAXBElement<?> elementRead = (JAXBElement<?>) unmarshaller.unmarshal(xml);
        AutomatonType rootElement = (AutomatonType) elementRead.getValue();

        String stateName = rootElement.getInitialState();
        FiniteAutomaton automaton = this.createFiniteAutomaton(
                rootElement.getName(),
                stateName);

        // for transitions
        HashMap<String, State> loadedStates = new HashMap<String, State>();
        loadedStates.put(stateName, automaton.getInitialState());

        List<Object> states = rootElement.getStates().getStateOrErrorState();
        for (Object someState : states) {
            stateName = null;
            State state = null;
            List<ListenerType> listeners = null;

            if (someState instanceof StateType) {
                StateType stateType = (StateType) someState;

                stateName = stateType.getName();
                state = this.createState(stateName, stateType.isFinal());

                listeners = stateType.getListener();
            }
            else if (someState instanceof ErrorStateType) {
                ErrorStateType errorStateType = (ErrorStateType) someState;

                stateName = errorStateType.getName();
                state = this.createErrorState(stateName,
                        errorStateType.getMessage());

                listeners = errorStateType.getListener();
            }

            if (state != null) {
                loadedStates.put(stateName, state);
                automaton.addState(state);

                for (ListenerType listenerType : listeners) {
                    StateListener listener = this.createStateListener(
                            listenerType.getClazz(), listenerType.getArgument());
                    if (listener != null) {
                        state.addListener(listener);
                    }
                }
            }
        }

        TransitionsType transitionsType = rootElement.getTransitions();
        List<Object> transitions = transitionsType.getTransitionOrSelfTransition();
        for (Object someTransition : transitions) {
            if (someTransition instanceof SelfTransitionType) {
                SelfTransitionType selfTransitionType = (SelfTransitionType) someTransition;

                State state = loadedStates.get(selfTransitionType.getState());

                Transition transition = this.createSelfTransition(state,
                        selfTransitionType.getTransitionString());
                automaton.addTransition(transition);

                continue;
            }

            if (someTransition instanceof TransitionType) {
                TransitionType transitionType = (TransitionType) someTransition;

                State source = loadedStates.get(transitionType.getSource());
                State target = loadedStates.get(transitionType.getTarget());

                Transition transition = this.createTransition(source, target,
                        transitionType.getTransitionString());
                automaton.addTransition(transition);
            }
        }

        return automaton;
    }

    /**
     * Creates finite automaton.
     *
     * @param name name of finite automaton.
     * @param initialState name of initial state.
     * @return Created finite automaton.
     */
    protected FiniteAutomaton createFiniteAutomaton(final String name,
                                                    final String initialState) {
        FiniteAutomaton automaton = new FiniteAutomaton(name, initialState);

        return automaton;
    }

    /**
     * Creates state of finite automaton.
     *
     * @param name name of state.
     * @param isFinal information whether state is final.
     * @return Created state of finite automaton.
     */
    protected State createState(final String name,
                                final Boolean isFinal) {
        State state = new State(name);

        if (isFinal == Boolean.TRUE) {
            state.setFinal(true);
        }

        return state;
    }

    /**
     * Creates error state of finite automaton.
     *
     * @param name name of error state.
     * @param errorMessage error message for error state.
     * @return Created error state of finite automaton.
     */
    protected ErrorState createErrorState(final String name,
                                          final String errorMessage) {
        ErrorState errorState = new ErrorState(name, errorMessage);

        return errorState;
    }

    /**
     * Creates transition of finite automaton.
     *
     * @param source source state of transition.
     * @param target target state of transition.
     * @param transitionString regular expression for transition.
     * @return Created transition.
     */
    protected Transition createTransition(final State source,
                                          final State target,
                                          final String transitionString) {
        RegularExpressionTransition transition = new RegularExpressionTransition(
                source, target, transitionString);

        return transition;
    }

    /**
     * Creates self-transition of finite automaton.
     *
     * @param state source and target state of transition.
     * @param transitionString regular expression for transition.
     * @return Created self-transition.
     */
    protected Transition createSelfTransition(final State state,
                                              final String transitionString) {
        RegularExpressionTransition transition = new RegularExpressionTransition(
                state, transitionString);

        return transition;
    }

    /**
     * Creates state listener.
     *
     * @param clazzName full class name of listener.
     * @param arguments arguments for constructor of listener.
     * @return Created state listener.
     */
    protected StateListener createStateListener(final String clazzName,
                                                final List<String> arguments) {
        StateListener listener = null;

        try {
            Class<?> clazz = Class.forName(clazzName);

            Constructor<?>[] constructs = clazz.getConstructors();
            Object[] args = arguments.toArray();
            if (args.length == 0) {
                listener = (StateListener) constructs[0].newInstance();
            }
            else {
                for (Constructor<?> constructor : constructs) {
                    int length = constructor.getTypeParameters().length;
                    if (length == args.length) {
                        listener = (StateListener) constructor.newInstance(args);
                        break;
                    }
                }
            }
        } catch (Throwable exc) {
            // listener will be null
        }

        return listener;
    }

    /**
     * Creates transition listener.
     *
     * @param clazzName full class name of listener.
     * @param arguments arguments for constructor of listener.
     * @return Created transition listener.
     */
    protected TransitionListener createTransitionListener(final String clazzName,
                                                          final List<String> arguments) {
        TransitionListener listener = null;

        try {
            Class<?> clazz = Class.forName(clazzName);

            Constructor<?>[] constructs = clazz.getConstructors();
            Object[] args = arguments.toArray();
            if (args.length == 0) {
                listener = (TransitionListener) constructs[0].newInstance();
            }
            else {
                for (Constructor<?> constructor : constructs) {
                    int length = constructor.getTypeParameters().length;
                    if (length == args.length) {
                        listener = (TransitionListener) constructor.newInstance(args);
                        break;
                    }
                }
            }
        } catch (Throwable exc) {
            // listener will be null
        }

        return listener;
    }

    /**
     * Validates XML file for configuration of finite automaton.
     *
     * @param xml input stream for XML file.
     * @return {@code true} if specified XML file is valid; 
     *         {@code false} otherwise.
     */
    protected boolean validate(final InputStream xml) {
        try {
            Source xmlSource = new StreamSource(xml);
            this.validator.validate(xmlSource);
        } catch (Throwable exc) {
            return false;
        }

        return true;
    }
}
