/**
 * This Java file was created 25.5.2012 21:18:44.
 */
package system.automaton.xml;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import org.w3c.dom.ls.LSInput;

/**
 * Class {@code Input} represents an input source for data.
 * <p>
 * This interface allows an application to encapsulate information about an
 * input source in a single object, which may include a public identifier, a
 * system identifier, a byte stream (possibly with a specified encoding), a base
 * URI, and/or a character stream.
 * <p>
 * The exact definitions of a byte stream and a character stream are binding
 * dependent.
 * <p>
 * The application is expected to provide objects that implement this interface
 * whenever such objects are needed. The application can either provide its own
 * objects that implement this interface, or it can use the generic factory
 * method <code>DOMImplementationLS.createLSInput()</code> to create objects
 * that implement this interface.
 * <p>
 * The <code>LSParser</code> will use the <code>LSInput</code> object to
 * determine how to read data. The <code>LSParser</code> will look at the
 * different inputs specified in the <code>LSInput</code> in the following order
 * to know which one to read from, the first one that is not null and not an
 * empty string will be used:
 * <ol>
 * <li> <code>LSInput.characterStream</code></li>
 * <li>
 * <code>LSInput.byteStream</code></li>
 * <li> <code>LSInput.stringData</code></li>
 * <li>
 * <code>LSInput.systemId</code></li>
 * <li> <code>LSInput.publicId</code></li>
 * </ol>
 * <p>
 * If all inputs are null, the <code>LSParser</code> will report a
 * <code>DOMError</code> with its <code>DOMError.type</code> set to
 * <code>"no-input-specified"</code> and its <code>DOMError.severity</code> set
 * to <code>DOMError.SEVERITY_FATAL_ERROR</code>.
 * <p>
 * <code>LSInput</code> objects belong to the application. The DOM
 * implementation will never modify them (though it may make copies and modify
 * the copies, if necessary).
 * <p>
 * See also the <a
 * href='http://www.w3.org/TR/2004/REC-DOM-Level-3-LS-20040407'>Document Object
 * Model (DOM) Level 3 Load and Save Specification</a>.
 * 
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.2
 * 
 * @since 0.1.5
 */
public class Input implements LSInput {
    /** The public identifier for this input source. */
    private String publicId;
    /**
     * The system identifier, a URI reference [<a
     * href='http://www.ietf.org/rfc/rfc2396.txt'>IETF RFC 2396</a>], for this
     * input source.
     */
    private String systemId;

    /** Buffered input stream. */
    private BufferedInputStream inputStream;

    /**
     * Constructs input.
     * 
     * @param publicId public identifier for this input source.
     * @param sysId the system identifier.
     * @param input input stream.
     */
    public Input(final String publicId,
                 final String sysId,
                 final InputStream input) {
        this.publicId = publicId;
        this.systemId = sysId;
        this.inputStream = new BufferedInputStream(input);
    }

    /**
     * Returns input stream.
     * 
     * @return buffered input stream.
     */
    public BufferedInputStream getInputStream() {
        return this.inputStream;
    }

    /**
     * Sets input stream.
     * 
     * @param inputStream input stream.
     */
    public void setInputStream(final BufferedInputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * An attribute of a language and binding dependent type that represents a
     * stream of 16-bit units. The application must encode the stream using
     * UTF-16 (defined in [Unicode] and in [ISO/IEC 10646]). It is not a
     * requirement to have an XML declaration when using character streams. If
     * an XML declaration is present, the value of the encoding attribute will
     * be ignored.
     */
    // JDK1.5 compatible @Override
    public Reader getCharacterStream() {
        return null;
    }

    /**
     * An attribute of a language and binding dependent type that represents a
     * stream of 16-bit units. The application must encode the stream using
     * UTF-16 (defined in [Unicode] and in [ISO/IEC 10646]). It is not a
     * requirement to have an XML declaration when using character streams. If
     * an XML declaration is present, the value of the encoding attribute will
     * be ignored.
     */
    // JDK1.5 compatible @Override
    public void setCharacterStream(final Reader characterStream) {
    }

    /**
     * An attribute of a language and binding dependent type that represents a
     * stream of bytes. <br>
     * If the application knows the character encoding of the byte stream, it
     * should set the encoding attribute. Setting the encoding in this way will
     * override any encoding specified in an XML declaration in the data.
     */
    // JDK1.5 compatible @Override
    public InputStream getByteStream() {
        return null;
    }

    /**
     * An attribute of a language and binding dependent type that represents a
     * stream of bytes. <br>
     * If the application knows the character encoding of the byte stream, it
     * should set the encoding attribute. Setting the encoding in this way will
     * override any encoding specified in an XML declaration in the data.
     */
    // JDK1.5 compatible @Override
    public void setByteStream(final InputStream byteStream) {
    }

    /**
     * String data to parse. If provided, this will always be treated as a
     * sequence of 16-bit units (UTF-16 encoded characters). It is not a
     * requirement to have an XML declaration when using <code>stringData</code>. 
     * If an XML declaration is present, the value of the encoding attribute
     * will be ignored.
     * 
     * @throws Error in case of error during reading the data.
     */
    // JDK1.5 compatible @Override
    public String getStringData() {
        synchronized (this.inputStream) {
            try {
                byte[] input = new byte[this.inputStream.available()];
                this.inputStream.read(input);
                String contents = new String(input);

                return contents;
            } catch (IOException exc) {
                throw new Error("Can't get string data", exc);
            }
        }
    }

    /**
     * String data to parse. If provided, this will always be treated as a
     * sequence of 16-bit units (UTF-16 encoded characters). It is not a
     * requirement to have an XML declaration when using <code>stringData</code>
     * . If an XML declaration is present, the value of the encoding attribute
     * will be ignored.
     */
    // JDK1.5 compatible @Override
    public void setStringData(final String stringData) {
    }

    /**
     * The system identifier, a URI reference [<a
     * href='http://www.ietf.org/rfc/rfc2396.txt'>IETF RFC 2396</a>], for this
     * input source. The system identifier is optional if there is a byte
     * stream, a character stream, or string data. It is still useful to provide
     * one, since the application will use it to resolve any relative URIs and
     * can include it in error messages and warnings. (The LSParser will only
     * attempt to fetch the resource identified by the URI reference if there is
     * no other input available in the input source.) <br>
     * If the application knows the character encoding of the object pointed to
     * by the system identifier, it can set the encoding using the
     * <code>encoding</code> attribute. <br>
     * If the specified system ID is a relative URI reference (see section 5 in
     * [<a href='http://www.ietf.org/rfc/rfc2396.txt'>IETF RFC 2396</a>]), the
     * DOM implementation will attempt to resolve the relative URI with the
     * <code>baseURI</code> as the base, if that fails, the behavior is
     * implementation dependent.
     */
    // JDK1.5 compatible @Override
    public String getSystemId() {
        return this.systemId;
    }

    /**
     * The system identifier, a URI reference [<a
     * href='http://www.ietf.org/rfc/rfc2396.txt'>IETF RFC 2396</a>], for this
     * input source. The system identifier is optional if there is a byte
     * stream, a character stream, or string data. It is still useful to provide
     * one, since the application will use it to resolve any relative URIs and
     * can include it in error messages and warnings. (The LSParser will only
     * attempt to fetch the resource identified by the URI reference if there is
     * no other input available in the input source.) <br>
     * If the application knows the character encoding of the object pointed to
     * by the system identifier, it can set the encoding using the
     * <code>encoding</code> attribute. <br>
     * If the specified system ID is a relative URI reference (see section 5 in
     * [<a href='http://www.ietf.org/rfc/rfc2396.txt'>IETF RFC 2396</a>]), the
     * DOM implementation will attempt to resolve the relative URI with the
     * <code>baseURI</code> as the base, if that fails, the behavior is
     * implementation dependent.
     */
    // JDK1.5 compatible @Override
    public void setSystemId(final String systemId) {
        this.systemId = systemId;
    }

    /**
     * The public identifier for this input source. This may be mapped to an
     * input source using an implementation dependent mechanism (such as
     * catalogues or other mappings). The public identifier, if specified, may
     * also be reported as part of the location information when errors are
     * reported.
     */
    // JDK1.5 compatible @Override
    public String getPublicId() {
        return this.publicId;
    }

    /**
     * The public identifier for this input source. This may be mapped to an
     * input source using an implementation dependent mechanism (such as
     * catalogues or other mappings). The public identifier, if specified, may
     * also be reported as part of the location information when errors are
     * reported.
     */
    // JDK1.5 compatible @Override
    public void setPublicId(final String publicId) {
        this.publicId = publicId;
    }

    /**
     * The base URI to be used (see section 5.1.4 in [<a
     * href='http://www.ietf.org/rfc/rfc2396.txt'>IETF RFC 2396</a>]) for
     * resolving a relative <code>systemId</code> to an absolute URI. <br>
     * If, when used, the base URI is itself a relative URI, an empty string, or
     * null, the behavior is implementation dependent.
     */
    // JDK1.5 compatible @Override
    public String getBaseURI() {
        return null;
    }

    /**
     * The base URI to be used (see section 5.1.4 in [<a
     * href='http://www.ietf.org/rfc/rfc2396.txt'>IETF RFC 2396</a>]) for
     * resolving a relative <code>systemId</code> to an absolute URI. <br>
     * If, when used, the base URI is itself a relative URI, an empty string, or
     * null, the behavior is implementation dependent.
     */
    // JDK1.5 compatible @Override
    public void setBaseURI(final String baseURI) {
    }

    /**
     * The character encoding, if known. The encoding must be a string
     * acceptable for an XML encoding declaration ([<a
     * href='http://www.w3.org/TR/2004/REC-xml-20040204'>XML 1.0</a>] section
     * 4.3.3 "Character Encoding in Entities"). <br>
     * This attribute has no effect when the application provides a character
     * stream or string data. For other sources of input, an encoding specified
     * by means of this attribute will override any encoding specified in the
     * XML declaration or the Text declaration, or an encoding obtained from a
     * higher level protocol, such as HTTP [<a
     * href='http://www.ietf.org/rfc/rfc2616.txt'>IETF RFC 2616</a>].
     */
    // JDK1.5 compatible @Override
    public String getEncoding() {
        return null;
    }

    /**
     * The character encoding, if known. The encoding must be a string
     * acceptable for an XML encoding declaration ([<a
     * href='http://www.w3.org/TR/2004/REC-xml-20040204'>XML 1.0</a>] section
     * 4.3.3 "Character Encoding in Entities"). <br>
     * This attribute has no effect when the application provides a character
     * stream or string data. For other sources of input, an encoding specified
     * by means of this attribute will override any encoding specified in the
     * XML declaration or the Text declaration, or an encoding obtained from a
     * higher level protocol, such as HTTP [<a
     * href='http://www.ietf.org/rfc/rfc2616.txt'>IETF RFC 2616</a>].
     */
    // JDK1.5 compatible @Override
    public void setEncoding(final String encoding) {
    }

    /**
     * If set to true, assume that the input is certified (see section 2.13 in
     * [<a href='http://www.w3.org/TR/2004/REC-xml11-20040204/'>XML 1.1</a>])
     * when parsing [<a href='http://www.w3.org/TR/2004/REC-xml11-20040204/'>XML
     * 1.1</a>].
     */
    // JDK1.5 compatible @Override
    public boolean getCertifiedText() {
        return false;
    }

    /**
     * If set to true, assume that the input is certified (see section 2.13 in
     * [<a href='http://www.w3.org/TR/2004/REC-xml11-20040204/'>XML 1.1</a>])
     * when parsing [<a href='http://www.w3.org/TR/2004/REC-xml11-20040204/'>XML
     * 1.1</a>].
     */
    // JDK1.5 compatible @Override
    public void setCertifiedText(final boolean certifiedText) {
    }
}