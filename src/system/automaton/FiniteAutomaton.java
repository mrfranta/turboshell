/**
 * This Java file was created 16.10.2013 22:27:38.
 */
package system.automaton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import system.utils.StringUtilities;

/**
 * Class {@code FiniteAutomaton} represents finite automaton with some name 
 * which contains states with unique names. Also contains transitions between 
 * states. Finite automaton can process strings and returns information whether 
 * was that string accepted by the finite automaton or not.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.5
 * 
 * @since 0.1.3
 */
public class FiniteAutomaton {
    /** Name of finite automaton. */
    private String name;

    /** Initial state of finite automaton. */
    private State initial;
    /** Actual state in which is finite automaton. */
    private State actual;

    /** 
     * States of finite automaton. It's used map because finite automaton 
     * should contains states with unique names.
     */
    private Map<String, State> states;

    /** 
     * Error state in which ends finite automaton in case of situation when 
     * retrieves a character for which doesn't exist transition from the 
     * actual state.
     */
    public static final ErrorState INVALID_TRANSITION = new ErrorState(
            "INVALID_TRANSITION",
            "Finite automaton processed character, for which doesn't exist transition");

    /**
     * Constructs finite automaton with specified initial state.
     *
     * @param initialState name of initial state of finite automaton.
     */
    public FiniteAutomaton(final String initialState) {
        this(null, new State(initialState));
    }

    /**
     * Constructs finite automaton with specified initial state.
     *
     * @param initial initial state of finite automaton.
     */
    public FiniteAutomaton(final State initial) {
        this(null, initial);
    }

    /**
     * Constructs finite automaton with specified initial state and name.
     *
     * @param name name of finite automaton.
     * @param initialState name of initial state of finite automaton.
     */
    public FiniteAutomaton(final String name, final String initialState) {
        this(name, new State(initialState));
    }

    /**
     * Constructs finite automaton with specified initial state and name.
     *
     * @param name name of finite automaton.
     * @param initial initial state of finite automaton.
     */
    public FiniteAutomaton(final String name, final State initial) {
        this.states = new HashMap<String, State>();

        this.setName(name);
        this.setInitialState(initial);
        this.setActualState(initial);
    }

    /**
     * Returns name of finite automaton.
     *
     * @return Name of finite automaton.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets name of finite automaton. If is entered empty name or {@code null} 
     * the name is set to &quot;FiniteAutomaton&quot;.
     *
     * @param name name of finite automaton to set.
     */
    public void setName(final String name) {
        if (StringUtilities.isEmpty(name)) {
            this.name = "FiniteAutomaton";
            return;
        }

        this.name = name;
    }

    /**
     * Returns initial state of finite automaton.
     *
     * @return Initial state of finite automaton.
     */
    public State getInitialState() {
        return this.initial;
    }

    /**
     * Sets initial state of finite automaton.
     *
     * @param initial initial state to set.
     * @throws NullPointerException in the case of entering the state
     * which is {@code null}.
     */
    public void setInitialState(final State initial) {
        if (initial == null) {
            throw new NullPointerException("Initial automaton state " +
                    "cannot be null");
        }

        if (this.states.get(initial) == null) {
            this.addState(initial);
        }

        if (this.initial != null) {
            this.initial.setInitial(false);
        }

        this.initial = initial;
        this.initial.setInitial(true);
    }

    /**
     * Returns actual state of finite automaton.
     *
     * @return Actual state of finite automaton.
     */
    public State getActual() {
        return this.actual;
    }

    /**
     * Sets actual state of finite automaton.
     *
     * @param actual state to set. If state is {@code null}, it will be set 
     * initial state as actual.
     */
    protected void setActualState(final State actual) {
        if (actual == null) {
            this.actual = this.getInitialState();

            return;
        }

        this.actual = actual;
    }

    /**
     * Changes actual state of finite automaton. This method also invokes 
     * listeners in actual and target states.
     *
     * @param targetState target state which will be actual. If target state 
     * is {@code null}, it will be set initial state as actual.
     * @param character character through which will be changed actual state.
     */
    protected void changeActualState(final State targetState,
                                     final char character) {
        State target = targetState;
        if (target == null) {
            target = this.getInitialState();
        }

        this.actual.fireLeavingState(target, character);
        target.fireEntryState(this.actual, character);
        this.actual = target;
    }

    /**
     * Adds new state with specified name into finite automaton.
     *
     * @param stateName name of state which will be added.
     * @return Instance of state which was added into finite automaton.
     * @throws IllegalArgumentException in the case of entering the state 
     * name which is already in finite automaton. Finite automaton should 
     * contains states with unique names.
     */
    public State addState(final String stateName) {
        if (stateName == null) {
            return null;
        }

        if (this.states.get(stateName) != null) {
            throw new IllegalArgumentException("Finite automaton already " +
                    "contains state with same name");
        }

        State state = new State(stateName);
        this.states.put(stateName, state);

        return state;
    }

    /**
     * Adds new state into finite automaton.
     *
     * @param state state to add.
     * @throws IllegalArgumentException in the case of entering the state 
     * with name which is already in finite automaton. Finite automaton should 
     * contains states with unique names.
     */
    public void addState(final State state) {
        if (state == null) {
            return;
        }

        if (this.states.get(state.getName()) != null) {
            throw new IllegalArgumentException("Finite automaton already " +
                    "contains state with same name");
        }

        this.states.put(state.getName(), state);
    }

    /**
     * Adds states into finite automaton.
     *
     * @param states array of states to add.
     * @throws IllegalArgumentException in the case of entering the state 
     * with name which is already in finite automaton. Finite automaton should 
     * contains states with unique names.
     */
    public void addStates(final State[] states) {
        if (states == null) {
            return;
        }

        for (State state : states) {
            this.addState(state);
        }
    }

    /**
     * Adds states into finite automaton.
     *
     * @param states collection of states to add.
     * @throws IllegalArgumentException in the case of entering the state 
     * with name which is already in finite automaton. Finite automaton should 
     * contains states with unique names.
     */
    public void addStates(final Collection<State> states) {
        if (states == null) {
            return;
        }

        for (State state : states) {
            this.addState(state);
        }
    }

    /**
     * Removes specified state from finite automaton.
     *
     * @param state state to remove.
     * @throws IllegalArgumentException in the case of entering the state 
     * which is initial because finite automaton must always contain 
     * one initial state.
     */
    public void removeState(final State state) {
        if (state == null) {
            return;
        }

        if (state.isInitial()) {
            throw new IllegalArgumentException("Automaton initial state " +
                    "cannot be removed");
        }

        this.states.remove(state);
    }

    /**
     * Returns state with specified name in finite automaton.
     *
     * @param stateName name of state.
     * @return State with specified name which is in finite automaton. If this 
     * automaton doesn't contain state with specified name, it will return 
     * {@code null}.
     */
    public State getState(final String stateName) {
        return this.states.get(stateName);
    }

    /**
     * Returns all states in finite automaton.
     *
     * @return All states in finite automaton.
     */
    public State[] getStates() {
        Collection<State> states = this.states.values();
        State[] array = new State[states.size()];

        return states.toArray(array);
    }

    /**
     * Adds specified transition into finite automaton.
     *
     * @param transition transition to add.
     */
    public void addTransition(final Transition transition) {
        if (transition == null) {
            return;
        }

        State source = transition.getSource();
        State target = transition.getTarget();

        if (this.getState(source.getName()) != null && this.getState(target.getName()) != null) {
            source.addTransition(transition);
        }
    }

    /**
     * Adds specified transitions into finite automaton.
     *
     * @param transitions array of transitions to add.
     */
    public void addTransitions(final Transition[] transitions) {
        if (transitions == null) {
            return;
        }

        for (Transition transition : transitions) {
            this.addTransition(transition);
        }
    }

    /**
     * Adds specified transitions into finite automaton.
     *
     * @param transitions collection of transitions to add.
     */
    public void addTransitions(final Collection<Transition> transitions) {
        if (transitions == null) {
            return;
        }

        for (Transition transition : transitions) {
            this.addTransition(transition);
        }
    }

    /**
     * Removes specified transition from finite automaton.
     *
     * @param transition transition to remove.
     */
    public void removeTransition(final Transition transition) {
        State source = transition.getSource();
        source.removeTransition(transition);
    }

    /**
     * Returns all transitions in finite automaton.
     *
     * @return All transitions in finite automaton.
     */
    public Transition[] getTransitions() {
        List<Transition> transitions = new ArrayList<Transition>();
        for (State state : this.states.values()) {
            Transition[] trans = state.getTransitions();
            for (Transition transition : trans) {
                transitions.add(transition);
            }
        }

        Transition[] array = new Transition[transitions.size()];

        return transitions.toArray(array);
    }

    /**
     * Returns error message from error state which is actual. If actual state 
     * isn't instance of error state it returns {@code null}.
     *
     * @return Error message from error state which is actual. If actual state 
     * isn't instance of error state it returns {@code null}.
     */
    public String getErrorMessage() {
        State actual = this.getActual();

        if (actual instanceof ErrorState) {
            return ((ErrorState) actual).getErrorMessage();
        }

        return null;
    }

    /**
     * Changes actual state to initial.
     */
    public void reset() {
        this.setActualState(null);
    }

    /**
     * Processes specified string and returns information whether was accepted 
     * or not.
     *
     * @param pattern string to process.
     * @return {@code true} if the specified string was accepted; 
     *         {@code false} otherwise.
     */
    public boolean process(final String pattern) {
        this.reset();

        for (int i = 0; i < pattern.length(); i++) {
            char character = pattern.charAt(i);
            String charStr = character + "";

            Transition[] transitions = this.getActual().getTransitions();
            Transition transitionMade = null;
            State target = null;
            for (Transition transition : transitions) {
                if (charStr.matches(transition.getTransitionRegularExpression())) {
                    target = transition.getTarget();
                    transitionMade = transition;

                    break;
                }
            }

            if (target == null) {
                this.changeActualState(FiniteAutomaton.INVALID_TRANSITION,
                        character);
                return false;
            }

            this.changeActualState(target, character);

            if (transitionMade != null) {
                transitionMade.fireTransitionMade(character);
            }

            if (target instanceof ErrorState) {
                return false;
            }
        }

        this.getActual().fireLeavingState(null, (char) 0x00);

        return this.getActual().isFinal();
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((this.initial == null) ? 0 : this.initial.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + ((this.states == null) ? 0 : this.states.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof FiniteAutomaton)) {
            return false;
        }

        FiniteAutomaton other = (FiniteAutomaton) obj;
        if (this.initial == null) {
            if (other.initial != null) {
                return false;
            }
        }
        else if (!this.initial.equals(other.initial)) {
            return false;
        }

        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        }
        else if (!this.name.equals(other.name)) {
            return false;
        }

        if (this.states == null) {
            if (other.states != null) {
                return false;
            }
        }
        else if (!this.states.equals(other.states)) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of finite automaton that has the format:
     * 
     * <pre>
     *     FiniteAutomaton [name=${FiniteAutomatonName}, initial=${NameOfInitialState}, actual=${NameOfActualState}]
     * </pre>
     *
     * @return String value of finite automaton.
     */
    @Override
    public String toString() {
        return "FiniteAutomaton [name=" + this.name +
                ", initial=" + this.initial.getName() +
                ", actual=" + this.actual.getName() + "]";
    }
}
