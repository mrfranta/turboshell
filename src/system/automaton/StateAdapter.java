/**
 * This Java file was created 24.10.2013 12:44:27.
 */
package system.automaton;

/**
 * Abstract class {@code StateAdapter} implements {@link StateListener} and 
 * represents adapter for listeners. This adapter is appropriate for a 
 * simpler implementation of state listeners.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.1.8
 */
public abstract class StateAdapter implements StateListener {
    /** Owner of this listener. */
    private State state;

    /**
     * Constructs adapter (listener) for any state.
     */
    public StateAdapter() {
        this.state = null;
    }

    /**
     * Constructs adapter (listener) for specified state.
     * 
     * @param state owner of state listener.
     */
    public StateAdapter(final State state) {
        this.state = state;
    }

    /**
     * Returns owner of this listener (state which has registered this listener).
     *
     * @return State which has registered this listener.
     */
    // JDK1.5 compatible @Override
    public State getState() {
        return this.state;
    }

    /**
     * Sets owner of this listener (state which has registered this listener).
     *
     * @param state state which has registered this listener.
     */
    // @Override => JDK 1.5 compatible
    public void setState(final State state) {
        this.state = state;
    }

    /**
     * Action that is being invoked in case of using transition to entry 
     * into owner of this listener from specified state.
     *
     * @param sourceState state from which the transition leads.
     * @param character character used to the transition.
     */
    // JDK1.5 compatible @Override
    public void entryState(final State sourceState, final char character) {
    }

    /**
     * Action that is being invoked in case of using transition to leave 
     * owner of this listener and entry into specified state.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    // JDK1.5 compatible @Override
    public void leavingState(final State targetState, final char character) {
    }
}
