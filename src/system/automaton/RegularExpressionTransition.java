/**
 * This Java file was created 17.10.2013 11:12:52.
 */
package system.automaton;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Class {@code RegularExpressionTransition} represents transition between 
 * states of finite automaton. For transitions is used regular expression 
 * which can be obtained using method {@link #getTransitionRegularExpression()}.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.1
 * 
 * @since 0.1.3
 */
public class RegularExpressionTransition extends Transition {
    /** 
     * Regular expression which represents character or character set for this 
     * transition. 
     */
    private String regularExpression;

    /**
     * Constructs self transition for specified state which uses specified 
     * regular expression for making transition.
     *
     * @param state automaton state for self transition.
     * @param regularExpression regular expression which will be used for 
     * making transition.
     * 
     * @throws IllegalArgumentException in the case of entering regular 
     * expression with invalid syntax.
     */
    public RegularExpressionTransition(final State state,
                                       final String regularExpression) {
        this(state, state, regularExpression);
    }

    /**
     * Constructs transition between specified states which uses specified 
     * regular expression for making transition.
     *
     * @param source source state of transition.
     * @param target transition state of transition.
     * @param regularExpression regular expression which will be used for 
     * making transition.
     * 
     * @throws IllegalArgumentException in the case of entering regular 
     * expression with invalid syntax.
     */
    public RegularExpressionTransition(final State source,
                                       final State target,
                                       final String regularExpression) {
        super(source, target);

        try {
            Pattern.compile(regularExpression);
        } catch (PatternSyntaxException exception) {
            throw new IllegalArgumentException("Invalid syntax of " +
                    "regular expression");
        }

        this.regularExpression = regularExpression;
    }

    /**
     * Returns regular expression which represents character or character set 
     * for this transition. The finite automaton using method 
     * {@link String#matches(String)} to identify whether this transition can 
     * be used.
     *
     * @return Regular expression which represents character or character set 
     * for this transition.
     */
    @Override
    public String getTransitionRegularExpression() {
        return this.regularExpression;
    }
}
