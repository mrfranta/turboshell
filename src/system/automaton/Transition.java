/**
 * This Java file was created 17.10.2013 9:30:46.
 */
package system.automaton;

import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code Transition} represents abstract transition between states of 
 * finite automaton. For transitions is used regular expression which can be 
 * obtained using method {@link #getTransitionRegularExpression()}.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.1.3
 */
public abstract class Transition implements Comparable<Transition> {
    /** Source state of transition. */
    private State source;
    /** Target state of transition. */
    private State target;

    /** List of listeners for this transition. */
    private List<TransitionListener> listeners;

    /**
     * Constructs self transition for specified state.
     *
     * @param state automaton state for self transition.
     */
    public Transition(final State state) {
        this(state, state);
    }

    /**
     * Constructs transition between specified states.
     *
     * @param source source state of transition.
     * @param target transition state of transition.
     */
    public Transition(final State source,
                      final State target) {
        this.setSource(source);
        this.setTarget(target);

        this.listeners = new ArrayList<TransitionListener>();
    }

    /**
     * Returns source state of transition.
     *
     * @return Source state of transition.
     */
    public State getSource() {
        return this.source;
    }

    /**
     * Sets source state of transition.
     *
     * @param source source state of transition.
     * @throws NullPointerException in the case of entering the source 
     * state which is {@code null}.
     */
    protected void setSource(final State source) {
        if (source == null) {
            throw new NullPointerException("Source automaton state " +
                    "cannot be null");
        }

        this.source = source;
    }

    /**
     * Returns target state of transition.
     *
     * @return Target state of transition.
     */
    public State getTarget() {
        return this.target;
    }

    /**
     * Sets target state of transition.
     *
     * @param target target state of transition.
     * @throws NullPointerException in the case of entering the state
     * which is {@code null}.
     */
    protected void setTarget(final State target) {
        if (target == null) {
            throw new NullPointerException(
                    "Target automaton state cannot be null");
        }

        this.target = target;
    }

    /**
     * Returns regular expression which represents character or character set 
     * for this transition. The finite automaton using method 
     * {@link String#matches(String)} to identify whether this transition can 
     * be used.
     *
     * @return Regular expression which represents character or character set 
     * for this transition.
     */
    public abstract String getTransitionRegularExpression();

    /**
     * Registers and adds specified transition listener.
     *
     * @param listener transition listener which will be added.
     */
    public void addListener(final TransitionListener listener) {
        if (listener == null) {
            return;
        }

        Transition transition = listener.getTransition();
        if (transition == this) {
            return;
        }

        if (transition != null) {
            transition.removeListener(listener);
        }

        this.listeners.add(listener);
        listener.setTransition(this);
    }

    /**
     * Unregisters and removes specified transition listener.
     *
     * @param listener transition listener which will be removed.
     */
    public void removeListener(final TransitionListener listener) {
        if (listener == null) {
            return;
        }

        if (this.listeners.contains(listener)) {
            this.listeners.remove(listener);
            listener.setTransition(null);
        }
    }

    /**
     * Returns all registered transition listeners for this transition.
     *
     * @return Array of registered transition listeners.
     */
    public TransitionListener[] getListeners() {
        TransitionListener[] array = new TransitionListener[this.listeners.size()];

        return this.listeners.toArray(array);
    }

    /**
     * Invokes method {@link TransitionListener#transitionMade(State, State, char)}
     * in all registered listeners in the case of using this transition in the final 
     * automaton.
     *
     * @param character character used to the transition.
     */
    protected void fireTransitionMade(final char character) {
        for (TransitionListener listener : this.listeners) {
            listener.transitionMade(this.getSource(), this.getTarget(),
                    character);
        }
    }

    /**
     * Compares this object with the specified object for order. Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param transition the transition to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     */
    // JDK1.5 compatible @Override
    public int compareTo(final Transition transition) {
        if (transition == null) {
            return 1;
        }

        State state = this.getSource();
        int result = state.compareTo(transition.getSource());
        if (result == 0) {
            state = transition.getTarget();
            result = state.compareTo(transition.getTarget());

            if (result == 0) {
                String expression = this.getTransitionRegularExpression();

                return expression.compareTo(transition.getTransitionRegularExpression());
            }
        }

        return result;
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((this.listeners == null) ? 0 : this.listeners.hashCode());
        result = prime * result + ((this.source == null) ? 0 : this.source.hashCode());
        result = prime * result + ((this.target == null) ? 0 : this.target.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Transition)) {
            return false;
        }

        Transition other = (Transition) obj;
        if (this.listeners == null) {
            if (other.listeners != null) {
                return false;
            }
        }
        else if (!this.listeners.equals(other.listeners)) {
            return false;
        }

        if (this.source == null) {
            if (other.source != null) {
                return false;
            }
        }
        else if (!this.source.equals(other.source)) {
            return false;
        }

        if (this.target == null) {
            if (other.target != null) {
                return false;
            }
        }
        else if (!this.target.equals(other.target)) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of transition that has the format:
     * 
     * <pre>
     *     Transition [source=${NameOfSourceState}, message=${NameOfTargetState}, transition="${TransitionRegexp}"]
     * </pre>
     *
     * @return String value of transition.
     */
    @Override
    public String toString() {
        return "Transition [source=" + this.source.getName() +
                ", target=" + this.target.getName() + ", transition=\"" +
                this.getTransitionRegularExpression() + "\"]";
    }
}
