/**
 * This Java file was created 21.10.2013 23:49:14.
 */
package system.automaton;

/**
 * Class {@code ErrorState} represents error state in finite automaton. This 
 * state is always final, cannot contains any transitions nad cannot be initial.
 * Error state contains error message which can be obtained by 
 * {@link FiniteAutomaton}.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.1
 * 
 * @since 0.1.7
 */
public class ErrorState extends State {
    /** Error message returned by this state. It can be null. */
    private String message;

    /**
     * Constructs error state with specified name. This state has no 
     * {@code errorMessage}.
     *
     * @param name name of error state.
     */
    public ErrorState(final String name) {
        this(name, null);
    }

    /**
     * Constructs error state with specified name and error message.
     *
     * @param name name of error state.
     * @param errorMessage error message for this state.
     */
    public ErrorState(final String name, final String errorMessage) {
        super(name);

        this.message = errorMessage;

        Transition transition = new RegularExpressionTransition(this, ".");
        super.addTransition(transition);
    }

    /**
     * Getter for error message.
     *
     * @return Error message for this state.
     */
    public String getErrorMessage() {
        return this.message;
    }

    /**
     * Setter for error message which can be {@code null}.
     *
     * @param errorMessage error message to set.
     */
    public void setErrorMessage(final String errorMessage) {
        this.message = errorMessage;
    }

    /**
     * This method is deprecated because ErrorState cannot be initial. This 
     * method always throws {@link UnsupportedOperationException}.
     *
     * @param initial information whether state is initial.
     * 
     * @deprecated This method is deprecated because ErrorState must be always 
     * final.
     * 
     * @throws UnsupportedOperationException this operation isn't supported by 
     * ErrorState. ErrorState must be always final.
     */
    @Override
    @Deprecated
    protected void setInitial(final boolean initial) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns always {@code false} because ErrorState cannot be initial.
     *
     * @return Always {@code false}.
     */
    @Override
    public boolean isInitial() {
        return false;
    }

    /**
     * This method is deprecated because ErrorState must be always final. 
     * This method always throws {@link UnsupportedOperationException}.
     *
     * @param isFinal information whether state is final.
     * 
     * @deprecated This method is deprecated because ErrorState must be always 
     * final.
     * 
     * @throws UnsupportedOperationException this operation isn't supported by 
     * ErrorState. ErrorState must be always final.
     */
    @Override
    @Deprecated
    public void setFinal(final boolean isFinal) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns always {@code true} because ErrorState is always final.
     *
     * @return Always {@code true}.
     */
    @Override
    public boolean isFinal() {
        return true;
    }

    /**
     * This method is deprecated because ErrorState cannot contains any 
     * transitions into other states. This method always throws 
     * {@link UnsupportedOperationException}.
     *
     * @param transition transition to add.
     * 
     * @deprecated This method is deprecated because ErrorState cannot contains 
     * any transitions into other states.
     * 
     * @throws UnsupportedOperationException this operation isn't supported by 
     * ErrorState. ErrorState cannot contains any transitions into other states.
     */
    @Override
    @Deprecated
    protected void addTransition(final Transition transition) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * This method is deprecated because ErrorState cannot contains any 
     * transitions into other states. This method always throws 
     * {@link UnsupportedOperationException}.
     *
     * @param transition transition to remove.
     * 
     * @deprecated This method is deprecated because ErrorState cannot contains 
     * any transitions into other states.
     * 
     * @throws UnsupportedOperationException this operation isn't supported by 
     * ErrorState. ErrorState cannot contains any transitions into other states.
     */
    @Override
    @Deprecated
    protected void removeTransition(final Transition transition) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;

        int result = super.hashCode();
        result = prime * result + ((this.message == null) ? 0 : this.message.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (!super.equals(obj)) {
            return false;
        }

        if (!(obj instanceof ErrorState)) {
            return false;
        }

        ErrorState other = (ErrorState) obj;
        if (this.message == null) {
            if (other.message != null) {
                return false;
            }
        }
        else if (!this.message.equals(other.message)) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of error state that has the format:
     * 
     * <pre>
     *     ErrorState [name=${ErrorStateName}, message=${ErrorMessage}]
     * </pre>
     *
     * @return String value of error state.
     */
    @Override
    public String toString() {
        return "ErrorState [name=" + this.getName() +
                ", message=" + this.getErrorMessage() + "]";
    }
}
