/**
 * This Java file was created 25.10.2013 2:38:35.
 */
package system.automaton;

/**
 * Interface {@code TransitionListener} serves for listening events of 
 * transitions in finite automaton.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.1
 * 
 * @since 0.1.8
 */
public interface TransitionListener {
    /**
     * Returns owner of this listener (transition which has registered this 
     * listener).
     *
     * @return Transition which has registered this listener.
     */
    public Transition getTransition();

    /**
     * Sets owner of this listener (transition which has registered this 
     * listener).
     *
     * @param transition transition which has registered this listener.
     */
    public void setTransition(Transition transition);

    /**
     * Action that is being invoked in case of using the transition in the 
     * final automaton.
     *
     * @param source state from which the transition was made.
     * @param target state to which the transition has been made.
     * @param character character used to the transition.
     */
    public void transitionMade(State source, State target, char character);
}
