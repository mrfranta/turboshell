/**
 * This Java file was created 17.10.2013 8:36:07.
 */
package system.automaton;

import java.util.ArrayList;
import java.util.List;

import system.utils.StringUtilities;

/**
 * Class {@code State} represents state of finite automaton. State can be 
 * initial and/or final. State also contains transitions from this state into 
 * other states (transition can be type of self-transition which leads from and 
 * into this state). 
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.1.3
 */
public class State implements Comparable<State> {
    /** Name of state. */
    private String name;

    /** Information whether the state is initial. */
    private boolean initialState;
    /** Information whether the state is final. */
    private boolean finalState;

    /** List of transitions which lead from this state. */
    private List<Transition> transitions;
    /** List of listeners for this state. */
    private List<StateListener> listeners;

    /**
     * Constructs state of finite automaton with specified name.
     *
     * @param name state name which can't be {@code null} or empty.
     * 
     * @throws NullPointerException in the case of entering the state name 
     * which is {@code null}.
     * @throws IllegalArgumentException in the case of entering the state name 
     * which is {@code null} or is empty.
     */
    public State(final String name) {
        this.setName(name);

        this.initialState = false;
        this.finalState = false;

        this.transitions = new ArrayList<Transition>();
        this.listeners = new ArrayList<StateListener>();
    }

    /**
     * Returns name of this state.
     *
     * @return Name of this state.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets name of state. Method is protected because finite automaton should 
     * contain states with unique names.
     *
     * @param name state name which can't be {@code null} or empty.
     * 
     * @throws NullPointerException in the case of entering the state name 
     * which is {@code null}.
     * @throws IllegalArgumentException in the case of entering the state name 
     * which is empty.
     */
    protected void setName(final String name) {
        if (name == null) {
            throw new NullPointerException("Name of automaton " +
                    "state cannot be null");
        }

        if (StringUtilities.isEmpty(name)) {
            throw new IllegalArgumentException("Name of automaton " +
                    "state cannot be empty");
        }

        this.name = name;
    }

    /**
     * Returns information whether the state is initial.
     *
     * @return {@code true} if this state is initial; 
     *         {@code false} otherwise.
     */
    public boolean isInitial() {
        return this.initialState;
    }

    /**
     * Sets this state as initial.
     * 
     * <p>This method is protected because the non-deterministic automaton can 
     * have more initial states. So user can override this class and make 
     * this method visible. For deterministic automaton is important to 
     * changing initial state only by finite automaton.</p>
     *
     * @param bool information whether is state initial.
     */
    protected void setInitial(final boolean bool) {
        this.initialState = bool;
    }

    /**
     * Returns information whether the state is final.
     *
     * @return {@code true} if this state is final; 
     *         {@code false} otherwise.
     */
    public boolean isFinal() {
        return this.finalState;
    }

    /**
     * Sets this state as final.
     *
     * @param bool information whether is state final.
     */
    public void setFinal(final boolean bool) {
        this.finalState = bool;
    }

    /**
     * Adds specified transition from this state.
     *
     * @param transition transition to add.
     * @throws IllegalArgumentException in the case of entering the transition 
     * which has source state which isn't this state.
     */
    protected void addTransition(final Transition transition) {
        if (transition == null) {
            return;
        }

        if (transition.getSource() != this) {
            throw new IllegalArgumentException(
                    "Automaton transition contains wrong source automaton state");
        }

        this.transitions.add(transition);
    }

    /**
     * Removes specified transition from this state.
     *
     * @param transition transition to remove.
     */
    protected void removeTransition(final Transition transition) {
        this.transitions.remove(transition);
    }

    /**
     * Returns transitions from this state.
     *
     * @return Array of transitions from this state.
     */
    public Transition[] getTransitions() {
        Transition[] array = new Transition[this.transitions.size()];

        return this.transitions.toArray(array);
    }

    /**
     * Registers and adds specified state listener.
     *
     * @param listener state listener which will be added.
     */
    public void addListener(final StateListener listener) {
        if (listener == null) {
            return;
        }

        State state = listener.getState();
        if (state == this) {
            return;
        }

        if (state != null) {
            state.removeListener(listener);
        }

        this.listeners.add(listener);
        listener.setState(this);
    }

    /**
     * Unregisters and removes specified state listener.
     *
     * @param listener state listener which will be removed.
     */
    public void removeListener(final StateListener listener) {
        if (listener == null) {
            return;
        }

        if (this.listeners.contains(listener)) {
            this.listeners.remove(listener);
            listener.setState(null);
        }
    }

    /**
     * Returns all registered state listeners for this state.
     *
     * @return Array of registered state listeners.
     */
    public StateListener[] getListeners() {
        StateListener[] array = new StateListener[this.listeners.size()];

        return this.listeners.toArray(array);
    }

    /**
     * Invokes method {@link StateListener#entryState(State, char)} in all 
     * registered listeners in the case of using transition to entry 
     * into this state from specified state.
     *
     * @param source state from which the transition leads.
     * @param character character used to the transition.
     */
    protected void fireEntryState(final State source, final char character) {
        for (StateListener listener : this.listeners) {
            listener.entryState(source, character);
        }
    }

    /**
     * Invokes method {@link StateListener#leavingState(State, char)} in all 
     * registered listeners in the case of using transition to leave this state 
     * and entry into specified state.
     *
     * @param target target state to which the transition leads.
     * @param character character used to the transition.
     */
    protected void fireLeavingState(final State target, final char character) {
        for (StateListener listener : this.listeners) {
            listener.leavingState(target, character);
        }
    }

    /**
     * Compares this object with the specified object for order. Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param state the state to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     */
    // JDK1.5 compatible @Override
    public int compareTo(final State state) {
        if (state == null) {
            return 1;
        }

        String stateName = state.getName();

        return this.getName().compareTo(stateName);
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + (this.finalState ? 1231 : 1237);
        result = prime * result + (this.initialState ? 1231 : 1237);
        result = prime * result + ((this.listeners == null) ? 0 : this.listeners.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + ((this.transitions == null) ? 0 : this.transitions.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof State)) {
            return false;
        }

        State other = (State) obj;
        if (this.finalState != other.finalState) {
            return false;
        }

        if (this.initialState != other.initialState) {
            return false;
        }

        if (this.listeners == null) {
            if (other.listeners != null) {
                return false;
            }
        }
        else if (!this.listeners.equals(other.listeners)) {
            return false;
        }

        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        }
        else if (!this.name.equals(other.name)) {
            return false;
        }

        if (this.transitions == null) {
            if (other.transitions != null) {
                return false;
            }
        }
        else if (!this.transitions.equals(other.transitions)) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of state that has the format:
     * 
     * <pre>
     *     State [name=${NameOfState}, initialState=${IsInitialState}, finalState=${IsFinalState}]
     * </pre>
     *
     * @return String value of state.
     */
    @Override
    public String toString() {
        return "State [name=" + this.getName() + ", initialState=" +
                this.isInitial() + ", finalState=" + this.isFinal() + "]";
    }
}
