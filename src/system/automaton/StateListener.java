/**
 * This Java file was created 22.10.2013 1:04:57.
 */
package system.automaton;

/**
 * Interface {@code StateListener} serves for listening events of states 
 * in finite automaton.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.1.7
 */
public interface StateListener {
    /**
     * Returns owner of this listener (state which has registered this listener).
     *
     * @return State which has registered this listener.
     */
    public State getState();

    /**
     * Sets owner of this listener (state which has registered this listener).
     *
     * @param state state which has registered this listener.
     */
    public void setState(State state);

    /**
     * Action that is being invoked in case of using transition to entry 
     * into owner of this listener from specified state.
     *
     * @param sourceState state from which the transition leads.
     * @param character character used to the transition.
     */
    public void entryState(State sourceState, char character);

    /**
     * Action that is being invoked in case of using transition to leave 
     * owner of this listener and entry into specified state.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    public void leavingState(State targetState, char character);
}
