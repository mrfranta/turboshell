/**
 * This file was generated by the JavaTM Architecture for XML Binding(JAXB) 
 * Reference Implementation, v2.2.4-2. See <a href="http://java.sun.com/xml/jaxb">
 * http://java.sun.com/xml/jaxb</a>. Any modifications to this file will be lost 
 * upon recompilation of the source schema.
 *  
 * Generated on: 2013.10.25 at 02:20:41 AM CEST 
 */
package system.automaton.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for statesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained 
 * within this class.
 * 
 * <pre>
 * &lt;complexType name="statesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="state" type="{}stateType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="errorState" type="{}errorStateType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * @author Mr.FrAnTA (Michal Dékány)
 * @author JavaTM Architecture for XML Binding(JAXB)
 * @version 1.2
 * 
 * @since 0.1.4
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "statesType", propOrder = { "stateOrErrorState" })
public class StatesType {
    @XmlElements({
            @XmlElement(name = "state", type = StateType.class),
            @XmlElement(name = "errorState", type = ErrorStateType.class)
    })
    protected List<Object> stateOrErrorState;

    /**
     * Gets the value of the stateOrErrorState property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the 
     * stateOrErrorState property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStateOrErrorState().add(newItem);
     * </pre>
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StateType }
     * {@link ErrorStateType }
     */
    public List<Object> getStateOrErrorState() {
        if (this.stateOrErrorState == null) {
            this.stateOrErrorState = new ArrayList<Object>();
        }
        return this.stateOrErrorState;
    }
}
