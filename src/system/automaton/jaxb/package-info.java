/**
 * Package {@code system.automaton.jaxb} contains classes for work with XML 
 * files using the JavaTM Architecture for XML Binding(JAXB) and others 
 * important tools.
 */
package system.automaton.jaxb;

