/**
 * This Java file was created 19.10.2013 1:17:09.
 */
package system.automaton;

/**
 * Interface {@code FiniteAutomatonLoader} serves for loading finite automaton 
 * from files.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 *  
 * @since 0.1.5
 */
public interface FiniteAutomatonLoader {
    /**
     * Loads finite automaton from specified file.
     *
     * @param fileName name of file from which will be automaton loaded.
     * @return Loaded finite automaton from specified file.
     * @throws Exception if some error occurs during loading automaton.
     */
    public FiniteAutomaton loadAutomaton(String fileName) throws Exception;
}
