/**
 * This Java file was created 31.10.2013 12:22:28.
 */
package system.automaton;

/**
 * Abstract class {@code TransitionAdapter} implements {@link TransitionListener}
 * and represents adapter for listeners. This adapter is appropriate for a 
 * simpler implementation of transition listeners.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.2.2
 */
public abstract class TransitionAdapter implements TransitionListener {
    /** Owner of this listener. */
    private Transition transition;

    /**
     * Constructs adapter (listener) for any transition.
     */
    public TransitionAdapter() {
        this.transition = null;
    }

    /**
     * Constructs adapter (listener) for specified transition.
     * 
     * @param transition owner of transition listener.
     */
    public TransitionAdapter(final Transition transition) {
        this.transition = transition;
    }

    /**
     * Returns owner of this listener (transition which has registered this 
     * listener).
     *
     * @return Transition which has registered this listener.
     */
    // JDK1.5 compatible @Override
    public Transition getTransition() {
        return this.transition;
    }

    /**
     * Sets owner of this listener (transition which has registered this 
     * listener).
     *
     * @param transition transition which has registered this listener.
     */
    // JDK1.5 compatible @Override
    public void setTransition(final Transition transition) {
        this.transition = transition;
    }

    /**
     * Action that is being invoked in case of using the transition in the 
     * final automaton.
     *
     * @param source state from which the transition was made.
     * @param target state to which the transition has been made.
     * @param character character used to the transition.
     */
    // JDK1.5 compatible @Override
    public void transitionMade(final State source,
                               final State target,
                               final char character) {
    }
}
