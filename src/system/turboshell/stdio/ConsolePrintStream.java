/**
 * This Java file was created 27.10.2013 22:40:56.
 */
package system.turboshell.stdio;

import java.io.PrintStream;

import system.turboshell.gui.ShellConsole;

/**
 * Class {@code ConsolePrintStream} represents standard output stream. All data 
 * written into this stream are printed into shell console.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.10
 * 
 * @since 0.1.9
 */
public class ConsolePrintStream extends PrintStream {
    /** Shell console which prints data from stream. */
    private ShellConsole console;

    /**
     * Constructs print stream for specified console.
     *
     * @param console shell console which will prints data from stream.
     * 
     * @throws NullPointerException whether is entered {@code null} as 
     * parameter for shell console.
     */
    public ConsolePrintStream(final ShellConsole console) {
        super(new ConsoleOutputStream(console));

        this.console = console;
    }

    /**
     * Returns shell console which prints data from stream.
     *
     * @return Shell console prints data from stream.
     */
    protected ShellConsole getConsole() {
        return this.console;
    }

    /**
     * Cleans up data written into stream (cleans up console).
     */
    public void clear() {
        this.getConsole().clear();
    }

    /**
     * Flushes the stream if can be flushed. This is done by writing any 
     * buffered output bytes to the underlying output stream and then flushing 
     * that stream.
     * 
     * @see system.turboshell.gui.ShellConsole#canCloseStream()
     */
    @Override
    public void flush() {
        if (this.getConsole().canCloseStream()) {
            super.flush();
        }
    }

    /**
     * Closes the stream if can be closed. This is done by flushing the stream 
     * and then closing the underlying output stream.
     * 
     * @see system.turboshell.gui.ShellConsole#canCloseStream()
     */
    @Override
    public void close() {
        if (this.getConsole().canCloseStream()) {
            super.close();
        }
    }
}
