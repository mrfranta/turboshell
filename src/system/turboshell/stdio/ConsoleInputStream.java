/**
 * This Java file was created 17.11.2013 21:30:02.
 */
package system.turboshell.stdio;

import java.io.InputStream;

import system.turboshell.gui.ShellConsole;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessPosition;

/**
 * Class {@code ConsoleInputStream} represents standard input stream from 
 * console. This input stream contains string buffer which stores data provided 
 * by console and which can be read. Console stores data using overloaded 
 * method {@link #append(String)}.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.5
 * 
 * @since 0.4.12
 */
public class ConsoleInputStream extends InputStream {
    /** Console which provides this standard input. */
    private ShellConsole console;
    /** Buffer for this input stream which contains data provided by console. */
    private StringBuffer buffer;
    /** Information whether is input stream closed. */
    private boolean closed;

    /**
     * Constructs input stream for specified shell console.
     *
     * @param shellConsole instance of shell console which provides this 
     * standard input. It can be {@code null}.
     */
    public ConsoleInputStream(final ShellConsole shellConsole) {
        this.console = shellConsole;
        this.buffer = new StringBuffer();
        this.closed = false;
    }

    /**
     * Reads the next char of data from the input stream. The value char is
     * returned as an <code>int</code> in the range <code>0</code> to
     * <code>0xFFFF</code>. If no char is available because the end of the stream
     * has been reached, the value <code>-1</code> is returned. This method
     * blocks until input data is available, the end of the stream is detected.
     *
     * @return the next char of data, or <code>-1</code> if the end of the 
     * stream is reached.
     */
    @Override
    public synchronized int read() {
        try {
            Thread thread = Thread.currentThread();
            if (thread instanceof ShellProcess) {
                while (((ShellProcess) thread).getPosition() == ShellProcessPosition.BACKGROUND) {
                    this.wait();
                }
            }

            while (this.buffer.length() == 0 && !this.closed) {
                this.wait();
            }
        } catch (InterruptedException exc) {
            return -1;
        }

        if (this.closed) {
            return -1;
        }

        char character = this.buffer.charAt(0);
        this.buffer.deleteCharAt(0);

        return character;
    }

    /**
     * Appends the specified char into this input stream. The general contract 
     * for <code>append</code> is that one char is written to the output stream.
     * The char to be written is the 16 low-order bits of the argument 
     * <code>c</code>. The 16 high-order bits of <code>c</code> are ignored.
     *
     * @param c the <code>char</code>.
     */
    public synchronized void append(final int c) {
        if (!this.closed) {
            this.buffer.append((char) c);
            this.notifyAll();
        }
    }

    /**
     * Appends <code>b.length</code> bytes from the specified byte array into 
     * this input stream. The general contract for <code>append(b)</code> is 
     * that it have exactly the same effect as the call <code>append(b, 0, 
     * b.length)</code>.
     *
     * @param b the data.
     */
    public synchronized void append(final byte[] b) {
        this.append(b, 0, b.length);
    }

    /**
     * Appends <code>len</code> bytes from the specified byte array starting 
     * at offset <code>off</code> into this input stream. The general contract 
     * for <code>append(b, off, len)</code> is that some of the bytes in the 
     * array <code>b</code> are appended into the input stream in order; 
     * element <code>b[off]</code> is the first byte appended and 
     * <code>b[off+len-1]</code> is the last byte appended by this operation.
     *
     * @param b the data.
     * @param offset the start offset in the data.
     * @param len the number of bytes to append.
     */
    public synchronized void append(final byte[] b,
                                    final int offset,
                                    final int len) {
        if (b == null) {
            return;
        }

        if (!this.closed) {
            this.buffer.append(new String(b, offset, len));
            this.notifyAll();
        }
    }

    /**
     * Appends specified string into this input stream.
     *
     * @param str string which will be appended into this input stream.
     */
    public synchronized void append(final String str) {
        if (str == null) {
            return;
        }

        if (!this.closed) {
            this.buffer.append(str);
            this.notifyAll();
        }
    }

    /**
     * Closes this input stream and releases any system resources associated
     * with the stream.
     */
    @Override
    public synchronized void close() {
        if (this.console == null) {
            if (!this.closed) {
                this.closed = true;
                this.buffer.setLength(0);
                this.notifyAll();
            }

            return;
        }

        if (this.console.canCloseStream() && !this.closed) {
            this.closed = true;
            this.buffer.setLength(0);
            this.notifyAll();
        }
    }
}
