/**
 * This Java file was created 27.10.2013 22:41:43.
 */
package system.turboshell.stdio;

import java.io.OutputStream;

import system.turboshell.gui.ShellConsole;

/**
 * Class {@code ConsoleOutputStream} represents an output stream  which accepts 
 * output bytes and sends them to shell console.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.10
 * 
 * @since 0.1.9
 */
public class ConsoleOutputStream extends OutputStream {
    /** Information whether is output stream closed. */
    private boolean closed;
    /** Text area for output stream. */
    private ShellConsole console;

    /**
     * <b>Constructor</b> constructs output stream into text area.
     *
     * @param console console for output stream.
     * 
     * @throws NullPointerException whether is entered {@code null} as 
     * parameter for shell console.
     */
    public ConsoleOutputStream(final ShellConsole console) {
        if (console == null) {
            throw new NullPointerException("Shell console cannot be null");
        }

        this.closed = false;
        this.console = console;
    }

    /**
     * Returns shell console which prints data from stream.
     *
     * @return Shell console prints data from stream.
     */
    protected ShellConsole getConsole() {
        return this.console;
    }

    /**
     * Writes the specified char to this output stream. The general contract 
     * for <code>write</code> is that one char is written to the output stream. 
     * The char to be written is the 16 low-order bits of the argument 
     * <code>c</code>. The 16 high-order bits of <code>c</code> are ignored.
     * 
     * @param c the <code>char</code>.
     */
    @Override
    public void write(final int c) {
        if (!this.closed) {
            this.console.streamAppend(String.valueOf((char) c));
        }
    }

    /**
     * Writes <code>b.length</code> bytes from the specified byte array
     * to this output stream. The general contract for <code>write(b)</code>
     * is that it should have exactly the same effect as the call
     * <code>write(b, 0, b.length)</code>.
     *
     * @param b the data.
     */
    @Override
    public void write(final byte[] b) {
        this.write(b, 0, b.length);
    }

    /**
     * Writes <code>len</code> bytes from the specified byte array
     * starting at offset <code>off</code> to this output stream.
     * The general contract for <code>write(b, off, len)</code> is that
     * some of the bytes in the array <code>b</code> are written to the
     * output stream in order; element <code>b[off]</code> is the first
     * byte written and <code>b[off+len-1]</code> is the last byte written
     * by this operation.
     *
     * @param b the data.
     * @param off the start offset in the data.
     * @param len the number of bytes to write.
     */
    @Override
    public void write(final byte[] b, final int off, final int len) {
        if (!this.closed) {
            this.console.streamAppend(new String(b, off, len));
        }
    }

    /**
     * Cleans up data written into stream (cleans up console).
     */
    public void clear() {
        this.console.clear();
    }

    /**
     * Closes this output stream and releases any system resources
     * associated with this stream. The general contract of <code>close</code>
     * is that it closes the output stream. A closed stream cannot perform
     * output operations and cannot be reopened.
     */
    @Override
    public void close() {
        if (!this.closed && this.console.canCloseStream()) {
            this.closed = true;
        }
    }
}
