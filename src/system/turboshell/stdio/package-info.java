/**
 * Package {@code system.turboshell.stdio} contains streams for standard input 
 * and output streams.
 * 
 * @see java.io.InputStream
 * @see java.io.PrintStream
 */
package system.turboshell.stdio;

