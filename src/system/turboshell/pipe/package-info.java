/**
 * Package {@code system.turboshell.pipe} contains streams for making pipes 
 * between two shell commands or processes.
 */
package system.turboshell.pipe;

