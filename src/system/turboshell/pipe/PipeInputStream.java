/**
 * This Java file was created 3.12.2013 18:29:41.
 */
package system.turboshell.pipe;

import java.io.IOException;
import java.io.InputStream;


/**
 * Class {@code PipeInputStream} represents input stream should be connected to 
 * a pipe output stream; the pipe input stream then provides whatever data bytes
 * are written to the pipe output stream. Typically, data is read from a 
 * <code>PipeInputStream</code> object by one thread  and data is written to the 
 * corresponding <code>PipeOutputStream</code> by some other thread.
 * 
 * <p>This input stream contains string buffer which stores data provided by 
 * output stream and which can be read. Output stream stores data using 
 * overloaded method {@link #append(String)}.</p>
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.7.6
 */
public class PipeInputStream extends InputStream {
    /** Buffer for this input stream which contains data provided by pipe output. */
    private StringBuffer buffer;
    /** Information whether is input stream closed. */
    private boolean closed;
    /** Information whether in input stream connected. */
    private boolean connected;

    /**
     * Constructs a <code>PipeInputStream</code> so that it is not yet 
     * {@linkplain #connect(PipeOutputStream) connected}.
     * 
     * It must be {@linkplain PipeOutputStream#connect(PipeInputStream) 
     * connected} to a <code>PipeOutputStream</code> before being used.
     */
    public PipeInputStream() {
        this.buffer = new StringBuffer();
        this.closed = false;
    }

    /**
     * Constructs a <code>PipeInputStream</code> so that it is connected to 
     * the pipe output stream <code>source</code>. Data bytes written to 
     * <code>source</code> will then be  available as input from this stream.
     *
     * @param source source pipe for this input.
     * @throws IOException if an I/O error occurs during pipe connecting.
     */
    public PipeInputStream(final PipeOutputStream source) throws IOException {
        this();

        this.connect(source);
    }

    /**
     * Returns information whether is this pipe closed.
     *
     * @return {@code true} whether is this pipe closed; {@code false} otherwise.
     */
    public boolean isClosed() {
        return this.closed;
    }

    /**
     * Returns information whether is this pipe connected.
     *
     * @return {@code true} whether is this pipe connected; 
     * {@code false} otherwise.
     */
    public boolean isConnected() {
        return this.connected;
    }

    /**
     * Connects this pipe input with specified source pipe.
     *
     * @param source source pipe to connect.
     * @throws IOException if an I/O error occurs during pipe connecting.
     */
    public synchronized void connect(final PipeOutputStream source) throws IOException {
        if (source == null) {
            throw new NullPointerException("Source cannot be null");
        }

        source.connect(this);
        this.connected = true;
    }

    /**
     * Disconnects connected source pipe.
     */
    protected synchronized void disconnect() {
        this.connected = false;
        this.notifyAll();
    }

    /**
     * Reads the next char of data from the input stream. The value char is
     * returned as an <code>int</code> in the range <code>0</code> to
     * <code>0xFFFF</code>. If no char is available because the end of the stream
     * has been reached, the value <code>-1</code> is returned. This method
     * blocks until input data is available, the end of the stream is detected.
     *
     * @return the next char of data, or <code>-1</code> if the end of the 
     * stream is reached.
     * @throws IOException whether is pipe closed. 
     */
    @Override
    public synchronized int read() throws IOException {
        if (this.closed) {
            throw new IOException("Pipe is closed");
        }

        try {
            while (this.buffer.length() == 0 && !this.closed) {
                if (!this.connected) {
                    return -1;
                }

                this.wait();
            }
        } catch (InterruptedException exc) {
            return -1;
        }

        if (this.closed) {
            return -1;
        }

        char character = this.buffer.charAt(0);
        this.buffer.deleteCharAt(0);

        return character;
    }

    /**
     * Appends the specified char into this input stream. The general contract 
     * for <code>append</code> is that one char is written to the output stream.
     * The char to be written is the 16 low-order bits of the argument 
     * <code>c</code>. The 16 high-order bits of <code>c</code> are ignored.
     *
     * @param c the <code>char</code>.
     * @throws IOException whether is pipe closed. 
     */
    protected synchronized void append(final int c) throws IOException {
        if (this.closed) {
            throw new IOException("Pipe is closed");
        }

        this.buffer.append((char) c);
        this.notifyAll();
    }

    /**
     * Appends <code>b.length</code> bytes from the specified byte array into 
     * this input stream. The general contract for <code>append(b)</code> is 
     * that it have exactly the same effect as the call <code>append(b, 0, 
     * b.length)</code>.
     *
     * @param b the data.
     * @throws IOException whether is pipe closed.
     */
    protected synchronized void append(final byte[] b) throws IOException {
        this.append(b, 0, b.length);
    }

    /**
     * Appends <code>len</code> bytes from the specified byte array starting 
     * at offset <code>off</code> into this input stream. The general contract 
     * for <code>append(b, off, len)</code> is that some of the bytes in the 
     * array <code>b</code> are appended into the input stream in order; 
     * element <code>b[off]</code> is the first byte appended and 
     * <code>b[off+len-1]</code> is the last byte appended by this operation.
     *
     * @param b the data.
     * @param offset the start offset in the data.
     * @param len the number of bytes to append.
     * @throws IOException whether is pipe closed.
     */
    protected synchronized void append(final byte[] b,
                                       final int offset,
                                       final int len) throws IOException {
        if (this.closed) {
            throw new IOException("Pipe is closed");
        }

        if (b == null) {
            return;
        }

        this.buffer.append(new String(b, offset, len));
        this.notifyAll();
    }

    /**
     * Appends specified string into this input stream.
     *
     * @param str string which will be appended into this input stream.
     * @throws IOException whether is pipe closed.
     */
    protected synchronized void append(final String str) throws IOException {
        if (this.closed) {
            throw new IOException("Pipe is closed");
        }

        if (str == null) {
            return;
        }

        this.buffer.append(str);
        this.notifyAll();
    }

    /**
     * Closes this input stream and releases any system resources associated
     * with the stream.
     *
     * @see java.io.InputStream#close()
     */
    @Override
    public synchronized void close() {
        if (!this.closed) {
            this.closed = true;
            this.buffer.setLength(0);
            this.notifyAll();
        }
    }
}