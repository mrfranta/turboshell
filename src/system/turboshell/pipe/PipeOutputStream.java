/**
 * This Java file was created 3.12.2013 18:28:45.
 */
package system.turboshell.pipe;

import java.io.IOException;
import java.io.OutputStream;


/**
 * Class {@code PipeOutputStream} represents output stream which can be 
 * connected to a pipe input stream to create a communications pipe. 
 * The pipe output stream is the sending end of the pipe. Typically, data is 
 * written to a <code>PipeOutputStream</code> object by one thread and data is 
 * read from the connected <code>PipeInputStream</code> by some other thread. 
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.5
 * 
 * @since 0.7.6
 */
public class PipeOutputStream extends OutputStream {
    /** Connected sink pipe. */
    private PipeInputStream sink;

    /**
     * Creates a pipe output stream that is not yet connected to a pipe input 
     * stream. It must be connected to a pipe input stream, either by the 
     * receiver or the sender, before being used. 
     *
     * @see PipeInputStream#connect(PipeOutputStream)
     * @see PipeOutputStream#connect(PipeInputStream)
     */
    public PipeOutputStream() {
        this.sink = null;
    }

    /**
     * Creates a pipe output stream connected to the specified pipe input 
     * stream. Data bytes written to this stream will then be available as 
     * input from <code>sink</code>.
     *
     * @param sink sink pipe for connection.
     * @throws NullPointerException if a sink is {@code null}.
     * @throws IOException if an I/O error occurs during pipe connecting.
     */
    public PipeOutputStream(final PipeInputStream sink) throws IOException {
        this.connect(sink);
    }

    /**
     * Connects this output pipe with specified sink pipe.
     *
     * @param snk sink pipe for connection.
     * @throws NullPointerException if a snk is {@code null}.
     * @throws IOException if an I/O error occurs during pipe connecting.
     */
    public synchronized void connect(final PipeInputStream snk) throws IOException {
        if (snk == null) {
            throw new NullPointerException("Sink pipe cannot be null");
        }

        if (snk.isClosed()) {
            throw new IOException("Pipe is closed");
        }

        if (snk.isConnected() || this.sink != null) {
            throw new IOException("Pipe is already connected");
        }

        this.sink = snk;
    }

    /**
     * Returns information whether is this pipe connected.
     *
     * @return {@code true} whether is this pipe connected; 
     * {@code false} otherwise.
     */
    public boolean isConnected() {
        return (this.sink != null);
    }

    /**
     * Writes the specified char to this output stream. The general contract 
     * for <code>write</code> is that one char is written to the output stream. 
     * The char to be written is the 16 low-order bits of the argument 
     * <code>c</code>. The 16 high-order bits of <code>c</code> are ignored.
     * 
     * @param c the <code>char</code>.
     * @throws IOException if the pipe is {@link #connect(PipeInputStream) 
     * unconnected}, closed, or if an I/O error occurs.
     */
    @Override
    public void write(final int c) throws IOException {
        if (this.sink == null) {
            throw new IOException("Pipe not connected");
        }

        this.sink.append(c);
    }

    /**
     * Writes <code>len</code> bytes from the specified byte array
     * starting at offset <code>off</code> to this output stream.
     * The general contract for <code>write(b, off, len)</code> is that
     * some of the bytes in the array <code>b</code> are written to the
     * output stream in order; element <code>b[off]</code> is the first
     * byte written and <code>b[off+len-1]</code> is the last byte written
     * by this operation.
     *
     * @param b the data.
     * @param off the start offset in the data.
     * @param len the number of bytes to write.
     * @throws IOException if the pipe is {@link #connect(PipeInputStream) 
     * unconnected}, closed, or if an I/O error occurs.
     */
    @Override
    public void write(final byte[] b, final int off, final int len) throws IOException {
        if (this.sink == null) {
            throw new IOException("Pipe not connected");
        }
        else if (b == null) {
            return;
        }
        else if (len == 0) {
            return;
        }

        this.sink.append(b, off, len);
    }

    /**
     * Closes this pipe output stream and releases any system resources 
     * associated with this stream. This stream may no longer be used for 
     * writing bytes.
     */
    @Override
    public void close() {
        if (this.sink != null) {
            this.sink.disconnect();
            this.sink = null;
        }
    }
}
