/**
 * This Java file was created 15.11.2013 20:35:30.
 */
package system.turboshell;

import system.utils.StringUtilities;

/**
 * Class {@code ShellLoginAuthentication}
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.4.11
 */
public class ShellLoginAuthentication {
    public boolean authenticate(final String userName, final String password) {
        if (userName == null) {
            return false;
        }

        String trimmed = userName.replaceAll(" ", "");
        if (StringUtilities.isEmpty(trimmed)) {
            return false;
        }

        return true;
    }
}
