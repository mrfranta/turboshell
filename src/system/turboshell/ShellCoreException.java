/**
 * This Java file was created 26.10.2013 18:21:37.
 */
package system.turboshell;

/**
 * Class {@code ShellCoreException} represents exception for shell core.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @author pkopac (Petr Kopač)
 * @version 1.1
 * 
 * @since 0.1.8
 */
public class ShellCoreException extends Exception {
    /** 
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a href=
     * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
     * details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 2L;

    /** 
     * Constructs exception for shell core with specified message.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     */
    public ShellCoreException(final String message) {
        super(message);
    }

    /** 
     * Constructs exception for shell core with specified cause.
     *
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method). (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellCoreException(final Throwable cause) {
        super(cause);
    }

    /** 
     * Constructs exception for shell core with specified message and cause.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method).  (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellCoreException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
