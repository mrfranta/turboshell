/**
 * This Java file was created 29.11.2013 7:28:00.
 */
package system.turboshell.process;

import java.util.Comparator;

/**
 * Class {@code ShellProcessPIDComparator} compares two processes for order by 
 * process PID (Process identifier). This comparator can be passed to a sort 
 * method (such as <tt>Collections.sort</tt> or <tt>Arrays.sort</tt>) to allow 
 * precise control over the sort order. Comparator can also be used to control 
 * the order of certain data structures (such as <tt>TreeSet</tt> or 
 * <tt>TreeMap</tt>).
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.6.10
 */
public class ShellProcessPIDComparator implements Comparator<ShellProcess> {
    /**
     * Compares its two processes for order by PID. Returns a negative integer,
     * zero, or a positive integer as the first argument is less than, equal
     * to, or greater than the second.
     *
     * @param p1 the first shell process to be compared.
     * @param p2 the second shell process to be compared.
     * @return a negative integer, zero, or a positive integer as the first 
     * argument is less than, equal to, or greater than the second. 
     */
    public int compare(final ShellProcess p1, final ShellProcess p2) {
        if (p1 == null) {
            if (p2 == null) {
                return 0;
            }

            return -1;
        }

        if (p2 == null) {
            return 1;
        }

        long pid1 = p1.getPID();
        long pid2 = p2.getPID();

        return (pid1 < pid2) ? -1 : ((pid1 == pid2) ? 0 : 1);
    }
};
