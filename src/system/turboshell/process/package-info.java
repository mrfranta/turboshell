/**
 * Package {@code system.turboshell.process} contains classes for 
 * implementation of processes in shell.
 */
package system.turboshell.process;

