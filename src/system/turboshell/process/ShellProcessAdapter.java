/**
 * This Java file was created 26.11.2013 12:55:33.
 */
package system.turboshell.process;

/**
 * Abstract class {@code ShellProcessAdapter} implements {@link 
 * ShellProcessListener} and represents adapter for listeners. 
 * This adapter is appropriate for a simpler implementation of 
 * shell process listeners.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.5.8
 */
public class ShellProcessAdapter implements ShellProcessListener {
    /**
     * Action that is being invoked in case of process creation.
     *
     * @param process process which was created.
     */
    // JDK1.5 compatible @Override
    public void processCreated(final ShellProcess process) {
    }

    /**
     * Action that is being invoked in case of process got ready.
     *
     * @param process process which got ready.
     */
    // JDK1.5 compatible @Override
    public void processReady(final ShellProcess process) {
    }

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    // JDK1.5 compatible @Override
    public void processStarted(final ShellProcess process) {
    }

    /**
     * Action that is being invoked in case of process was killed.
     *
     * @param process process which was killed.
     */
    // JDK1.5 compatible @Override
    public void processKilled(final ShellProcess process) {
    }

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    // JDK1.5 compatible @Override
    public void processFinished(final ShellProcess process) {
    }

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    // JDK1.5 compatible @Override
    public void processMovedIntoForeground(final ShellProcess process) {
    }

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    // JDK1.5 compatible @Override
    public void processMovedIntoBackground(final ShellProcess process) {
    }
}
