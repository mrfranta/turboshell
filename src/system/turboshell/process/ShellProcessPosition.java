/**
 * This Java file was created 12.11.2013 3:28:33.
 */
package system.turboshell.process;

/**
 * Enumeration {@code ShellProcessPosition} represents position of shell 
 * process which can be {@code foreground} or {@code background}.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.6.10
 */
public enum ShellProcessPosition {
    /** Position for foreground shell process. */
    FOREGROUND("foreground"),
    /** Position for background shell process. */
    BACKGROUND("background");

    /** String value representing position. */
    private String position;

    /**
     * Constructs field of enumeration.
     *
     * @param position string representing shell process position.
     */
    private ShellProcessPosition(final String position) {
        this.position = position;
    }

    /**
     * Returns string value of shell process position.
     *
     * @return String value of shell process position.
     */
    public String getPosition() {
        return this.position;
    }

    /**
     * Returns string value of shell process position.
     *
     * @return String value of shell process position.
     */
    @Override
    public String toString() {
        return this.position + " process";
    }

    /**
     * Returns shell process position which has specified string value.
     *
     * @param value string value of shell process position.
     * @return Shell process position with specified string value or 
     * {@code null} for invalid value.
     */
    public static ShellProcessPosition fromValue(final String value) {
        for (ShellProcessPosition scp : ShellProcessPosition.values()) {
            if (scp.getPosition().equals(value)) {
                return scp;
            }
        }

        return null;
    }
}
