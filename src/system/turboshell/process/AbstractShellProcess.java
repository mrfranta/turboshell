/**
 * This Java file was created 11.11.2013 12:44:00.
 */
package system.turboshell.process;

import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code AbstractShellProcess} is {@code ShellProcess} which uses class 
 * {@code Thread} for running. Also this class implements a lot of methods from 
 * interface ShellProcess. All processes in this system should extends this 
 * class because there is right implementation of most of shell process methods.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.19
 * 
 * @since 0.4.0
 */
public abstract class AbstractShellProcess extends Thread implements ShellProcess {
    /** Shell process {@code pid}. */
    private long pid;
    /** Status which represents life cycle of shell process. */
    private ShellProcessStatus status;
    /** Position of shell process. */
    private ShellProcessPosition position;
    /** Information whether was process killed. */
    protected boolean killed;
    /** Registered listeners of this shell process. */
    private List<ShellProcessListener> listeners;

    /** Static variable for id numbers for shell processes. */
    private static int processID;

    /**
     * Constructs abstract shell process with {@code pid = -1} and process 
     * status {@code created}.
     */
    public AbstractShellProcess() {
        this.pid = -1;
        this.killed = false;
        this.listeners = new ArrayList<ShellProcessListener>();

        this.setStatus(ShellProcessStatus.CREATED);
        this.setPosition(ShellProcessPosition.FOREGROUND);

        this.setName("ShellProcess-" + AbstractShellProcess.processID++);
    }

    /**
     * Sets {@code pid} (Process identifier) of this shell process.
     *
     * @param pid long number for {@code pid} (Process identifier) of this 
     * shell process.
     */
    // JDK1.5 compatible @Override
    public void setPID(final long pid) {
        this.pid = pid;
    }

    /**
     * Returns {@code pid} (Process identifier) of this shell process.
     *
     * @return Long number representing {@code pid} (Process identifier) of 
     * this shell process.
     */
    // JDK1.5 compatible @Override
    public long getPID() {
        return this.pid;
    }

    /**
     * Runs method for thread which calls {@link #processMethod()}.
     */
    @Override
    public synchronized void run() {
        this.setStatus(ShellProcessStatus.RUNNING);
        this.processMethod();
        this.setStatus(ShellProcessStatus.FINISHED);
    }

    /**
     * Main method of this shell process.
     */
    public abstract void processMethod();

    /**
     * Returns status which represents life cycle of process. Real process has 
     * more statuses but processes in shell has only these statuses:
     * 
     * <pre>
     *     created -> ready -> running -> finished
     * </pre>
     *
     * @return Status which represents life cycle of process.
     */
    // JDK1.5 compatible @Override
    public ShellProcessStatus getStatus() {
        return this.status;
    }

    /**
     * Sets status which represents life cycle of process. Real process has 
     * more statuses but processes in shell has only these statuses:
     * 
     * <pre>
     *     created -> ready -> running -> finished
     * </pre>
     *
     * @param status status which represents life cycle of process.
     */
    // JDK1.5 compatible @Override
    public void setStatus(final ShellProcessStatus status) {
        if (status == null) {
            return;
        }

        if (status == this.status) {
            return;
        }

        this.status = status;

        switch (status) {
            case CREATED:
                this.fireProcessCreated();
                break;

            case READY:
                this.fireProcessReady();
                break;

            case RUNNING:
                this.fireProcessStarted();
                break;

            case FINISHED:
                if (this.isKilled()) {
                    this.fireProcessKilled();
                }
                else {
                    this.fireProcessFinished();
                }
                break;
        }
    }

    /**
     * Registers and adds specified shell process listener.
     *
     * @param listener shell process listener which will be added.
     */
    public void addListener(final ShellProcessListener listener) {
        if (listener != null) {
            this.listeners.add(listener);
        }
    }

    /**
     * Unregisters and removes specified shell process listener.
     *
     * @param listener shell process listener which will be removed.
     */
    public void removeListener(final ShellProcessListener listener) {
        if (listener != null) {
            this.listeners.remove(listener);
        }
    }

    /**
     * Returns all registered listeners for this shell process.
     *
     * @return Array of registered shell process listeners.
     */
    public ShellProcessListener[] getListeners() {
        ShellProcessListener[] array = new ShellProcessListener[this.listeners.size()];

        return this.listeners.toArray(array);
    }

    /**
     * Invokes method {@link ShellProcessListener#processCreated(ShellProcess) 
     * processCreated} in all registered listeners in the case of this process 
     * is created.
     */
    protected void fireProcessCreated() {
        for (ShellProcessListener listener : this.listeners) {
            listener.processCreated(this);
        }
    }

    /**
     * Invokes method {@link ShellProcessListener#processReady(ShellProcess) 
     * processReady} in all registered listeners in the case of this process 
     * is get ready.
     */
    protected void fireProcessReady() {
        for (ShellProcessListener listener : this.listeners) {
            listener.processReady(this);
        }
    }

    /**
     * Invokes method {@link ShellProcessListener#processStarted(ShellProcess) 
     * processStarted} in all registered listeners in the case of this process 
     * is started its job.
     */
    protected void fireProcessStarted() {
        for (ShellProcessListener listener : this.listeners) {
            listener.processStarted(this);
        }
    }

    /**
     * Invokes method {@link ShellProcessListener#processKilled(ShellProcess) 
     * processKilled} in all registered listeners in the case of this process 
     * was killed.
     */
    protected void fireProcessKilled() {
        for (ShellProcessListener listener : this.listeners) {
            listener.processFinished(this);
        }
    }

    /**
     * Invokes method {@link ShellProcessListener#processFinished(ShellProcess) 
     * processFinished} in all registered listeners in the case of this process 
     * is finished its job.
     */
    protected void fireProcessFinished() {
        for (ShellProcessListener listener : this.listeners) {
            listener.processKilled(this);
        }
    }

    /**
     * Invokes method {@link ShellProcessListener#processMovedIntoForeground(
     * ShellProcess) processMovedIntoForeground} in all registered listeners 
     * in the case of this process is moved into foreground.
     */
    protected void fireProcessMovedIntoForeground() {
        for (ShellProcessListener listener : this.listeners) {
            listener.processMovedIntoForeground(this);
        }
    }

    /**
     * Invokes method {@link ShellProcessListener#processMovedIntoBackground(
     * ShellProcess) processMovedIntoBackground} in all registered listeners 
     * in the case of this process is moved into background.
     */
    protected void fireProcessMovedIntoBackground() {
        for (ShellProcessListener listener : this.listeners) {
            listener.processMovedIntoBackground(this);
        }
    }

    /**
     * Returns shell process position.
     *
     * @return Shell process position.
     */
    public ShellProcessPosition getPosition() {
        return this.position;
    }

    /**
     * Sets position of shell process. This method invokes method 
     * {@link ShellProcessListener#processMovedIntoBackground(ShellProcess) 
     * processMovedIntoBackground} in registered listeners for {@link 
     * ShellProcessPosition#BACKGROUND background} {@code position}. For
     * {@link ShellProcessPosition#FOREGROUND foreground} {@code position} 
     * is invoked method {@link ShellProcessListener#processMovedIntoForeground(
     * ShellProcess) processMovedIntoForeground}. {@code position = null} 
     * has no effect.
     *
     * @param position shell process position to set.
     */
    public void setPosition(final ShellProcessPosition position) {
        if (position == null) {
            return;
        }

        this.position = position;
    }

    /**
     * Kills shell process. This method invokes method 
     * {@link ShellProcessListener#processKilled(ShellProcess) processKilled} 
     * in registered listeners.
     */
    // JDK1.5 compatible @Override
    public void kill() {
        if (this.killed) {
            return;
        }

        if (this.getStatus() == ShellProcessStatus.RUNNING) {
            this.killed = true;
            this.interrupt();
            this.setStatus(ShellProcessStatus.FINISHED);
        }
    }

    /**
     * Returns information whether was process killed.
     *
     * @return information whether was process killed.
     */
    public boolean isKilled() {
        return this.killed;
    }

    /**
     * Compares this object with the specified object for order. Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param process process to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     */
    public int compareTo(final ShellProcess process) {
        if (process == null) {
            return 1;
        }

        int result = this.name().compareTo(process.name());
        if (result == 0) {
            long pid1 = this.getPID();
            long pid2 = process.getPID();

            return (pid1 < pid2) ? -1 : ((pid1 == pid2) ? 0 : 1);
        }

        return result;
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + (this.killed ? 1231 : 1237);
        result = prime * result + ((this.listeners == null) ? 0 : this.listeners.hashCode());
        result = prime * result + (int) (this.pid ^ (this.pid >>> 32));
        result = prime * result + ((this.position == null) ? 0 : this.position.hashCode());
        result = prime * result + ((this.status == null) ? 0 : this.status.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AbstractShellProcess)) {
            return false;
        }

        AbstractShellProcess other = (AbstractShellProcess) obj;
        if (this.killed != other.killed) {
            return false;
        }

        if (this.listeners == null) {
            if (other.listeners != null) {
                return false;
            }
        }
        else if (!this.listeners.equals(other.listeners)) {
            return false;
        }

        if (this.pid != other.pid) {
            return false;
        }

        if (this.position != other.position) {
            return false;
        }

        if (this.status != other.status) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of abstract shell process that has the format:
     * 
     * <pre>
     *     ShellProcess [pid=${pid}, name=${name}, status=${status}, position=${position}]
     * </pre>
     *
     * @return String value of abstract shell process.
     */
    @Override
    public String toString() {
        return "ShellProcess [pid=" + this.getPID() +
                ", name=" + this.name() +
                ", status=" + this.getStatus() + "," +
                ", position=" + this.getPosition().getPosition() + "]";
    }
}
