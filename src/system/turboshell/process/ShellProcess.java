/**
 * This Java file was created 11.11.2013 12:40:14.
 */
package system.turboshell.process;

/**
 * Interface {@code ShellProcess} represent single process in this shell system.
 * Each process has unique PID (Process identifier), name, status and position. 
 * Status represents life cycle of process ({@link ShellProcessStatus#CREATED
 * created} -&gt; {@link ShellProcessStatus#READY ready} -&gt; {@link 
 * ShellProcessStatus#RUNNING running} -&gt; {@link ShellProcessStatus#FINISHED
 * finished}). Position can be {@link ShellProcessPosition#FOREGROUND foreground} 
 * or {@link ShellProcessPosition#BACKGROUND background}.
 * 
 * <p>Shell process is started by method {@code start()} and can be killed using 
 * method {@code kill()}. Process also can registers and unregisters listeners.</p>
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.12
 * 
 * @since 0.4.0
 */
public interface ShellProcess extends Comparable<ShellProcess> {
    /**
     * Sets {@code pid} (Process identifier) of this shell process.
     *
     * @param pid long number for {@code pid} (Process identifier) of this 
     * shell process.
     */
    public void setPID(long pid);

    /**
     * Returns {@code pid} (Process identifier) of this shell process.
     *
     * @return Long number representing {@code pid} (Process identifier) of 
     * this shell process.
     */
    public long getPID();

    /**
     * Returns name of this shell process.
     *
     * @return Name of this shell process.
     */
    public String name();

    /**
     * Returns status which represents life cycle of process. Real process has 
     * more statuses but processes in shell has only these statuses:
     * 
     * <pre>
     *     created -&gt; ready -&gt; running -&gt; finished
     * </pre>
     *
     * @return Status which represents life cycle of process.
     */
    public ShellProcessStatus getStatus();

    /**
     * Sets status which represents life cycle of process. Real process has 
     * more statuses but processes in shell has only these statuses:
     * 
     * <pre>
     *     created -&gt; ready -&gt; running -&gt; finished
     * </pre>
     *
     * @param status status which represents life cycle of process.
     */
    public void setStatus(ShellProcessStatus status);

    /**
     * Returns shell process position.
     *
     * @return Shell process position.
     */
    public ShellProcessPosition getPosition();

    /**
     * Sets position of shell process.
     *
     * @param position shell process position to set.
     */
    public void setPosition(ShellProcessPosition position);

    /**
     * Starts shell process.
     */
    public void start();

    /**
     * Kills shell process.
     */
    public void kill();

    /**
     * Returns information whether was process killed.
     *
     * @return information whether was process killed.
     */
    public boolean isKilled();

    /**
     * Registers and adds specified shell process listener.
     *
     * @param listener shell process listener which will be added.
     */
    public void addListener(ShellProcessListener listener);

    /**
     * Unregisters and removes specified shell process listener.
     *
     * @param listener shell process listener which will be removed.
     */
    public void removeListener(ShellProcessListener listener);

    /**
     * Returns all registered listeners for this shell process.
     *
     * @return Array of registered shell process listeners.
     */
    public ShellProcessListener[] getListeners();
}
