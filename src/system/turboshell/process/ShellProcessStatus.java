/**
 * This Java file was created 15.11.2013 14:49:38.
 */
package system.turboshell.process;

/**
 * Enumeration {@code ShellProcessStatus} represents life cycle (status) of 
 * process. Real process has more statuses but processes in shell has only 
 * these statuses:
 * 
 * <pre>
 *     created -> ready -> running -> finished
 * </pre>
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.4.10
 */
public enum ShellProcessStatus {
    /** Status for created shell process. */
    CREATED("created"),
    /** Status for ready shell process which waiting for start of running. */
    READY("ready"),
    /** Status for running shell process. */
    RUNNING("running"),
    /** Status for finished or killed shell process. */
    FINISHED("finished");

    /** String value of shell process status for method {@link #toString()}. */
    private String status;

    /**
     * Constructs field of enumeration.
     *
     * @param status string representing shell process status.
     */
    private ShellProcessStatus(final String status) {
        this.status = status;
    }

    /**
     * Returns sring value of shell process status.
     *
     * @return String value of shell process status.
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Returns string value of shell process status.
     *
     * @return String value of shell process status.
     */
    @Override
    public String toString() {
        return this.status;
    }

    /**
     * Returns shell process status which has specified string value.
     *
     * @param value string value of shell process status.
     * @return Shell process status with specified string value or
     * {@code null} for invalid value.
     */
    public static ShellProcessStatus fromValue(final String value) {
        for (ShellProcessStatus sps : ShellProcessStatus.values()) {
            if (sps.getStatus().equals(value)) {
                return sps;
            }
        }

        return null;
    }
}
