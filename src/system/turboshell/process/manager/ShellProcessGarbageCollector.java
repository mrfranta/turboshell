/**
 * This Java file was created 24.11.2013 23:36:58.
 */
package system.turboshell.process.manager;

import system.turboshell.command.cmd.Sleep;

/**
 * Class {@code ShellProcessGarbageCollector} represents garbage collector 
 * which starts every 5 minutes and removes all finished shell processes from 
 * {@code ShellProcessTree}.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.5.2
 */
public class ShellProcessGarbageCollector extends Thread {
    /** Owner of garbage collector. */
    private ShellProcessManager manager;

    /**
     * Constructs garbage collector for shell processes.
     *
     * @param manager owner of garbage collector.
     */
    public ShellProcessGarbageCollector(final ShellProcessManager manager) {
        this.manager = manager;
    }

    /**
     * Run method of garbage collector, which is started every 5 minutes. GC 
     * calls {@link ShellProcessManager#removeFinishedProcesses()} method which 
     * removes all finished processes from {@code ShellProcessTree}.
     */
    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(5 * Sleep.MINUTE_IN_MILLIS);
                this.manager.removeFinishedProcesses();
            }
        } catch (InterruptedException exc) {
            // interrupted by Virtual Machine Manager
        }
    }
}
