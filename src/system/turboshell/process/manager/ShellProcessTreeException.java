/**
 * This Java file was created 26.10.2013 18:21:37.
 */
package system.turboshell.process.manager;

/**
 * Class {@code ShellProcessTreeException} represents exception for tree of 
 * shell processes.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.4.6
 */
public class ShellProcessTreeException extends Exception {
    /** 
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a href=
     * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
     * details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 3L;

    /** 
     * Constructs exception for tree of shell processes with specified message.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     */
    public ShellProcessTreeException(final String message) {
        super(message);
    }

    /** 
     * Constructs exception for tree of shell processes with specified cause.
     *
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method). (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellProcessTreeException(final Throwable cause) {
        super(cause);
    }

    /** 
     * Constructs exception for tree of shell processes with specified message 
     * and cause.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method).  (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellProcessTreeException(final String message,
                                     final Throwable cause) {
        super(message, cause);
    }
}
