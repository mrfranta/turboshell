/**
 * Package {@code system.turboshell.process.manager} contains classes which 
 * serves for shell process managing. Package contains process manager, process 
 * tree with tree nodes. Also contains garbage collector which cleans up 
 * process tree from finished processes.
 */
package system.turboshell.process.manager;

