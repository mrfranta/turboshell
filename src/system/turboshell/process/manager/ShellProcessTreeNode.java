/**
 * This Java file was created 6.11.2013 20:23:14.
 */
package system.turboshell.process.manager;

import java.util.ArrayList;
import java.util.List;

import system.turboshell.process.ShellProcess;

/**
 * Class {@code ShellProcessTreeNode} represents tree node in {@link 
 * ShellProcessTree}. This node stores one shell process. This shell process 
 * has one caller is represented by {@code parent} node. Only init process 
 * has no caller. For this process is {@code parent = null}. Also this shell 
 * process can has zero or more subprocesses which are represented by 
 * {@code children} (child nodes).
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.8
 * 
 * @since 0.4.0
 */
public class ShellProcessTreeNode {
    /** Shell process which is stored in this tree node. */
    private ShellProcess process;

    /** Parent node of this tree node. */
    private ShellProcessTreeNode parent;
    /** Children nodes of this tree node. */
    private List<ShellProcessTreeNode> children;

    /**
     * Constructs tree node which stores shell process in shell process tree.
     *
     * @param process process which will be stored.
     */
    public ShellProcessTreeNode(final ShellProcess process) {
        this(process, null);
    }

    /**
     * Constructs tree node which stores shell process in shell process tree.
     *
     * @param process process which will be stored.
     * @param parent parent node of this tree node.
     * 
     * @throws NullPointerException whether is entered {@code null} as shell 
     * process parameter.
     */
    public ShellProcessTreeNode(final ShellProcess process,
                                final ShellProcessTreeNode parent) {
        if (process == null) {
            throw new NullPointerException("Process cannot be null");
        }

        this.process = process;
        this.parent = parent;
        if (parent != null) {
            parent.addChild(this);
        }

        this.children = new ArrayList<ShellProcessTreeNode>();
    }

    /**
     * Returns shell process stored in this tree node.
     *
     * @return Shell process which is stored in this node.
     */
    public ShellProcess getProcess() {
        return this.process;
    }

    /**
     * Returns parent of this tree node.
     *
     * @return parent of this tree node.
     */
    public ShellProcessTreeNode getParent() {
        return this.parent;
    }

    /**
     * Sets parent of this tree node.
     *
     * @param parent tree node which will be parent of this node.
     * @throws IllegalArgumentException if entered parent node is {@code null}.
     */
    private void setParent(final ShellProcessTreeNode parent) {
        if (parent == this) {
            throw new IllegalArgumentException("Parent cannot be this node");
        }

        this.parent = parent;
    }

    /**
     * Adds specified process as new child of this tree node. The general 
     * contract for <code>addChild(process)</code> is that it should have 
     * exactly the same effect as the call <code>addChild(new 
     * ShellProcessTreeNode(process))</code>.
     *
     * @param process shell process to add.
     * @throws NullPointerException whether is entered {@code null} as shell 
     * process parameter.
     */
    public void addChild(final ShellProcess process) {
        this.addChild(new ShellProcessTreeNode(process));
    }

    /**
     * Adds specified tree node into this tree node (tree node will be child).
     *
     * @param child tree node which will be child of this tree node.
     */
    public void addChild(final ShellProcessTreeNode child) {
        if (child == null) {
            return;
        }

        child.setParent(this);
        this.children.add(child);
    }

    /**
     * Removes specified child from this tree node.
     *
     * @param child child node to remove.
     */
    public void removeChild(final ShellProcessTreeNode child) {
        if (child == null) {
            return;
        }

        if (this.children.remove(child)) {
            child.setParent(null);
        }
    }

    /**
     * Returns array which contains children of this tree node.
     *
     * @return Array of children of this tree node.
     */
    public ShellProcessTreeNode[] getChildren() {
        ShellProcessTreeNode[] array = new ShellProcessTreeNode[this.children.size()];

        return this.children.toArray(array);
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        ShellProcess parentProcess = this.parent.getProcess();

        result = prime * result + ((this.children == null) ? 0 : this.children.hashCode());
        result = prime * result + ((parentProcess == null) ? 0 : parentProcess.hashCode());
        result = prime * result + ((this.process == null) ? 0 : this.process.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof ShellProcessTreeNode)) {
            return false;
        }

        ShellProcessTreeNode other = (ShellProcessTreeNode) obj;
        if (this.children == null) {
            if (other.children != null) {
                return false;
            }
        }
        else if (!this.children.equals(other.children)) {
            return false;
        }

        if (this.parent == null) {
            if (other.parent != null) {
                return false;
            }
        }
        else if (!this.parent.equals(other.parent)) {
            return false;
        }

        if (this.process == null) {
            if (other.process != null) {
                return false;
            }
        }
        else if (!this.process.equals(other.process)) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of shell process tree node which has format:
     * 
     * <pre>
     *     ProcessTreeNode [process=${StringValueOfProcess}, parent=${StringValueOfProcessInParent}]
     * </pre>
     *
     * @return String value of tree node.
     */
    @Override
    public String toString() {
        String parentStr = null;
        if (this.parent != null) {
            parentStr = this.parent.getProcess() + "";
        }

        return "ProcessTreeNode [process=" + this.getProcess() +
                ", parent=" + parentStr + "]";
    }
}
