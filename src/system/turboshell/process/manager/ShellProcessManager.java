/**
 * This Java file was created 21.10.2013 15:48:55.
 */
package system.turboshell.process.manager;

import java.util.Comparator;

import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessNameComparator;
import system.turboshell.process.ShellProcessPIDComparator;
import system.turboshell.process.ShellProcessStatus;

/**
 * Class {@code ShellProcessManager} represents process manager which manage 
 * shell processes in system. Process manager uses process tree for storing 
 * running processes.
 * 
 * <p>Process manager also starts processes after they are added into shell 
 * process tree.</p>
 * 
 * <p>It also contains garbage collector which starts every 5 minutes and 
 * removes all finished shell processes from process tree.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.22
 *
 * @since 0.4.0
 */
public class ShellProcessManager {
    /** Tree structure which contains tree nodes with shell processes. */
    private ShellProcessTree tree;
    /** 
     * Garbage collector which cleans up shell process tree from finished 
     * processes.
     */
    private ShellProcessGarbageCollector gc;

    /** 
     * Static variable for counting unique {@code pid's} (Process identifier) 
     * of added shell processes.
     */
    private static long pidCounter = 0;

    /**
     * Constructs process manager with specified root process.
     *
     * @param rootProcess root process for process tree.
     */
    public ShellProcessManager(final ShellProcess rootProcess) {
        rootProcess.setPID(++ShellProcessManager.pidCounter);
        this.tree = new ShellProcessTree(rootProcess);

        this.gc = new ShellProcessGarbageCollector(this);
        this.gc.start();
    }

    /**
     * Adds specified process into shell process tree for specified caller. 
     * Added process will be started.
     *
     * @param caller caller of specified shell process.
     * @param process shell process which will be added into process tree.
     */
    public synchronized void addProcess(final ShellProcess caller,
                                        final ShellProcess process) {
        process.setPID(++ShellProcessManager.pidCounter);

        this.tree.addProcess(caller, process);
        process.start();
    }

    /**
     * Adds specified processes into shell process tree for specified caller. 
     * Added processes will be started.
     *
     * @param caller caller of specified shell process.
     * @param processes shell processes which will be added into process tree.
     */
    public synchronized void addProcesses(final ShellProcess caller,
                                          final ShellProcess[] processes) {
        for (ShellProcess process : processes) {
            this.addProcess(caller, process);
        }
    }

    /**
     * Returns caller process of specified shell process.
     *
     * @param process shell process for which will be obtained caller.
     * @return Caller process of specified shell process.
     * @throws ShellProcessManagerException if the entered shell process doesn't 
     * exist in this shell tree or if the caller doesn't exist.
     */
    public synchronized ShellProcess getCaller(final ShellProcess process) throws ShellProcessManagerException {
        try {
            return this.tree.getCaller(process);
        } catch (ShellProcessTreeException exc) {
            throw new ShellProcessManagerException(exc.getMessage(), exc);
        }
    }

    /**
     * Returns array of subprocesses of specified shell process.
     *
     * @param process shell process for which will be obtained all subprocesses.
     * @return Array of subprocesses of specified shell process.
     * @throws ShellProcessManagerException if the entered process doesn't 
     * exist in this shell tree.
     */
    public synchronized ShellProcess[] getSubprocesses(final ShellProcess process) throws ShellProcessManagerException {
        try {
            return this.tree.getSubprocesses(process);
        } catch (ShellProcessTreeException exc) {
            throw new ShellProcessManagerException(exc.getMessage(), exc);
        }
    }

    /**
     * Removes all nodes with finished shell process from shell process tree.
     * This method deserves for garbage collector.
     */
    protected synchronized void removeFinishedProcesses() {
        this.tree.removeFinished();
    }

    /**
     * Returns string value of shell process tree with moved root into node 
     * which contains process with specified PID.
     *
     * <p>Example for {@code rootPID = 3}:</p>
     * <pre>
     *      shell (3) (running)
     *          +-- pstree (4) (finished)
     *          +-- pstree (5) (running)
     * </pre>
     *
     * @param rootPID builds shell process subtree from node which contains 
     * shell process with this pid.
     * @param showAll information whether will be shown all processes (all 
     * processes includes created, ready and finished processes).
     * @param showPID information whether will be shown pid of process.
     * @param showStatus information whether will be shown status of process.
     * @param comparator comparator for sorting children of some node. For 
     * comparator which is {@code null} is used {@link ShellProcessPIDComparator
     * PID comparator} (default).
     * 
     * @return String value representing shell process tree.
     * 
     * @throws ShellProcessManagerException if the tree node with entered 
     * rootPID doesn't exist in this shell process tree.
     */
    public synchronized String getProcessTree(final long rootPID,
                                              final boolean showAll,
                                              final boolean showPID,
                                              final boolean showStatus,
                                              final Comparator<ShellProcess> comparator) throws ShellProcessManagerException {
        try {
            return this.tree.toString(rootPID, showAll, showPID, showStatus,
                    comparator);
        } catch (ShellProcessTreeException exc) {
            throw new ShellProcessManagerException(exc.getMessage(), exc);
        }
    }

    /**
     * Returns string value of shell process tree.
     *
     * <p>Example for {@code comparator = }{@link ShellProcessNameComparator}:</p>
     * <pre>
     *      vmm (init) (1) (running)
     *          +-- login (2) (finished)
     *          +-- shell (3) (running)
     *                +-- cat (6) (finished)
     *                +-- echo (5) (finished)
     *                +-- pstree (4) (finished)
     *                +-- pstree (7) (running)
     * </pre>
     *
     * @param showAll information whether will be shown all processes (all 
     * processes includes created, ready and finished processes).
     * @param showPID information whether will be shown pid of process.
     * @param showStatus information whether will be shown status of process.
     * @param comparator comparator for sorting children of some node. For 
     * comparator which is {@code null} is used {@link ShellProcessPIDComparator
     * PID comparator} (default).
     * 
     * @return String value representing shell process tree.
     */
    public synchronized String getProcessTree(final boolean showAll,
                                              final boolean showPID,
                                              final boolean showStatus,
                                              final Comparator<ShellProcess> comparator) {
        return this.tree.toString(showAll, showPID, showStatus, comparator);
    }

    /**
     * Returns string value of shell process tree. The general contract for 
     * <code>getProcessTree(showAll, showPID, showStatus)</code> is that it 
     * should have exactly the same effect as the call <code>getProcessTree(
     * showAll, showPID, showStatus, null)</code>. 
     *
     * <p>Example for {@code showAll = false}:</p>
     * <pre>
     *      vmm (init) (1) (running)
     *          +-- shell (3) (running)
     *                +-- pstree (5) (running)
     * </pre>
     *
     * @param showAll information whether will be shown all processes (all 
     * processes includes created, ready and finished processes).
     * @param showPID information whether will be shown pid of process.
     * @param showStatus information whether will be shown status of process.
     * 
     * @return String value representing shell process tree.
     */
    public synchronized String getProcessTree(final boolean showAll,
                                              final boolean showPID,
                                              final boolean showStatus) {
        return this.tree.toString(showAll, showPID, showStatus);
    }

    /**
     * Returns string value of shell process tree. The general contract for 
     * <code>getProcessTree()</code> is that it should have exactly the same 
     * effect as the call <code>getProcessTree(true, true, true, null)</code>. 
     * 
     * <p>Example:</p>
     * <pre>
     *      vmm (init) (1) (running)
     *          +-- login (2) (finished)
     *          +-- shell (3) (running)
     *                +-- pstree (4) (finished)
     *                +-- pstree (5) (running)
     * </pre>
     *
     * @return String value representing shell process tree.
     */
    public synchronized String getProcessTree() {
        return this.tree.toString();
    }

    /**
     * Returns shell process with specified pid (Process identifier).
     *
     * @param pid process identifier.
     * @return Founded shell process or {@code null}.
     */
    public synchronized ShellProcess getProcess(final int pid) {
        ShellProcessTreeNode node = this.tree.find(pid);

        if (node == null) {
            return null;
        }

        return node.getProcess();
    }

    /**
     * Returns array of shell processes which are in the shell process tree. 
     * Array is builded using pre-order traversal of tree.
     *
     * @return Array of all processes in shell process tree.
     */
    public synchronized ShellProcess[] getProcesses() {
        return this.tree.getProcesses();
    }

    /**
     * Kills specified shell process.
     *
     * @param process shell process which will be killed.
     * @throws ShellProcessManagerException if entered shell process doesn't 
     * exist in shell process tree or if is it already finished/killed.
     */
    public synchronized void kill(final ShellProcess process) throws ShellProcessManagerException {
        if (process == null) {
            return;
        }

        this.kill(process.getPID());
    }

    /**
     * Kills shell process with specified {@code pid} (Process identifier).
     *
     * @param pid pid (Process identifier) of process which will be killed.
     * @throws ShellProcessManagerException if the shell process with entered 
     * pid doesn't exist in shell process tree or if is it already 
     * finished/killed.
     */
    public synchronized void kill(final long pid) throws ShellProcessManagerException {
        ShellProcessTreeNode processNode = this.tree.find(pid);
        if (processNode == null) {
            throw new ShellProcessManagerException("Cannot find process "
                    + "with pid=" + pid);
        }

        ShellProcess process = processNode.getProcess();
        if (process.getStatus() == ShellProcessStatus.FINISHED) {
            throw new ShellProcessManagerException("Cannot kill process "
                    + "with pid=" + pid);
        }

        process.kill();
    }

    /**
     * Kills all shell processes except VMM (init) process.
     */
    public synchronized void killAll() {
        ShellProcess root = this.tree.getRoot().getProcess();

        ShellProcess[] processes = this.tree.getPostOrderProcesses();
        for (ShellProcess process : processes) {
            if (process != root) {
                process.kill();
            }
        }

        this.gc.interrupt();
        this.tree.clear();
    }
}
