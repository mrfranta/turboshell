/**
 * This Java file was created 26.11.2013 2:48:32.
 */
package system.turboshell.process.manager;

/**
 * Class {@code ShellProcessTreeException} represents exception for manager of 
 * shell processes.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.5.6
 */
public class ShellProcessManagerException extends Exception {
    /** 
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a href=
     * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
     * details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 3L;

    /** 
     * Constructs exception for shell process manager with specified message.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     */
    public ShellProcessManagerException(final String message) {
        super(message);
    }

    /** 
     * Constructs exception for shell process manager with specified cause.
     *
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method). (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellProcessManagerException(final Throwable cause) {
        super(cause);
    }

    /** 
     * Constructs exception for shell process manager with specified message 
     * and cause.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method).  (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellProcessManagerException(final String message,
                                        final Throwable cause) {
        super(message, cause);
    }
}
