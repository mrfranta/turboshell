/**
 * This Java file was created 11.11.2013 15:50:10.
 */
package system.turboshell.process.manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessNameComparator;
import system.turboshell.process.ShellProcessPIDComparator;
import system.turboshell.process.ShellProcessStatus;
import system.utils.StringUtilities;

/**
 * Class {@code ShellProcessTree} represents tree structure which contains tree 
 * nodes. Each tree node contains shell process. Tree structure is important 
 * for caller -&gt; subprocesses hierarchy of shell processes.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.16
 * 
 * @since 0.4.0
 */
public class ShellProcessTree {
    /** Root node of this tree. */
    private ShellProcessTreeNode root;
    /** List for tree traversal (pre-order or post-order). */
    private List<ShellProcessTreeNode> traversal;
    /** Structure which stores data for {@code toString()} method. */
    private ShellProcessTreeToString toString;

    /**
     * Class {@code ShellProcessTreeToString}
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.0
     * 
     * @since 0.4.0
     */
    private static class ShellProcessTreeToString {
        /** 
         * Information whether will be shown all processes (all processes 
         * includes created, ready and finished processes).
         */
        public final boolean showAll;
        /** Information whether will be shown pid of process. */
        public final boolean showPID;
        /** Information whether will be shown status of process. */
        public final boolean showStatus;
        /** Comparator for sorting children of some node. */
        public final Comparator<ShellProcessTreeNode> comparator;

        /**
         * 
         * <b>Constructor</b>.
         *
         * @param showAll information whether will be shown all processes (all 
         * processes includes created, ready and finished processes).
         * @param showPID information whether will be shown pid of process.
         * @param showStatus information whether will be shown status of process.
         * @param comparator comparator for sorting children of some node. For 
         * comparator which is {@code null} is used {@link ShellProcessPIDComparator
         * PID comparator} (default).
         */
        public ShellProcessTreeToString(final boolean showAll,
                                        final boolean showPID,
                                        final boolean showStatus,
                                        final Comparator<ShellProcess> comparator) {
            this.showAll = showAll;
            this.showPID = showPID;
            this.showStatus = showStatus;
            this.comparator = new ShellProcessTreeNodeComparator(comparator);
        }
    }

    /**
     * Class {@code ShellProcessTreeNodeComparator} compares two tree nodes for order 
     * by specified comparator for processes. This comparator can be passed to a sort 
     * method (such as <tt>Collections.sort</tt> or <tt>Arrays.sort</tt>) to allow 
     * precise control over the sort order. Comparator can also be used to control 
     * the order of certain data structures (such as <tt>TreeSet</tt> or 
     * <tt>TreeMap</tt>).
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.0
     * 
     * @since 0.4.0
     */
    private static class ShellProcessTreeNodeComparator implements Comparator<ShellProcessTreeNode> {
        /** Comparator for shell processes. */
        private Comparator<ShellProcess> comparator;

        /**
         * Constructs comparator for shell process tree nodes.
         *
         * @param comparator comparator for shell processes.
         */
        public ShellProcessTreeNodeComparator(final Comparator<ShellProcess> comparator) {
            if (comparator == null) {
                this.comparator = new ShellProcessNameComparator();
                return;
            }

            this.comparator = comparator;
        }

        /**
         * Compares its two tree nodes for order by specified shell processes 
         * comparator. Returns a negative integer, zero, or a positive integer 
         * as the first argument is less than, equal to, or greater than the 
         * second.
         *
         * @param node1 the first tree node to be compared.
         * @param node2 the second tree node to be compared.
         * @return a negative integer, zero, or a positive integer as the first 
         * argument is less than, equal to, or greater than the second. 
         */
        public int compare(final ShellProcessTreeNode node1,
                           final ShellProcessTreeNode node2) {
            return this.comparator.compare(node1.getProcess(),
                    node2.getProcess());
        }
    }

    /**
     * Constructs shell process tree with entered shell process as root 
     * process. The general contract for this constructor is that it should 
     * have exactly the same effect as the call <code>new ShellProcessTree(new 
     * ShellProcessTreeNode(rootProcess, null))</code>. 
     *
     * @param rootProcess shell process which will be root process.
     */
    public ShellProcessTree(final ShellProcess rootProcess) {
        this(new ShellProcessTreeNode(rootProcess, null));
    }

    /**
     * Constructs shell process tree with entered tree node which contains a 
     * root shell process.
     *
     * @param root node which contains root shell process.
     */
    public ShellProcessTree(final ShellProcessTreeNode root) {
        this.root = root;
        this.traversal = new ArrayList<ShellProcessTreeNode>();
    }

    /**
     * Returns root node of this shell process tree.
     *
     * @return Root node of this shell process tree.
     */
    public ShellProcessTreeNode getRoot() {
        return this.root;
    }

    /**
     * Returns information whether this shell process tree contains specified 
     * shell process.
     *
     * @param process shell process whose presence in this tree is to be tested.
     * @return {@code true} if this tree contains the specified shell process;
     *         {@code false} otherwise.
     */
    public boolean contains(final ShellProcess process) {
        if (process == null) {
            return false;
        }

        ShellProcessTreeNode found = this.find(this.getRoot(), process.getPID());

        return (found != null);
    }

    /**
     * Returns information whether this shell process tree contains shell 
     * process with specified {@code pid} (Process identifier).
     *
     * @param processPID pid (Process identifier) of shell process whose 
     * presence in this tree is to be tested.
     * @return {@code true} if this tree contains the specified shell process
     * with entered pid (Process identifier); {@code false} otherwise.
     */
    public boolean contains(final long processPID) {
        ShellProcessTreeNode found = this.find(this.getRoot(), processPID);

        return (found != null);
    }

    /**
     * Recursively finds shell process with specified {@code pid} (Process identifier).
     *
     * @param node actual process tree node for passage.
     * @param processPID {@code pid} (Process identifier) of shell process 
     * which is searched.
     * @return Founded shell process or {@code null}.
     */
    private ShellProcessTreeNode find(final ShellProcessTreeNode node,
                                      final long pid) {
        if (node == null) {
            return null;
        }

        if (node.getProcess().getPID() == pid) {
            return node;
        }

        for (ShellProcessTreeNode child : node.getChildren()) {
            ShellProcessTreeNode found = this.find(child, pid);
            if (found != null) {
                return found;
            }
        }

        return null;
    }

    /**
     * Finds shell process tree node which contains specified shell process.
     *
     * @param process process which will be searched.
     * @return Founded shell process or {@code null}.
     */
    public ShellProcessTreeNode find(final ShellProcess process) {
        if (process == null) {
            return null;
        }

        return this.find(this.getRoot(), process.getPID());
    }

    /**
     * Finds shell process tree node which contains shell process with 
     * specified shell process {@code pid} (Process identifier).
     *
     * @param processPID {@code pid} (Process identifier) of shell process 
     * which will be searched.
     * @return Founded shell process or {@code null}.
     */
    public ShellProcessTreeNode find(final long processPID) {
        return this.find(this.getRoot(), processPID);
    }

    /**
     * Adds specified process into shell process tree for specified caller. 
     * Status of process will be changed into {@link ShellProcessStatus#READY 
     * ready}.
     *
     * @param caller caller of specified shell process.
     * @param process shell process which will be added into process tree.
     */
    public void addProcess(final ShellProcess caller,
                           final ShellProcess process) {
        if (process == null) {
            return;
        }

        if (process.name().equals("shell")) {
            ShellProcessTreeNode parent = this.getRoot();
            new ShellProcessTreeNode(process, parent);
        }
        else {
            ShellProcessTreeNode parent = this.find(caller);
            if (parent == null) {
                parent = this.getRoot();
            }

            new ShellProcessTreeNode(process, parent);
        }

        process.setStatus(ShellProcessStatus.READY);
    }

    /**
     * Returns caller process of specified shell process.
     *
     * @param process shell process for which will be obtained caller.
     * @return Caller process of specified shell process.
     * @throws ShellProcessTreeException if the entered shell process doesn't 
     * exist in this shell tree or if the caller doesn't exist.
     */
    public ShellProcess getCaller(final ShellProcess process) throws ShellProcessTreeException {
        if (process == null) {
            return null;
        }

        ShellProcessTreeNode node = this.find(process);
        if (node == null) {
            throw new ShellProcessTreeException("Process not found");
        }

        node = node.getParent();
        if (node == null) {
            throw new ShellProcessTreeException("Caller not found");
        }

        return node.getProcess();
    }

    /**
     * Returns array of subprocesses of specified shell process.
     *
     * @param process shell process for which will be obtained all subprocesses.
     * @return Array of subprocesses of specified shell process.
     * @throws ShellProcessTreeException if the entered shell process doesn't 
     * exist in this shell tree.
     */
    public ShellProcess[] getSubprocesses(final ShellProcess process) throws ShellProcessTreeException {
        if (process == null) {
            return null;
        }

        ShellProcessTreeNode node = this.find(process);
        if (node == null) {
            throw new ShellProcessTreeException("Process not found");
        }

        ShellProcessTreeNode[] children = node.getChildren();
        ShellProcess[] subprocesses = new ShellProcess[children.length];
        for (int i = 0; i < children.length; i++) {
            subprocesses[i] = children[i].getProcess();
        }

        return subprocesses;
    }

    /**
     * Recursively build list of shell processes using pre-order traversal.
     *
     * @param node actual process tree node for passage.
     */
    private void buildPreOrder(final ShellProcessTreeNode node) {
        this.traversal.add(node);

        for (ShellProcessTreeNode child : node.getChildren()) {
            this.buildPreOrder(child);
        }
    }

    /**
     * Recursively build list of shell processes using post-order traversal.
     *
     * @param node actual process tree node for passage.
     */
    private void buildPostOrder(final ShellProcessTreeNode node) {
        for (ShellProcessTreeNode child : node.getChildren()) {
            this.buildPreOrder(child);
        }

        this.traversal.add(node);
    }

    /**
     * Returns array of shell processes which are in the shell process tree. 
     * Array is builded using post-order traversal of tree.
     *
     * @return Array of all processes in shell process tree.
     */
    public ShellProcess[] getPostOrderProcesses() {
        this.traversal.clear();

        this.buildPostOrder(this.getRoot());
        ShellProcess[] processes = new ShellProcess[this.traversal.size()];
        for (int i = 0; i < processes.length; i++) {
            processes[i] = this.traversal.get(i).getProcess();
        }

        return processes;
    }

    /**
     * Returns array of shell processes which are in the shell process tree. 
     * Array is builded using pre-order traversal of tree.
     *
     * @return Array of all processes in shell process tree.
     */
    public ShellProcess[] getProcesses() {
        this.traversal.clear();

        this.buildPreOrder(this.getRoot());
        ShellProcess[] processes = new ShellProcess[this.traversal.size()];
        for (int i = 0; i < processes.length; i++) {
            processes[i] = this.traversal.get(i).getProcess();
        }

        return processes;
    }

    /**
     * Recursively removes all nodes with finished shell process from each 
     * children node of entered process tree node.
     * 
     * @param node actual process tree node for passage.
     */
    private void removeFinished(final ShellProcessTreeNode node) {
        if (node == null) {
            return;
        }

        ShellProcessTreeNode[] children = node.getChildren();
        if (children.length == 0) {
            if (node.getProcess().getStatus() == ShellProcessStatus.FINISHED) {
                node.getParent().removeChild(node);
            }

            return;
        }

        for (ShellProcessTreeNode child : children) {
            this.removeFinished(child);
        }
    }

    /**
     * Removes all nodes with finished shell process.
     */
    public void removeFinished() {
        this.removeFinished(this.getRoot());
    }

    /**
     * Cleares shell process tree - removes all three nodes except root node.
     */
    public void clear() {
        this.traversal.clear();
        this.buildPostOrder(this.getRoot());

        for (ShellProcessTreeNode treeNode : this.traversal) {
            if (treeNode.getParent() != null) {
                treeNode.getParent().removeChild(treeNode);
            }

            treeNode = null;
        }
    }

    /**
     * Recursively creates string value of shell process tree. This method 
     * traversal nodes in  this tree and makes their string value. Also models 
     * tree structure.
     *
     * <p>Example of result of one passage of this method:</p>
     * <pre>
     *          +-- login (2) (finished)
     *          +-- shell (3) (running)
     * </pre>
     *
     * @param node actual process tree node for passage.
     * @param depth actual depth in shell process tree.
     * @return String value representing this shell process tree.
     */
    private String toString(final ShellProcessTreeNode node, final int depth) {
        String str = StringUtilities.repeat(" ", depth * 3);
        if (depth > 1) {
            str += StringUtilities.repeat(" ", 3);
        }

        if (depth > 0) {
            str += " +-- ";
        }

        ShellProcess process = node.getProcess();
        if (!this.toString.showAll && process.getStatus() == ShellProcessStatus.FINISHED) {
            return "";
        }

        str += process.name();
        if (this.toString.showPID) {
            str += " (" + process.getPID() + ")";
        }

        if (this.toString.showStatus) {
            str += " (" + process.getStatus() + ")";
        }

        str += "\n";

        ShellProcessTreeNode[] children = node.getChildren();
        Arrays.sort(children, this.toString.comparator);
        for (ShellProcessTreeNode child : children) {
            str += this.toString(child, depth + 1);
        }

        return str;
    }

    /**
     * Returns string value of shell process tree with moved root into node 
     * which contains process with specified PID.
     *
     * <p>Example for {@code rootPID = 3}:</p>
     * <pre>
     *      shell (3) (running)
     *          +-- pstree (4) (finished)
     *          +-- pstree (5) (running)
     * </pre>
     *
     * @param rootPID builds shell process subtree from node which contains 
     * shell process with this pid.
     * @param showAll information whether will be shown all processes (all 
     * processes includes created, ready and finished processes).
     * @param showPID information whether will be shown pid of process.
     * @param showStatus information whether will be shown status of process.
     * @param comparator comparator for sorting children of some node. For 
     * comparator which is {@code null} is used {@link ShellProcessPIDComparator
     * PID comparator} (default).
     * 
     * @return String value representing shell process tree.
     * 
     * @throws ShellProcessTreeException if the tree node with entered rootPID 
     * doesn't exist in this shell process tree.
     */
    public String toString(final long rootPID,
                           final boolean showAll,
                           final boolean showPID,
                           final boolean showStatus,
                           final Comparator<ShellProcess> comparator) throws ShellProcessTreeException {
        this.toString = new ShellProcessTreeToString(showAll, showPID,
                showStatus, comparator);

        ShellProcessTreeNode node = this.find(rootPID);
        if (node == null) {
            throw new ShellProcessTreeException("Process not found");
        }

        return this.toString(node, 0);
    }

    /**
     * Returns string value of shell process tree.
     *
     * <p>Example for {@code comparator = }{@link ShellProcessNameComparator}:</p>
     * <pre>
     *      vmm (init) (1) (running)
     *          +-- login (2) (finished)
     *          +-- shell (3) (running)
     *                +-- cat (6) (finished)
     *                +-- echo (5) (finished)
     *                +-- pstree (4) (finished)
     *                +-- pstree (7) (running)
     * </pre>
     *
     * @param showAll information whether will be shown all processes (all 
     * processes includes created, ready and finished processes).
     * @param showPID information whether will be shown pid of process.
     * @param showStatus information whether will be shown status of process.
     * @param comparator comparator for sorting children of some node. For 
     * comparator which is {@code null} is used {@link ShellProcessPIDComparator
     * PID comparator} (default).
     * 
     * @return String value representing shell process tree.
     */
    public String toString(final boolean showAll,
                           final boolean showPID,
                           final boolean showStatus,
                           final Comparator<ShellProcess> comparator) {
        this.toString = new ShellProcessTreeToString(showAll, showPID,
                showStatus, comparator);

        return this.toString(this.getRoot(), 0);
    }

    /**
     * Returns string value of shell process tree. The general contract for 
     * <code>toString(showAll, showPID, showStatus)</code> is that it should 
     * have exactly the same effect as the call <code>toString(showAll, showPID, 
     * showStatus, null)</code>. 
     *
     * <p>Example for {@code showAll = false}:</p>
     * <pre>
     *      vmm (init) (1) (running)
     *          +-- shell (3) (running)
     *                +-- pstree (5) (running)
     * </pre>
     *
     * @param showAll information whether will be shown all processes (all 
     * processes includes created, ready and finished processes).
     * @param showPID information whether will be shown pid of process.
     * @param showStatus information whether will be shown status of process.
     * 
     * @return String value representing shell process tree.
     */
    public String toString(final boolean showAll,
                           final boolean showPID,
                           final boolean showStatus) {
        this.toString = new ShellProcessTreeToString(showAll, showPID,
                showStatus, null);

        return this.toString(this.getRoot(), 0);
    }

    /**
     * Returns string value of shell process tree. The general contract for 
     * <code>toString()</code> is that it should have exactly the same effect 
     * as the call <code>toString(true, true, true, null)</code>. 
     * 
     * <p>Example:</p>
     * <pre>
     *      vmm (init) (1) (running)
     *          +-- login (2) (finished)
     *          +-- shell (3) (running)
     *                +-- pstree (4) (finished)
     *                +-- pstree (5) (running)
     * </pre>
     *
     * @return String value representing shell process tree.
     */
    @Override
    public String toString() {
        this.toString = new ShellProcessTreeToString(true, true,
                true, null);

        return this.toString(this.getRoot(), 0);
    }
}
