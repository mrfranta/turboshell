/**
 * This Java file was created 26.11.2013 12:53:47.
 */
package system.turboshell.process;

/**
 * Interface {@code ShellProcessListener} serves for listening of changes in 
 * shell process. This interface allows listen to process state changes and 
 * process position changes.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.5.8
 */
public interface ShellProcessListener {
    /**
     * Action that is being invoked in case of process creation.
     *
     * @param process process which was created.
     */
    public void processCreated(ShellProcess process);

    /**
     * Action that is being invoked in case of process got ready.
     *
     * @param process process which got ready.
     */
    public void processReady(ShellProcess process);

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    public void processStarted(ShellProcess process);

    /**
     * Action that is being invoked in case of process was killed.
     *
     * @param process process which was killed.
     */
    public void processKilled(ShellProcess process);

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    public void processFinished(ShellProcess process);

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    public void processMovedIntoForeground(ShellProcess process);

    /**
     * Action that is being invoked in case of process started.
     *
     * @param process process which was started.
     */
    public void processMovedIntoBackground(ShellProcess process);
}
