package system.turboshell;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import system.turboshell.command.parser.CommandStructure;

/**
 * Dummy regexp interpreter. Uses Java regexp engine and doesn't really work
 * with anything except local files.
 *
 * @author pkopac
 */
public class RegExpInterpreter {

    public static void interpreteRegexps(final CommandStructure[] structures,
                                         final File workingDir) {
        for (CommandStructure cmd : structures) {
            List<String> params = new LinkedList<String>(
                    Arrays.asList(cmd.getParameters()));

            for (ListIterator<String> it = params.listIterator(); it.hasNext();) {
                String[] res = RegExpInterpreter.interpreteRegexp(it.next(),
                        workingDir);
                if (res != null) {
                    it.remove();
                    for (String interpreted : res) {
                        it.add(interpreted);
                    }
                }
            }

            cmd.setParameters(params);
        }
    }

    private static String[] interpreteRegexp(final String string,
                                             final File workingDir) {
        final String regexp = RegExpInterpreter.convertGlobFormat(string);

        final Pattern p;
        try {
            p = Pattern.compile(regexp);
        } catch (PatternSyntaxException e) {
            return null;
        }

        File[] files = workingDir.listFiles(new FilenameFilter() {
            public boolean accept(final File dir, final String name) {
                return p.matcher(name).matches();
            }
        });

        if (files == null || files.length == 0) {
            return null;
        }

        return RegExpInterpreter.filesToStrings(files);
    }

    private static String[] filesToStrings(final File[] files) {
        String[] strings = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            strings[i] = files[i].getName();
        }

        return strings;
    }

    private static String convertGlobFormat(final String glob) {
        String out = "^";
        for (int i = 0; i < glob.length(); ++i) {
            final char c = glob.charAt(i);
            switch (c) {
                case '*':
                    out += ".*";
                    break;

                case '?':
                    out += '.';
                    break;

                case '.':
                    out += "\\.";
                    break;

                case '\\':
                    out += "\\\\";
                    break;

                default:
                    out += c;
            }
        }
        out += '$';

        return out;
    }
}
