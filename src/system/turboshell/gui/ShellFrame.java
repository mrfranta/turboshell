package system.turboshell.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

import system.turboshell.ShellCore;
import system.turboshell.ShellCoreException;
import system.utils.SwingxUtilities;

/**
 * <b>ShellFrame</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.5
 * 
 * @since 0.2.1
 */
public class ShellFrame extends JFrame {
    private final ShellCore core;

    private final ShellConsole console;
    private ConsoleSizeLabel consoleSize;
    private boolean showConsoleSize;

    private static int paddingXCounter = 0;
    private static int paddingYCounter = 0;
    private static final int PADDING = 25; // px

    /** 
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a href=
     * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
     * details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 6L;

    /**
     * <b>ConsoleSizeLabel</b>.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.0
     * 
     * @since 0.2.1
     */
    public static class ConsoleSizeLabel extends JLabel {
        /** long variable serialVersionUID. */
        private static final long serialVersionUID = 1L;

        private int cols;
        private int rows;

        public ConsoleSizeLabel() {
            super("");

            Color color = UIManager.getColor("ToolTip.background");
            this.setBackground(color);
            this.setOpaque(true);

            this.setFont(new Font("Dialog", Font.PLAIN, 14));

            Border border = BorderFactory.createCompoundBorder(
                    UIManager.getBorder("ToolTip.border"),
                    BorderFactory.createEmptyBorder(5, 5, 5, 5));
            this.setBorder(border);

            this.setConsoleSize(0, 0);
        }

        protected void changeText() {
            this.setText(this.cols + " \u00D7 " + this.rows);
        }

        public void setConsoleSize(final int cols, final int rows) {
            this.cols = cols;
            this.rows = rows;
            this.changeText();
        }

        public int getConsoleColumns() {
            return this.cols;
        }

        public void setConsoleColumns(final int cols) {
            this.cols = cols;
        }

        public int getConsoleRows() {
            return this.cols;
        }

        public void setConsoleRows(final int rows) {
            this.rows = rows;
        }
    }

    /**
     * <b>ConsoleSizeListener</b>.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.0
     * 
     * @since 0.2.1
     */
    private class ConsoleSizeListener extends ComponentAdapter {
        private Timer timer;
        private FontMetrics fm;

        public ConsoleSizeListener() {
            this.timer = new Timer(500, new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    ShellFrame.this.getGlassPane().setVisible(false);
                }
            });
            this.timer.setRepeats(false);

            Font font = ShellFrame.this.console.getFont();
            this.fm = ShellFrame.this.console.getFontMetrics(font);
        }

        @Override
        public void componentResized(final ComponentEvent event) {
            if (!ShellFrame.this.showConsoleSize) {
                if (ShellFrame.this.isVisible()) {
                    ShellFrame.this.showConsoleSize = true;
                }
                return;
            }

            Component comp = event.getComponent();

            int width = comp.getWidth();
            int height = comp.getHeight();

            int cols = width / this.fm.charWidth(' ');
            int rows = height / this.fm.getHeight();

            ShellFrame.this.console.setColumns(cols);
            ShellFrame.this.console.setRows(rows);

            ShellFrame.this.consoleSize.setConsoleSize(cols, rows);
            ShellFrame.this.getGlassPane().setVisible(true);

            this.timer.restart();
        }
    }

    /**
     * <b>ShellFrameListener</b>.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.2
     * 
     * @since 0.4.1
     */
    private class ShellFrameListener extends WindowAdapter {
        @Override
        public void windowClosing(final WindowEvent event) {
            ShellFrame.this.console.setCanCloseStream(true);

            ShellFrame.this.setVisible(false);
            ShellFrame.this.dispose();

            ShellFrame.this.core.getShellProcess().kill();
        }
    }

    public ShellFrame(final ShellCore core) throws ShellCoreException {
        super("TurboShell");

        this.core = core;
        this.console = new ShellConsole(this.core);
        this.showConsoleSize = false;

        this.setIconImage(ShellIcons.APPICON);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new ShellFrameListener());
    }

    protected void init() {
        this.setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(this.console,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        this.add(scrollPane, BorderLayout.CENTER);

        JComponent rootPane = this.getRootPane();
        rootPane.addComponentListener(new ConsoleSizeListener());

        JComponent glassPane = (JComponent) this.getGlassPane();
        glassPane.setLayout(new GridBagLayout());

        this.consoleSize = new ConsoleSizeLabel();
        glassPane.add(this.consoleSize);

        this.pack();
    }

    public ShellConsole getConsole() {
        return this.console;
    }

    public void open() {
        if (!this.isVisible()) {
            this.init();

            int x = (ShellFrame.paddingXCounter++) * ShellFrame.PADDING;
            int y = (ShellFrame.paddingYCounter++) * ShellFrame.PADDING;
            Dimension dimension = this.getSize();
            if ((y + dimension.height) > SwingxUtilities.SCREEN_SIZE.height) {
                ShellFrame.paddingXCounter++;
                ShellFrame.paddingYCounter = 1;

                x += ShellFrame.PADDING;
                y = 0;
            }

            if ((x + dimension.width) > SwingxUtilities.SCREEN_SIZE.width) {
                ShellFrame.paddingXCounter = 0;
                ShellFrame.paddingYCounter = 0;

                x = 0;
                y = 0;
            }

            this.setLocation(x, y);

            this.setVisible(true);
        }
    }

    public void close() {
        WindowEvent closingEvent = new WindowEvent(this,
                WindowEvent.WINDOW_CLOSING);
        SwingxUtilities.TOOLKIT.getSystemEventQueue().postEvent(
                closingEvent);
    }
}
