/**
 * This Java file was created 11.11.2013 22:56:48.
 */
package system.turboshell.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;

import javax.swing.Action;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import system.turboshell.ShellCore;
import system.turboshell.ShellCoreException;
import system.turboshell.VMM;
import system.turboshell.command.cmd.Shell;
import system.turboshell.command.history.CommandHistory;
import system.turboshell.prompt.Prompt;
import system.turboshell.prompt.ShellPrompt;
import system.turboshell.stdio.ConsoleInputStream;
import system.turboshell.stdio.ConsolePrintStream;
import system.utils.ResourcesLoader;
import system.utils.StringUtilities;
import system.utils.SwingxUtilities;

/**
 * <b>ShellConsole</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.10
 *
 * @since 0.4.0
 */
public class ShellConsole extends JTextArea implements KeyListener {

    private final ShellCore core;
    private final CommandHistory history;
    private final ShellPrompt shellPrompt;

    private Prompt prompt;
    private int minCaretPosition;
    private boolean canCloseStream;

    private boolean blocked;
    public final StringBuffer buffer;

    private final ConsoleInputStream inputStream;
    private final ConsolePrintStream outputStream;
    private final ConsolePrintStream errorStream;

    /**
     * long variable serialVersionUID.
     */
    private static final long serialVersionUID = 11L;

    private class ShellConsolePopupMenu extends JPopupMenu {

        private JMenuItem selectAll;
        private JMenuItem copy;
        private JMenuItem paste;

        private Clipboard clipboard;

        /**
         * long variable serialVersionUID.
         */
        private static final long serialVersionUID = 1L;

        /**
         * <b>Constructor</b>.
         *
         * @param label
         */
        public ShellConsolePopupMenu() {
            super("Shell menu");

            this.clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

            this.init();
        }

        protected void init() {
            JMenuItem item = new JMenuItem("Open terminal");
            item.addActionListener(new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    Shell shell = new Shell();
                    VMM.getInstance().startProcess(shell.getProcess());
                }
            });
            this.add(item);

            this.addSeparator();

            item = new JMenuItem("Close window");
            item.addActionListener(new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    WindowEvent closingEvent = new WindowEvent(
                            SwingxUtilities.getWindowAncestor(ShellConsole.this),
                            WindowEvent.WINDOW_CLOSING);
                    SwingxUtilities.TOOLKIT.getSystemEventQueue().postEvent(
                            closingEvent);
                }
            });
            this.add(item);

            this.addSeparator();

            this.selectAll = new JMenuItem("Select all");
            this.selectAll.addActionListener(new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    ShellConsole.this.setSelectionStart(0);
                    ShellConsole.this.setSelectionEnd(ShellConsole.this.getTextLength());
                }
            });
            this.add(this.selectAll);

            this.copy = new JMenuItem("Copy");
            this.copy.addActionListener(new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    String selected = ShellConsole.this.getSelectedText();
                    StringSelection stringSelection = new StringSelection(
                            selected);
                    ShellConsolePopupMenu.this.clipboard.setContents(
                            stringSelection, null);
                }
            });
            this.add(this.copy);

            this.paste = new JMenuItem("Paste");
            this.paste.addActionListener(new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    Transferable t = ShellConsolePopupMenu.this.clipboard.getContents(null);
                    try {
                        if (t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                            String data = (String) t.getTransferData(DataFlavor.stringFlavor);
                            if (data == null) {
                                return;
                            }

                            data = data.replaceAll("\n", " ");

                            if (ShellConsole.this.getCaretPosition() < ShellConsole.this.getMinCaretPosition()) {
                                ShellConsole.this.append(data);
                            }
                            else {
                                Document doc = ShellConsole.this.getDocument();
                                try {
                                    doc.insertString(
                                            ShellConsole.this.getCaretPosition(),
                                            data, null);
                                } catch (BadLocationException exc) {
                                    // no way!
                                }
                            }
                        }
                    } catch (Exception exc) {
                        // no action
                    }
                }
            });
            this.add(this.paste);

            this.addSeparator();

            item = new JMenuItem("Background color");
            item.addActionListener(new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    ShellConsoleColorChooser chooser = new ShellConsoleColorChooser(
                            "Background color", false);
                    chooser.show();
                }
            });
            this.add(item);

            item = new JMenuItem("Font color");
            item.addActionListener(new ActionListener() {
                // JDK1.5 compatible @Override
                public void actionPerformed(final ActionEvent event) {
                    ShellConsoleColorChooser chooser = new ShellConsoleColorChooser(
                            "Font color", true);
                    chooser.show();
                }
            });
            this.add(item);
        }

        ;

        @Override
        public void show(final Component invoker, final int x, final int y) {
            super.show(invoker, x, y);

            String selected = ShellConsole.this.getSelectedText();

            boolean enabled = selected == null
                    || selected.length() != ShellConsole.this.getTextLength();

            this.selectAll.setEnabled(enabled);
            this.copy.setEnabled(selected != null && !StringUtilities.isEmpty(selected));
            this.paste.setEnabled(this.clipboard.getContents(null) != null);
        }
    }

    private class ShellConsoleColorChooser {

        private JColorChooser chooser;
        private JDialog dialog;
        private Color old;

        public ShellConsoleColorChooser(final String title,
                                        final boolean foreground) {
            if (foreground) {
                this.old = ShellConsole.this.getForeground();
            }
            else {
                this.old = ShellConsole.this.getBackground();
            }

            this.chooser = new JColorChooser(this.old);
            this.chooser.getSelectionModel().addChangeListener(
                    // JDK1.5 compatible @Override
                    new ChangeListener() {
                        // JDK1.5 compatible @Override
                        public void stateChanged(final ChangeEvent event) {
                            SwingUtilities.invokeLater(new Runnable() {
                                // JDK1.5 compatible @Override
                                public void run() {
                                    if (foreground) {
                                        ShellConsole.this.setForeground(
                                                ShellConsoleColorChooser.this.chooser.getColor());
                                    }
                                    else {
                                        ShellConsole.this.setBackground(
                                                ShellConsoleColorChooser.this.chooser.getColor());
                                    }
                                }
                            });
                        }
                    });

            this.dialog = JColorChooser.createDialog(ShellConsole.this, title,
                    true, this.chooser, new ActionListener() {
                        // JDK1.5 compatible @Override
                        public void actionPerformed(final ActionEvent event) {
                            // no action
                        }
                    }, new ActionListener() {
                        // JDK1.5 compatible @Override
                        public void actionPerformed(final ActionEvent event) {
                            if (foreground) {
                                ShellConsole.this.setForeground(ShellConsoleColorChooser.this.old);
                            }
                            else {
                                ShellConsole.this.setBackground(ShellConsoleColorChooser.this.old);
                            }
                        }
                    });
        }

        public void show() {
            this.dialog.setVisible(true);
        }
    }

    public ShellConsole(final ShellCore core) throws ShellCoreException {
        super(24, 80);

        if (core == null) {
            throw new NullPointerException("Shell core cannot be null");
        }

        this.core = core;

        this.inputStream = new ConsoleInputStream(this);
        this.outputStream = new ConsolePrintStream(this);
        this.errorStream = new ConsolePrintStream(this);

        try {
            this.initCore();
        } catch (Exception exc) {
            throw new ShellCoreException("Cannot initialize streams "
                    + "in shell core", exc);
        }

        this.history = new CommandHistory();
        this.shellPrompt = new ShellPrompt(core);

        this.minCaretPosition = 0;
        this.canCloseStream = false;
        this.blocked = false;

        this.buffer = new StringBuffer();

        Font font = ResourcesLoader.getTrueTypeFont("consolas.ttf");
        this.setFont(font.deriveFont(Font.PLAIN, 14));
        this.setForeground(Color.CYAN);
        this.setBackground(Color.BLACK);

        this.setComponentPopupMenu(new ShellConsolePopupMenu());

        this.setLineWrap(true);
        this.addKeyListener(this);
        this.setTransferHandler(null);

        Action[] actions = this.getActions();
        for (Action action : actions) {
            if (action.getValue(Action.NAME).equals("select-all")) {
                action.setEnabled(false);
                break;
            }
        }

        this.prompt = this.shellPrompt;
        this.appendPromptLine();
    }

    private void initCore() throws Exception {
        Class<?> clazz = this.core.getClass();
        Method method = clazz.getDeclaredMethod("initConsole",
                ShellConsole.class);

        method.setAccessible(true);
        method.invoke(this.core, this);
        method.setAccessible(false);
    }

    public int getTextLength() {
        return this.getText().length();
    }

    private void executeCommand() {
        String command = this.getCommand();
        this.append("\n");
        if (command == null || StringUtilities.isEmpty(command)) {
            this.appendPromptLine();
            return;
        }

        this.history.addCommand(command);

        try {
            this.core.execute(command);
        } catch (ShellCoreException exc) {
            this.append(exc.getMessage() + "\n");
            this.appendPromptLine();
        }
    }

    public void dropCommand() {
        this.append("^C\n");
        this.appendPromptLine();
    }

    public void clearCommand() {
        this.replaceRange(null, this.minCaretPosition,
                this.getTextLength());
    }

    public void setCommand(final String command) {
        if (command == null) {
            return;
        }

        this.clearCommand();
        this.append(command);
    }

    public String getCommand() {
        return this.getText().substring(this.minCaretPosition);
    }

    @Override
    public synchronized void append(final String text) {
        try {
            super.append(text);

            this.setCaretPosition(this.getTextLength());
        } catch (Error err) {
            // interrupting error
        }
    }

    public synchronized void append(final char character) {
        this.append("" + character);
    }

    public synchronized void streamAppend(final String text) {
        super.append(text);

        this.minCaretPosition = this.getTextLength();
        this.setCaretPosition(this.minCaretPosition);
    }

    public synchronized void appendPromptLine() {
        this.core.appendBackgroundInfo();
        this.append(this.prompt.getPrompt());
        this.minCaretPosition = this.getTextLength();
    }

    public synchronized boolean isBlocked() {
        return this.blocked;
    }

    public synchronized void setBlocked(final boolean blocked) {
        this.blocked = blocked;
    }

    public int getMinCaretPosition() {
        return this.minCaretPosition;
    }

    public InputStream getInputStream() {
        return this.inputStream;
    }

    public PrintStream getOutputStream() {
        return this.outputStream;
    }

    public PrintStream getErrorStream() {
        return this.errorStream;
    }

    public boolean canCloseStream() {
        return this.canCloseStream;
    }

    protected void setCanCloseStream(final boolean canCloseStream) {
        this.canCloseStream = canCloseStream;
    }

    protected void close() {
        this.setCanCloseStream(true);
        PrintStream stream = this.getOutputStream();
        stream.flush();
        stream.close();

        stream = this.getErrorStream();
        stream.flush();
        stream.close();
    }

    public void clear() {
        this.setText("");
    }

    private void exit() {
        this.append("^D\n");
        this.appendPromptLine();

        WindowEvent closingEvent = new WindowEvent(
                SwingxUtilities.getWindowAncestor(this),
                WindowEvent.WINDOW_CLOSING);
        SwingxUtilities.TOOLKIT.getSystemEventQueue().postEvent(
                closingEvent);
    }

    // JDK1.5 compatible @Override
    public void keyTyped(final KeyEvent event) {
        if (this.blocked) {
            event.consume();
        }
    }

    // JDK1.5 compatible @Override
    public void keyPressed(final KeyEvent event) {
        int code = event.getKeyCode();
        char character = event.getKeyChar();

        if (this.blocked) {
            if (code == KeyEvent.VK_ENTER) {
                this.append(character);
                String str = this.buffer.toString() + "\n";
                this.inputStream.append(str);

                this.buffer.setLength(0);
            }
            else if (code == KeyEvent.VK_C && event.isControlDown()) {
                this.streamAppend("^C\n");
                this.buffer.setLength(0);
                this.core.blocking.get(0).kill();
            }
            else if (code == KeyEvent.VK_Z && event.isControlDown()) {
                this.streamAppend("^Z\n");
                this.buffer.setLength(0);
                this.core.sendToBackground(this.core.blocking.get(0));

            }
            else if (code == KeyEvent.VK_D && event.isControlDown()) {
                String str = this.buffer.toString();
                if (str.length() == 0) {
                    this.core.blocking.get(0).kill();
                }

                this.inputStream.append(str);
                this.buffer.setLength(0);
            }
            else if (code == KeyEvent.VK_BACK_SPACE && this.buffer.length() > 0) {
                int last = this.buffer.length() - 1;
                this.buffer.deleteCharAt(last);
                Document doc = this.getDocument();
                try {
                    doc.remove(doc.getLength() - 1, 1);
                } catch (BadLocationException exc) {
                    // no way!
                }
            }
            else if (character != KeyEvent.CHAR_UNDEFINED) {
                if (character != 8) { // some bug with backspace
                    this.buffer.append(character);
                    this.append(character + "");
                }
            }

            event.consume();
            return;
        }

        String command = "";

        if (this.getSelectionStart() < this.getMinCaretPosition()) {
            this.setSelectionStart(this.getMinCaretPosition());
        }

        switch (code) {
            case KeyEvent.VK_ENTER:
                this.executeCommand();
                event.consume();

                break;

            case KeyEvent.VK_UP:
                command = this.history.moveIteratorForward();
                this.setCommand(command);
                event.consume();

                break;

            case KeyEvent.VK_DOWN:
                command = this.history.moveIteratorBackward();
                this.setCommand(command);
                event.consume();

                break;

            case KeyEvent.VK_LEFT:
                if (this.getCaretPosition() <= this.getMinCaretPosition()) {
                    event.consume();
                }

                break;

            case KeyEvent.VK_DELETE:
                if (this.getCaretPosition() < this.getMinCaretPosition()) {
                    this.setCaretPosition(this.getMinCaretPosition());
                }

                break;

            case KeyEvent.VK_BACK_SPACE:
                if (this.getCaretPosition() < this.getMinCaretPosition()) {
                    event.consume();
                }

                if (this.getCaretPosition() == this.getMinCaretPosition()
                        && this.getSelectedText() == null) {
                    event.consume();
                }

                break;

            default:
                if (event.getKeyCode() == KeyEvent.VK_C && event.isControlDown()) {
                    this.dropCommand();

                    break;
                }

                if (event.getKeyCode() == KeyEvent.VK_D && event.isControlDown()) {
                    if (StringUtilities.isEmpty(this.getCommand())) {
                        this.exit();
                    }

                    break;
                }

                if (this.getCaretPosition() > this.getMinCaretPosition()) {
                    this.history.moveIteratorToHead();
                }

                break;
        }
    }

    // JDK1.5 compatible @Override
    public void keyReleased(final KeyEvent event) {
        if (this.blocked) {
            event.consume();
        }
    }
}
