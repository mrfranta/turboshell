/**
 * This Java file was created 15.11.2013 13:02:54.
 */
package system.turboshell.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

import system.turboshell.ShellLoginAuthentication;
import system.turboshell.VMM;
import system.utils.SpringUtilities;

/**
 * <b>LoginDialog</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.1
 * 
 * @since 0.4.11
 */
public class ShellLoginDialog extends JDialog {
    private String userName;
    private FormTextField userNameField;
    private ShellLoginAuthentication auth;

    private static final int MARGIN = 10;

    /** long variable serialVersionUID. */
    private static final long serialVersionUID = 2L;

    /**
     * <b>FormLabel</b> represents label for form panel.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.0
     * 
     * @since 0.4.11
     */
    private static class FormLabel extends JLabel {
        /**
         * Determines if a de-serialized file is compatible with this class.
         * 
         * Maintainers must change this value if and only if the new version of this
         * class is not compatible with old versions. See Oracle docs for <a
         * href="http://docs.oracle.com/javase/1.4.2/docs/guide/
         * serialization/">details</a>.
         * 
         * Not necessary to include in first version of the class, but included here
         * as a reminder of its importance.
         */
        private static final long serialVersionUID = 1L;

        /**
         * <b>Constructor</b> constructs form label with text.
         *
         * @param text text for form label.
         */
        public FormLabel(final String text) {
            super(text);
        }

        @Override
        public void setLabelFor(final Component comp) {
            super.setLabelFor(comp);

            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(final MouseEvent event) {
                    comp.requestFocus();

                    if (comp instanceof JComboBox) {
                        ((JComboBox) comp).showPopup();
                    }
                }
            });
        }
    }

    /**
     * <b>FormTextField</b> represents text field for form panel.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.0
     */
    private static class FormTextField extends JTextField implements FocusListener {
        /** Default border for text component. */
        private static final Border BORDER = BorderFactory.createLineBorder(Color.GRAY);
        /** Focused border for text component. */
        private static final Border FOCUSED_BORDER = BorderFactory.createLineBorder(new Color(
                0x3e8ce6));
        /** 
         * Read only border for text component (for component which don't allow 
         * text editing).
         */
        private static final Border READONLY_BORDER = BorderFactory.createLineBorder(Color.LIGHT_GRAY);

        /** 
         * Determines if a de-serialized file is compatible with this class.
         * 
         * Maintainers must change this value if and only if the new version of this
         * class is not compatible with old versions. See Oracle docs for <a href=
         * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
         * details</a>.
         * 
         * Not necessary to include in first version of the class, but included here
         * as a reminder of its importance.
         */
        private static final long serialVersionUID = 1L;

        /**
         * <b>Constructor</b> construct form text field.
         */
        public FormTextField(final int columns) {
            super(columns);

            this.addFocusListener(this);
        }

        /**
         * <b>Setter for Editable</b> allows or disallows editing text in text 
         * field.
         *
         * @param editable information whether is text field editable.
         */
        @Override
        public void setEditable(final boolean editable) {
            super.setEditable(editable);

            if (editable) {
                this.setForeground(Color.BLACK);
                this.setBorder(FormTextField.BORDER);
            }
            else {
                this.setForeground(Color.DARK_GRAY);
                this.setBorder(FormTextField.READONLY_BORDER);
            }
        }

        /**
         * <b>Method focusGained</b> changes border for editable text field on focus 
         * gained.
         *
         * @param event focus event.
         */
        // JDK1.5 compatible @Override
        public void focusGained(final FocusEvent event) {
            if (this.isEditable() && this.isEnabled()) {
                this.setBorder(FormTextField.FOCUSED_BORDER);
            }
        }

        /**
         * <b>Method focusLost</b> changes border for editable text field on focus 
         * lost.
         *
         * @param event focus event.
         */
        // JDK1.5 compatible @Override
        public void focusLost(final FocusEvent event) {
            if (this.isEditable()) {
                this.setBorder(FormTextField.BORDER);
            }
        }
    }

    private class LoginActionListener implements ActionListener {
        // JDK1.5 compatible @Override
        public void actionPerformed(final ActionEvent event) {
            String userName = ShellLoginDialog.this.userNameField.getText();
            if (ShellLoginDialog.this.auth.authenticate(userName, null)) {
                ShellLoginDialog.this.userName = userName;
                ShellLoginDialog.this.close();
            }
            else {
                JOptionPane.showMessageDialog(ShellLoginDialog.this,
                        "Authentication failure", "Authentication",
                        JOptionPane.WARNING_MESSAGE, ShellIcons.WARNING_ICON);
            }
        }
    }

    private ShellLoginDialog() {
        super((Frame) null, "Login", true);

        this.userName = null;
        this.auth = new ShellLoginAuthentication();

        // this.set(ShellIcons.APPICON);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent event) {
                ShellLoginDialog.this.close();
            }
        });

        this.init();
    }

    protected void init() {
        this.setLayout(new BorderLayout());

        JLabel icon = new JLabel(ShellIcons.USER_ICON);
        icon.setBorder(BorderFactory.createEmptyBorder(ShellLoginDialog.MARGIN,
                ShellLoginDialog.MARGIN, ShellLoginDialog.MARGIN,
                ShellLoginDialog.MARGIN));
        this.add(icon, BorderLayout.WEST);

        JPanel center = new JPanel(new BorderLayout());
        this.add(center, BorderLayout.CENTER);

        JPanel form = new JPanel(new SpringLayout());

        FormLabel label = new FormLabel("User name:");
        form.add(label);

        ActionListener listener = new LoginActionListener();

        this.userNameField = new FormTextField(20);
        this.userNameField.setText(VMM.getProperty("user.name"));
        label.setLabelFor(this.userNameField);
        form.add(this.userNameField);
        this.userNameField.addActionListener(listener);
        this.userNameField.requestFocusInWindow();

        int cols = 2;
        int rows = form.getComponentCount() / cols;
        SpringUtilities.makeCompactGrid(form, rows, cols, 10, 10, 10, 10);

        center.add(form, BorderLayout.CENTER);

        JPanel buttons = new JPanel();
        JButton login = new JButton("Log In");
        login.addActionListener(listener);
        buttons.add(login);

        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {
            // JDK1.5 compatible @Override
            public void actionPerformed(final ActionEvent event) {
                ShellLoginDialog.this.close();
            }
        });
        buttons.add(cancel);

        center.add(buttons, BorderLayout.SOUTH);
    }

    public void open() {
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public void close() {
        this.setVisible(false);
        this.dispose();

        if (this.userName == null) {
            VMM vmm = VMM.getInstance();
            vmm.shutdown();
        }
    }

    public String getUserName() {
        return this.userName;
    }

    public static String showDialog() {
        ShellLoginDialog dialog = new ShellLoginDialog();
        dialog.open();

        return dialog.getName();
    }
}
