/** This Java file was created 25.1.2012 21:55:43. */
package system.turboshell.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import system.utils.SwingxUtilities;

/**
 * Class {@code ErrorDialog} serves for showing throwable in dialog.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.9
 * 
 * @since 0.1.6
 */
public class ErrorDialog extends JDialog {
    /** Error dialog initialized. */
    private boolean init;
    /** Error for dialog. */
    private final Throwable throwable;

    /** Margin for label and text area. */
    private static final int MARGIN = 20;
    /** Font for text area. */
    private static final Font ERROR_LOG_FONT = new Font("Monospaced",
            Font.BOLD, 14);

    /**
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a
     * href="http://docs.oracle.com/javase/1.4.2/docs/guide/
     * serialization/">details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 9L;

    /**
     * Class {@code ErrorOutputStream} is output stream into text area for 
     * throwable. This class serves for streaming exception into JTextArea.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.9
     * 
     * @since 0.1.6
     */
    private static final class ErrorOutputStream extends OutputStream {
        /** Text area for output stream. */
        private JTextArea textArea;

        /**
         * Constructs output stream into text area.
         *
         * @param textArea text area for output stream.
         */
        public ErrorOutputStream(final JTextArea textArea) {
            this.textArea = textArea;
        }

        /**
         * Writes the specified char to this output stream. The general contract 
         * for <code>write</code> is that one char is written to the output stream. 
         * The char to be written is the 16 low-order bits of the argument 
         * <code>c</code>. The 16 high-order bits of <code>c</code> are ignored.
         * 
         * @param c the <code>char</code>.
         */
        @Override
        public void write(final int c) {
            this.textArea.append(String.valueOf((char) c));

        }

        /**
         * Writes <code>b.length</code> bytes from the specified byte array
         * to this output stream. The general contract for <code>write(b)</code>
         * is that it should have exactly the same effect as the call
         * <code>write(b, 0, b.length)</code>.
         *
         * @param b the data.
         */
        @Override
        public void write(final byte[] b) {
            this.write(b, 0, b.length);
        }

        /**
         * Writes <code>len</code> bytes from the specified byte array
         * starting at offset <code>off</code> to this output stream.
         * The general contract for <code>write(b, off, len)</code> is that
         * some of the bytes in the array <code>b</code> are written to the
         * output stream in order; element <code>b[off]</code> is the first
         * byte written and <code>b[off+len-1]</code> is the last byte written
         * by this operation.
         *
         * @param b the data.
         * @param offset the start offset in the data.
         * @param len the number of bytes to write.
         */
        @Override
        public void write(final byte[] b, final int offset, final int len) {
            this.textArea.append(new String(b, offset, len));
        }
    }

    /**
     * Constructs error dialog.
     *
     * @param throwable error for error dialog.
     */
    public ErrorDialog(final Throwable throwable) {
        this(null, throwable);
    }

    /**
     * Constructs error dialog.
     *
     * @param comp owner for error dialog.
     * @param throwable error for error dialog.
     */
    public ErrorDialog(final Component comp, final Throwable throwable) {
        super((Frame) SwingxUtilities.getWindowAncestor(comp), true);

        this.init = false;
        this.throwable = throwable;

        this.setTitle("Error: " + this.throwable.getClass().getName());
        // this.setIconImage(ShellIcons.APPICON);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent event) {
                ErrorDialog.this.close();
            }
        });
    }

    /**
     * Serves for initializing error dialog.
     */
    public void init() {
        if (!this.init) {
            this.add(this.errorMessage(), BorderLayout.NORTH);
            this.add(this.errorLog(), BorderLayout.CENTER);
            this.add(this.button(), BorderLayout.SOUTH);

            this.init = true;
        }
    }

    /**
     * Returns label with throwable message and error icon.
     *
     * @return Label with throwable message and error icon.
     */
    private JLabel errorMessage() {
        String message = this.throwable.getLocalizedMessage();
        message = "Error message: " + message;

        JLabel errorMessage = new JLabel(message);
        errorMessage.setIcon(ShellIcons.ERROR_ICON);

        int margin = ErrorDialog.MARGIN;
        errorMessage.setBorder(BorderFactory.createEmptyBorder(margin, margin,
                margin, margin));

        return errorMessage;
    }

    /**
     * Returns text area with stack trace of throwable (error) and with scrollbars.
     *
     * @return Text area with stack trace of throwable (error) and with 
     * scrollbars.
     */
    private JScrollPane errorLog() {
        JTextArea errorLog = new JTextArea();
        errorLog.setEditable(false);
        errorLog.setFont(ErrorDialog.ERROR_LOG_FONT);
        errorLog.setForeground(Color.RED);

        ErrorOutputStream outputStream = new ErrorOutputStream(errorLog);
        PrintStream ps = new PrintStream(outputStream);
        this.throwable.printStackTrace(ps);

        JScrollPane scrolledErrorLog = new JScrollPane(errorLog);
        int margin = ErrorDialog.MARGIN;
        scrolledErrorLog.setBorder(BorderFactory.createEmptyBorder(0, margin,
                0, margin));

        scrolledErrorLog.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        scrolledErrorLog.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        errorLog.setCaretPosition(0);

        return scrolledErrorLog;
    }

    /**
     * Returns button panel.
     *
     * @return Button panel.
     */
    private JPanel button() {
        JPanel bottom = new JPanel();

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            // JDK1.5 compatible @Override
            public void actionPerformed(final ActionEvent event) {
                ErrorDialog.this.close();
            }
        });

        bottom.add(okButton);

        return bottom;
    }

    /**
     * Opens error dialog.
     */
    public void open() {
        this.init();
        this.pack();

        SwingxUtilities.fitToScreen(this);
        this.setLocationRelativeTo(this.getOwner());
        this.setVisible(true);
    }

    /**
     * Closes error dialog.
     */
    public void close() {
        this.setVisible(false);
        this.dispose();

        System.exit(1);
    }

    /**
     * Prints stack trace of throwable.
     */
    public void printStackTrace() {
        this.throwable.printStackTrace();
    }

    /**
     * Returns throwable from dialog.
     *
     * @return Throwable (Error) from dialog.
     */
    public Throwable getThrowable() {
        return this.throwable;
    }

    /**
     * Shows error dialog for specified throwable.
     *
     * @param throwable throwable (error) for error dialog.
     */
    public static void showErrorDialog(final Throwable throwable) {
        ErrorDialog.showErrorDialog(null, throwable);
    }

    /**
     * Shows error dialog for specified throwable.
     *
     * @param owner owner for dialog.
     * @param throwable throwable (error) for error dialog.
     */
    public static void showErrorDialog(final Component owner,
                                       final Throwable throwable) {
        ErrorDialog message = new ErrorDialog(owner, throwable);
        message.printStackTrace();
        message.open();
    }
}
