/**
 * This Java file was created 24.11.2013 22:58:50.
 */
package system.turboshell.gui;

import java.awt.Image;

import javax.swing.ImageIcon;

import system.utils.ResourcesLoader;

/**
 * Static class {@code ShellIcons} contains icons for swing components in system.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.5.11
 */
public class ShellIcons {
    /**
     * Private empty constructor serves for deny instancing this class, because 
     * this class is &quot;static&quot;.
     */
    private ShellIcons() {
    }

    /** Application icon for all windows. */
    public static final Image APPICON = ResourcesLoader.getImage("appicon.png",
            "Application icon");

    /** Error icon for error dialog. */
    public static final ImageIcon ERROR_ICON = ResourcesLoader.getIcon(
            "error.png", "Error");

    /** Warning icon. */
    public static final ImageIcon WARNING_ICON = ResourcesLoader.getIcon(
            "warning.png", "Warning");

    /** User icon for login dialog. */
    public static final ImageIcon USER_ICON = ResourcesLoader.getIcon(
            "user.png", "User");

}
