/**
 * This Java file was created 15.11.2013 13:03:54.
 */
package system.turboshell;

import system.turboshell.gui.ShellLoginDialog;
import system.turboshell.process.AbstractShellProcess;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.manager.ShellProcessManagerException;

/**
 * <b>ShellLogin</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.1
 * 
 * @since 0.4.10
 */
public class ShellLogin extends AbstractShellProcess {
    private VMM vmm;

    public ShellLogin() {
        this.vmm = VMM.getInstance();
    }

    // JDK1.5 compatible @Override
    public String name() {
        return "login";
    }

    @Override
    public void processMethod() {
        String userName = ShellLoginDialog.showDialog();
        VMM.setProperty("user.name", userName);

        try {
            ShellProcess caller = this.vmm.getProcessManager().getCaller(this);
            synchronized (caller) {
                caller.notify();
            }
        } catch (ShellProcessManagerException exc) {
            // no way!
        }
    }
}
