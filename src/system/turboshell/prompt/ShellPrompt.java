/**
 * This Java file was created 10.10.2013 8:15:05.
 */
package system.turboshell.prompt;

import system.turboshell.ShellCore;
import system.utils.FileUtilities;

/**
 * Class {@code ShellPrompt} represents prompt which is used in shell console 
 * and has the format:
 * 
 * <pre>
 *     WorkingDirectory>
 * </pre>
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.9
 * 
 * @since 0.1.1
 */
public class ShellPrompt implements Prompt {
    /** Shell core for obtaining working directory. */
    private ShellCore core;

    /**
     * Constructs command prompt which is used in shell console.
     *
     * @param core for obtaining working directory.
     * 
     * @throws NullPointerException whether is entered {@code null} as shell 
     * core parameter.
     */
    public ShellPrompt(final ShellCore core) {
        if (core == null) {
            throw new NullPointerException("Shell core cannot be null");
        }

        this.core = core;
    }

    /**
     * Returns command prompt which includes the path of the current working 
     * directory. Prompt has the format:
     * 
     * <pre>
     *     WorkingDirectory>
     * </pre>
     *
     * @return String representing prompt which includes the path of the current 
     * working directory.
     */
    // JDK1.5 compatible @Override
    public String getPrompt() {
        String path = FileUtilities.toString(this.core.getWorkingDirectory());

        return path + ">";
    }

    /**
     * Returns string value of prompt which is returned by {@link #getPrompt()}.
     *
     * @return String value of shell prompt.
     */
    @Override
    public String toString() {
        return this.getPrompt();
    }
}
