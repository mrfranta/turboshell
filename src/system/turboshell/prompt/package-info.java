/**
 * Package {@code system.turboshell.prompt} contains prompts for command-line 
 * interface (console). Prompt is a sequence of (one or more) characters 
 * usually ends with one of the characters $, %, #, :, &gt; and often includes
 * other information, such as the path of the current working directory.
 */
package system.turboshell.prompt;

