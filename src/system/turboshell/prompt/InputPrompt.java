/**
 * This Java file was created 8.11.2013 9:35:34.
 */
package system.turboshell.prompt;

/**
 * Class {@code InputPrompt} represents prompt which is used in shell console 
 * for user input (stdin).
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.3.0
 */
public class InputPrompt implements Prompt {
    /**
     * Returns command prompt &quot;&gt;&quot; for user input (stdin).
     *
     * @return String representing prompt for user input (stdin).
     */
    // JDK1.5 compatible @Override
    public String getPrompt() {
        return ">";
    }

    /**
     * Returns string value of prompt which is returned by {@link #getPrompt()}.
     *
     * @return String value of shell prompt.
     */
    @Override
    public String toString() {
        return this.getPrompt();
    }
}
