/**
 * This Java file was created 10.10.2013 8:11:29.
 */
package system.turboshell.prompt;

/**
 * Interface {@code Prompt} represents a command prompt, which is a sequence 
 * of (one or more) characters used in a command-line interface (console) to 
 * indicate readiness to accept commands. Its intent is to literally prompt 
 * the user to take action. A prompt usually ends with one of the characters 
 * $, %, #, :, &gt; and often includes other information, such as the path of 
 * the current working directory.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.1.1
 */
public interface Prompt {
    /**
     * Returns command prompt string.
     *
     * @return String representing command prompt.
     */
    public String getPrompt();
}
