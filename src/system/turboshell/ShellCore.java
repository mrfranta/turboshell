/**
 * This Java file was created 11.11.2013 23:01:19.
 */
package system.turboshell;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.ShellCommandListener;
import system.turboshell.command.cmd.Shell;
import system.turboshell.command.parser.CommandStructure;
import system.turboshell.command.parser.CommandsParser;
import system.turboshell.command.parser.CommandsParserException;
import system.turboshell.gui.ShellConsole;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessListener;
import system.turboshell.process.ShellProcessPosition;
import system.utils.FileUtilities;

/**
 * <b>ShellCore</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.10
 *
 * @since 0.1.6
 */
public class ShellCore {
    public List<ShellProcess> blocking = new ArrayList<ShellProcess>();
    private Map<Integer, ShellProcess> onBackground = new HashMap<Integer, ShellProcess>();
    private List<String> backgroundFinishedInfoStack = new ArrayList<String>();
    private Integer backgroundCounter = 0;

    private final Shell shellProcess;

    private final List<ShellCommandListener> staticListeners;
    private final List<ShellProcessListener> staticProcessListeners;
    private CommandsParser parser;
    private File workingDirectory;

    public final InputStream stdin;
    public final PrintStream stdout;
    public final PrintStream stderr;
    private final ShellConsole console;
    private ShellProcess latestOnBackground;

    public ShellCore(final Shell shellProcess) throws ShellCoreException {
        this.shellProcess = shellProcess;

        this.stdin = null;
        this.stdout = null;
        this.stderr = null;
        this.console = null;

        try {
            this.parser = new CommandsParser();
        } catch (Exception exc) {
            throw new ShellCoreException("Cannot initialize command parser",
                    exc);
        }

        this.staticListeners = new ArrayList<ShellCommandListener>();
        this.staticProcessListeners = new ArrayList<ShellProcessListener>();

        this.workingDirectory = new File(".");
        this.setUserWorkingDirectory();
        this.addShellProcessListener(new ConsoleShellProcessListener(this));
    }

    private void initFinalVariable(final String varName,
                                   final Object varValue) throws Exception {
        Class<?> clazz = this.getClass();
        Field field = clazz.getDeclaredField(varName);

        field.setAccessible(true);
        field.set(this, varValue);
        field.setAccessible(false);
    }

    void initConsole(final ShellConsole console) throws Exception {
        if (console == null) {
            throw new NullPointerException("Console cannot be null");
        }

        InputStream stdin = console.getInputStream();
        PrintStream stdout = console.getOutputStream();
        PrintStream stderr = console.getErrorStream();

        if (stdin == null) {
            throw new NullPointerException("Standard input cannot be null");
        }

        if (stdout == null) {
            throw new NullPointerException("Standard output cannot be null");
        }

        this.initFinalVariable("stdin", stdin);
        this.initFinalVariable("stdout", stdout);

        if (stderr == null) {
            this.initFinalVariable("stderr", stdout);
        }
        else {
            this.initFinalVariable("stderr", stderr);
        }

        this.initFinalVariable("console", console);
    }

    public void addShellCommandListener(final ShellCommandListener listener) {
        if (listener == null) {
            return;
        }

        this.staticListeners.add(listener);
    }

    public void removeShellCommandListener(final ShellCommandListener listener) {
        if (listener == null) {
            return;
        }

        this.staticListeners.remove(listener);
    }

    public ShellCommandListener[] getShellCommandListeners() {
        ShellCommandListener[] array = new ShellCommandListener[this.staticListeners.size()];

        return this.staticProcessListeners.toArray(array);
    }

    public void addShellProcessListener(final ShellProcessListener listener) {
        if (listener == null) {
            return;
        }

        this.staticProcessListeners.add(listener);
    }

    public void removeShellProcessListener(final ShellProcessListener listener) {
        if (listener == null) {
            return;
        }

        this.staticProcessListeners.remove(listener);
    }

    public ShellProcessListener[] getShellProcessListeners() {
        ShellProcessListener[] array = new ShellProcessListener[this.staticProcessListeners.size()];

        return this.staticProcessListeners.toArray(array);
    }

    private ShellCommand createCommand(final CommandStructure structure) throws ShellCoreException {
        String clazzName = structure.getCommandClassName();

        Class<?> clazz = null;
        try {
            clazz = Class.forName(clazzName);
        } catch (ClassNotFoundException exc) {
            throw new ShellCoreException(structure.getName()
                    + ": command not found", exc);
        }

        try {
            Constructor<?>[] constructs = clazz.getConstructors();

            ShellCommand command = null;
            for (Constructor<?> construct : constructs) {
                if (construct.getParameterTypes().length == 0) {
                    command = (ShellCommand) construct.newInstance();
                    continue;
                }

                if (construct.getParameterTypes().length == 1) {
                    command = (ShellCommand) construct.newInstance(this);
                    break;
                }
            }

            command.init(structure);

            return command;
        } catch (ShellCommandException exc) {
            throw new ShellCoreException(exc.getMessage(), exc);
        } catch (Exception exc) {
            throw new ShellCoreException(structure.getName()
                    + ": cannot execute command", exc);
        }
    }

    public void execute(final String command) throws ShellCoreException {
        CommandStructure[] structures;

        try {
            structures = this.parser.process(command);
            RegExpInterpreter.interpreteRegexps(structures,
                    this.getWorkingDirectory());
        } catch (CommandsParserException exc) {
            throw new ShellCoreException("shell: " + exc.getMessage(), exc);
        }

        ShellCommand commands = null;
        ShellCommand actual = null;
        for (int i = 0; i < structures.length; i++) {
            ShellCommand cmd = this.createCommand(structures[i]);

            if (commands == null) {
                commands = cmd;
                actual = cmd;

                continue;
            }

            try {
                actual.setPipe(cmd);
                actual = cmd;
            } catch (ShellCommandException exc) {
                throw new ShellCoreException("shell: " + exc.getMessage(), exc);
            }
        }

        for (ShellCommand cmd = commands; cmd != null; cmd = cmd.getPipe()) {
            for (ShellProcessListener listener : this.staticProcessListeners) {
                cmd.getProcess().addListener(listener);
            }

            for (ShellCommandListener listener : this.staticListeners) {
                cmd.addListener(listener);
            }
        }

        this.shellProcess.executeCommand(commands);
    }

    public ShellProcess getShellProcess() {
        return this.shellProcess.getProcess();
    }

    public File getWorkingDirectory() {
        return this.workingDirectory;
    }

    public String getUserWorkingDirectory() {
        return VMM.getProperty("user.dir");
    }

    public String getUserHomeDirectory() {
        return VMM.getProperty("user.home");
    }

    public void setUserWorkingDirectory() throws ShellCoreException {
        this.setWorkingDirectory(this.getUserWorkingDirectory());
    }

    public void setUserHomeDirectory() throws ShellCoreException {
        this.setWorkingDirectory(this.getUserHomeDirectory());
    }

    public synchronized void setWorkingDirectory(final String workingDirectory) throws ShellCoreException {
        if (workingDirectory == null) {
            throw new ShellCoreException("name of working directory "
                    + "can't be null");
        }

        String path = workingDirectory;
        if (path.equals("..")) {
            File parent = this.workingDirectory.getParentFile();
            if (parent != null) {
                this.setWorkingDirectory(parent);
            }

            return;
        }

        if (path.equals("\\") || path.equals("/")) {
            File dir = this.workingDirectory.getParentFile();

            if (dir == null) {
                dir = this.workingDirectory;
            }

            File p = dir.getParentFile();
            while (p != null) {
                dir = p;
                p = p.getParentFile();
            }

            this.setWorkingDirectory(dir);
            return;
        }

        if (path.startsWith("\\") || path.startsWith("/")) {
			  if(System.getProperty("os.name").toLowerCase().contains("win")) {
			   String wdPath = this.workingDirectory.getAbsolutePath();

            Pattern pattern = Pattern.compile("^[a-zA-Z]:\\\\");
            Matcher matcher = pattern.matcher(wdPath);
            // windows
            if (matcher.find()) {
                path = wdPath.substring(0, matcher.end()) + path;
            }
			  }

            this.setWorkingDirectory(new File(path));
            return;
        }

        if (path.equals("~")) {
            this.setWorkingDirectory(new File(this.getUserHomeDirectory()));

            return;
        }

        if (path.startsWith("~")) {
            path = this.getUserHomeDirectory()
                    + VMM.getProperty("file.separator") + path.substring(1);
            this.setWorkingDirectory(new File(path));

            return;
        }

        File dir = new File(path);
        if (dir.isAbsolute()) {
            this.setWorkingDirectory(dir);

            return;
        }

        // relative path
        path = this.workingDirectory + VMM.getProperty("file.separator") + path;
        this.setWorkingDirectory(new File(path));
    }

    public synchronized void setWorkingDirectory(final File workingDirectory) throws ShellCoreException {
        if (workingDirectory == null) {
            throw new ShellCoreException("working directory can't be null");
        }

        try {
            String path = workingDirectory.getCanonicalPath();
            File dir = new File(path);

            if (!dir.exists()) {
                path = FileUtilities.toString(workingDirectory);

                throw new ShellCoreException(path
                        + ": working directory doesn't exist");
            }

            if (!dir.isDirectory()) {
                path = FileUtilities.toString(workingDirectory);

                throw new ShellCoreException(path + ": isn't directory");
            }

            this.workingDirectory = dir;
        } catch (IOException exc) {
            String path = FileUtilities.toString(workingDirectory);

            throw new ShellCoreException(path
                    + ": cannot set working directory: " + exc.getMessage(),
                    exc);
        }
    }

    public synchronized File getFile(final String filePath) {
        String path = filePath;
        if (path.startsWith("\\") || path.startsWith("/")) {
            String wdPath = this.workingDirectory.getAbsolutePath();

            Pattern pattern = Pattern.compile("^[a-zA-Z]:\\\\");
            Matcher matcher = pattern.matcher(wdPath);
            // windows
            if (matcher.find()) {
                path = wdPath.substring(0, matcher.end()) + path;
            }
        }
        else if (path.startsWith("~")) {
            path = this.getUserHomeDirectory()
                    + VMM.getProperty("file.separator") + path.substring(1);
        }

        File file = new File(path);
        if (!file.isAbsolute()) {
            file = new File(this.workingDirectory
                    + VMM.getProperty("file.separator") + path);
        }

        try {
            return file.getCanonicalFile();
        } catch (IOException exc) {
            return file;
        }
    }

    /* Foreground/background workings */
    public void sendToBackground(final ShellProcess process) {
        this.blocking.remove(process);
        if (this.onBackground.isEmpty()) {
            this.backgroundCounter = 0; // reset counter
        }

        this.backgroundCounter++;
        process.setPosition(ShellProcessPosition.BACKGROUND);
        this.onBackground.put(this.backgroundCounter, process);
        this.console.append("[" + this.backgroundCounter + "] " + process.getPID() + "\n");
        if (this.blocking.isEmpty()) {
            this.console.setBlocked(false);
            this.console.appendPromptLine();
        }

        this.latestOnBackground = process;
    }

    public void finishedOnBackground(final ShellProcess process) {
        int id = this.findProcessJobNumber(process);
        this.onBackground.remove(id);
        this.backgroundFinishedInfoStack.add("[" + id + "] Done " +
                process.getPID() + " " + process.name() + "\n");
    }

    public int findProcessJobNumber(final ShellProcess process) {
        for (Map.Entry<Integer, ShellProcess> entry : this.onBackground.entrySet()) {
            int integer = entry.getKey();
            ShellProcess shProc = entry.getValue();
            if (shProc.equals(process)) {
                return integer;
            }
        }

        return -1;
    }

    public void finishedOnForeground(final ShellProcess process) {
        boolean result = this.blocking.remove(process);

        if (result && this.blocking.isEmpty()) {
            this.console.setBlocked(false);

            this.console.appendPromptLine();
            this.console.setCommand(this.console.buffer.toString());
            this.console.buffer.setLength(0);
        }
    }

    public void appendBackgroundInfo() {
        for (Iterator<String> it = this.backgroundFinishedInfoStack.listIterator(); it.hasNext();) {
            this.console.append(it.next());
            it.remove();
        }
    }

    public void processReady(final ShellProcess process) {
        this.blocking.add(process);
        this.console.setBlocked(true);
    }

    public String getJobsInfo() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, ShellProcess> entry : this.onBackground.entrySet()) {
            int id = entry.getKey();
            ShellProcess proc = entry.getValue();
            sb.append("[").append(id).append("] Running ").append(proc.getPID());
            sb.append(" ").append(proc.name()).append("\n");
        }
        return sb.toString();

    }

    public ShellProcess getJob(final int targetJob) {
        return this.onBackground.get(targetJob);
    }

    public ShellProcess getJob() {
        return this.latestOnBackground;
    }

    public void sendToForeground(final ShellProcess process) {
        this.onBackground.remove(this.findProcessJobNumber(process));
        this.blocking.add(process);
        process.setPosition(ShellProcessPosition.FOREGROUND);
        this.console.append(process.name() + "\n");
        this.console.setBlocked(true);
    }

    public ShellProcess getProcess(final int targetPid) {
        return VMM.getInstance().getProcessManager().getProcess(targetPid);
    }
}
