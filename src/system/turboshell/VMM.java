/**
 * This Java file was created 11.11.2013 12:39:29.
 */
package system.turboshell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import system.turboshell.command.cmd.Shell;
import system.turboshell.gui.ErrorDialog;
import system.turboshell.process.AbstractShellProcess;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.manager.ShellProcessManager;
import system.utils.ResourcesLoader;

/**
 * <b>VMM</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @author pkopac (Petr Kopač)
 * @version 1.8
 * 
 * @since 0.4.0
 */
public class VMM extends AbstractShellProcess {
    private ShellProcessManager processManager;
    private List<WaitingProcess> waitingProcesses;

    private static VMM instance;

    private static final Map<String, String> properties = new HashMap<String, String>();

    private class WaitingProcess {
        private ShellProcess caller;
        private ShellProcess process;
        private List<ShellProcess> dependent;

        public WaitingProcess(final ShellProcess caller,
                              final ShellProcess process) {
            this.caller = caller;
            this.process = process;

            if (caller == null) {
                this.caller = VMM.this;
            }
        }

        public WaitingProcess(final ShellProcess caller,
                              final ShellProcess[] processes) {
            this.caller = caller;
            this.dependent = new ArrayList<ShellProcess>();
            for (ShellProcess process : processes) {
                this.dependent.add(process);
            }

            if (caller == null) {
                this.caller = VMM.this;
            }
        }

        public ShellProcess getCaller() {
            return this.caller;
        }

        public ShellProcess getProcess() {
            return this.process;
        }

        public ShellProcess[] getProcesses() {
            ShellProcess[] array = new ShellProcess[this.dependent.size()];

            return this.dependent.toArray(array);
        }
    }

    /**
     * Private empty constructor serves for deny instancing this class, because 
     * this class is &quot;static&quot;.
     */
    private VMM() {
        this.processManager = new ShellProcessManager(this);
        this.waitingProcesses = new ArrayList<WaitingProcess>();
    }

    /**
     * Initializes properties of VMM.
     */
    static {
        VMM.properties.put("file.separator", "/");
        VMM.properties.put("line.separator", "\n");
        VMM.properties.put("os.name", "TurboShell");
        VMM.properties.put("os.version", "unknown");

        try {
            URL versionURL = ResourcesLoader.getResource("../version.txt");
            if (versionURL != null) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        versionURL.openStream()));
                VMM.properties.put("os.version", br.readLine());
                br.close();
            }
        } catch (IOException exc) {
            // os.version will be "unknown"
        }

        VMM.properties.put("os.root", "/");
        VMM.properties.put("user.name", "");
        VMM.properties.put("user.home", System.getProperty("user.home"));
        VMM.properties.put("user.dir", System.getProperty("user.dir"));
    }

    // JDK1.5 compatible @Override
    public String name() {
        return "vmm (init)";
    }

    public ShellProcessManager getProcessManager() {
        return this.processManager;
    }

    @Override
    public synchronized void processMethod() {
        try {
            ShellLogin login = new ShellLogin();
            this.processManager.addProcess(this, login);

            this.wait();

            Shell shell = new Shell();
            this.processManager.addProcess(this, shell.getProcess());
        } catch (InterruptedException exc) {
            // no action
            return;
        }

        try {
            while (true) {
                this.wait();

                for (int i = 0; i < this.waitingProcesses.size(); i++) {
                    WaitingProcess process = this.waitingProcesses.remove(i);

                    ShellProcess sp = process.getProcess();
                    if (sp == null) {
                        this.processManager.addProcesses(
                                process.getCaller(),
                                process.getProcesses());
                    }
                    else {
                        this.processManager.addProcess(process.getCaller(),
                                sp);
                    }
                }
            }
        } catch (InterruptedException exc) {
            // no action
        }
    }

    public synchronized void startProcess(final ShellProcess process) {
        this.startProcess(null, process);
    }

    public synchronized void startProcess(final ShellProcess caller,
                                          final ShellProcess process) {
        if (process == null) {
            return;
        }

        WaitingProcess waitingProcess = new WaitingProcess(caller, process);
        this.waitingProcesses.add(waitingProcess);
        this.notify();
    }

    public synchronized void startProcesses(final ShellProcess[] processes) {
        this.startProcesses(null, processes);
    }

    public synchronized void startProcesses(final ShellProcess caller,
                                            final ShellProcess[] processes) {
        if (processes == null) {
            return;
        }

        WaitingProcess waitingProcess = new WaitingProcess(caller, processes);
        this.waitingProcesses.add(waitingProcess);
        this.notify();
    }

    public void shutdown() {
        this.kill();
    }

    @Override
    public void kill() {
        if (!this.isKilled()) {
            this.getProcessManager().killAll();
            this.interrupt();
            System.exit(0);
        }
    }

    public static String getProperty(final String propertyName) {
        return VMM.properties.get(propertyName);
    }

    public static void setProperty(final String propertyName,
                                   final String newValue) {
        if (VMM.properties.get(propertyName) != null) {
            VMM.properties.put(propertyName, newValue);
        }
    }

    public static VMM getInstance() {
        if (VMM.instance == null) {
            VMM.instance = new VMM();
        }

        return VMM.instance;
    }

    public static void startVMM() {
        VMM vmm = VMM.getInstance();
        vmm.start();
    }

    public static void shutdownVMM() {
        VMM vmm = VMM.getInstance();
        vmm.shutdown();
    }

    public static void main(final String[] args) {
        try {
            VMM.startVMM();
        } catch (Throwable t) {
            ErrorDialog.showErrorDialog(t);
        }
    }
}
