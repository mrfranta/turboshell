package system.turboshell;

import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessAdapter;
import system.turboshell.process.ShellProcessPosition;

/**
 *
 * @author pkopac
 */
class ConsoleShellProcessListener extends ShellProcessAdapter {
    private ShellCore core;

    public ConsoleShellProcessListener(final ShellCore core) {
        this.core = core;
    }

    @Override
    public void processReady(final ShellProcess process) {
        this.core.processReady(process);
    }

    @Override
    public void processStarted(final ShellProcess process) {
        if (process.getPosition() == ShellProcessPosition.BACKGROUND) {
            this.core.sendToBackground(process);
        }
    }

    @Override
    public void processKilled(final ShellProcess process) {
        this.processFinished(process);
    }

    @Override
    public void processFinished(final ShellProcess process) {
        if (process.getPosition() == ShellProcessPosition.FOREGROUND) {
            this.core.finishedOnForeground(process);
        }
        else {
            this.core.finishedOnBackground(process);
        }
    }

}
