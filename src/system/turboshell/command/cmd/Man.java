/**
 * This Java file was created 13.11.2013 23:50:15.
 */
package system.turboshell.command.cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.utils.ResourcesLoader;

/**
 * <b>Man</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.1
 *
 * @since 0.4.8
 */
public class Man extends AbstractShellCommand {

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O
     * and working directory.
     */
    public Man(final ShellCore shellCore) {
        super(shellCore);
    }

    // JDK1.5 compatible @Override
	 /**
	  * Prints the manual page after checking user arguments.
	  * @return 
	  */
    public int execute() {
        String[] args = this.getArguments();
        PrintStream output = this.getOutputStream();
        PrintStream error = this.getErrorStream();

        if (args.length == 0) {
            error.println("What manual page do you want?");

            return ShellCommand.EXIT_FAILURE;
        }

        int i = 0;
        for (String arg : args) {
            if (i > 0) {
                output.println();
                output.println();
            }

            if (arg.startsWith("-")) {
                if (arg.equals("--help")) {
                    output.println(this.getManual());

                    return 0;
                }
                else {
                    error.println(this.name() + ": " + arg + ": invalid option");
                    error.println(this.toString());

                    return 1;
                }
            }

            String manual = "";
            URL manualURL = ResourcesLoader.getManual(arg + ".txt");
            if (manualURL != null) {
                BufferedReader br = null;
                try {
                    br = new BufferedReader(new InputStreamReader(
                            manualURL.openStream(), "UTF-8"));
                    for (String line = br.readLine(); line != null; line = br.readLine()) {
                        manual += line + "\n";
                    }

                    manual = manual.substring(0, manual.length() - 1);
                } catch (Exception exc) {
                    error.println(this.name() + ": cannot get manual entry for " + args[i]);
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException ex) {
                            // ignore as we don't care
                        }
                    }

                    return ShellCommand.EXIT_FAILURE;
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException exc) {
                            // ignore as we don't care
                        }
                    }
                }
            }
            else {
                error.println(this.name() + ": no manual entry for " + args[i]);

                return ShellCommand.EXIT_FAILURE;
            }

            output.println(manual);

            i++;
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Returns string which represents usage of this command which has format:
     *
     * <pre>
     *     man: usage: [OPTIONS] PAGE...
     * </pre>
     *
     * @return String which represents usage of this command.
     */
    @Override
    public String toString() {
        return this.name() + ": usage: man [OPTIONS] PAGE...";
    }
}
