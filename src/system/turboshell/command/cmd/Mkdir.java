/**
 * This Java file was created 25.11.2013 20:07:20.
 */
package system.turboshell.command.cmd;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.parser.CommandStructure;

/**
 * Class {@code Mkdir} represents command {@code mkdir} which creates the 
 * directory(ies), if they do not already exist.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.5.4
 */
public class Mkdir extends AbstractShellCommand {
    /** List of directories to create. */
    private List<File> directories;
    /** List of paths for directories which was given as parameters. */
    private List<String> names;

    /** Information whether will be created parents for directories. */
    private boolean parents;
    /** 
     * Information whether will be printed informations about directory 
     * creation.
     */
    private boolean verbose;

    /** Version of this command. */
    public static final String VERSION = "1.0";

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Mkdir(final ShellCore shellCore) {
        super(shellCore);

        this.directories = new ArrayList<File>();
        this.names = new ArrayList<String>();
    }

    /**
     * Executes command. Checks whether the command has some arguments. If 
     * command has no arguments, it will write error. Then parses arguments and 
     * creates directories which was given as arguments.
     *
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    // JDK1.5 compatible @Override
    public int execute() {
        String[] args = this.getArguments();
        PrintStream error = this.getErrorStream();

        if (args.length == 0) {
            error.println(this.name() + ": missing operand");
            error.println("Try '" + this.name() + " --help' for more information.");

            return ShellCommand.EXIT_FAILURE;
        }

        try {
            String res = this.parseArgs(args);
            if (res != null) {
                this.getOutputStream().println(res);

                return ShellCommand.EXIT_SUCCESS;
            }
        } catch (ShellCommandException exc) {
            this.getErrorStream().println(exc.getMessage());

            return ShellCommand.EXIT_FAILURE;
        }

        if (this.directories.isEmpty()) {
            return ShellCommand.EXIT_FAILURE;
        }

        this.makeDirectories();

        return ShellCommand.EXIT_SUCCESS;
    }

    /** 
     * Prints help for this command.
     */
    protected void printHelp() {
        this.getOutputStream().println(this.getManual());
    }

    /**
     * Prints version of this command.
     */
    protected void printVersion() {
        this.getOutputStream().println(this.getVersion());
    }

    /**
     * Parses arguments which was given to command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints 
     * something.
     */
    private String parseArgs(final String[] args) throws ShellCommandException {
        String res = null;
        for (int i = 0; i < args.length; i++) {
            String arg = CommandStructure.removeEscapes(args[i]);
            if (arg.startsWith("--")) {
                res = this.parseArgsInternal(arg.substring(2));
                if (res != null) {
                    return res;
                }
            }
            else if (arg.startsWith("-")) {
                char[] charAr = arg.substring(1).toCharArray();
                for (char c : charAr) {
                    switch (c) {
                        case 'p':
                            this.parseArgsInternal("parents");
                            break;

                        case 'v':
                            this.parseArgsInternal("verbose");
                            break;

                        default:
                            throw new ShellCommandException(this.name() +
                                    ": invalid option '" + c + "'\n" +
                                    "Try '" + this.name() +
                                    " --help' for more information.");
                    }
                }
            }
            else {
                this.directories.add(this.core.getFile(arg));
                this.names.add(arg);
            }
        }

        return null;
    }

    private String parseArgsInternal(final String arg) throws ShellCommandException {
        if (arg.equals("parents")) {
            this.parents = true;
            return null;
        }
        else if (arg.equals("verbose")) {
            this.verbose = true;
            return null;
        }
        else if (arg.equals("help")) {
            return this.getManual();
        }
        else if (arg.equals("version")) {
            return this.getVersion();
        }
        else {
            throw new ShellCommandException(this.name() +
                    ": invalid option '" + arg + "'\n" +
                    "Try '" + this.name() +
                    " --help' for more information.");
        }
    }

    /**
     * Creates directories which was given as parameters.
     */
    private void makeDirectories() {
        int i = 0;
        for (File file : this.directories) {
            String name = this.names.get(i);

            if (file.exists()) {
                this.getErrorStream().println(this.name() +
                        ": cannot create directory \"" +
                        name + "\": File exists");
                continue;
            }

            boolean result = false;
            if (this.parents) {
                if (this.verbose) {
                    Stack<File> stack = new Stack<File>();
                    File parent = file.getParentFile();
                    while (!parent.exists()) {
                        stack.push(parent);
                        parent = parent.getParentFile();
                    }

                    while (!stack.isEmpty()) {
                        parent = stack.pop();
                        result = this.mkdir(parent, parent.getName());
                        if (!result) {
                            break;
                        }
                    }

                    if (!result) {
                        continue;
                    }

                    result = this.mkdir(file, name);
                }
                else {
                    result = file.mkdirs();
                    if (!result) {
                        this.getErrorStream().println(this.name() +
                                ": cannot create directory \"" + name + "\"");
                    }
                }
            }
            else {
                result = this.mkdir(file, name);
            }

            i++;
        }
    }

    /**
     * Create specified directory and if is activated verbose, it will prints 
     * information or error message.
     *
     * @param dir directory to create.
     * @param name path for directory which was given as parameter.
     * @return Information whether was directory created.
     */
    private boolean mkdir(final File dir, final String name) {
        boolean result = dir.mkdir();

        if (this.verbose && result) {
            this.getOutputStream().println(this.name() +
                    ": created directory \"" + name + "\"");
        }

        if (!result) {
            this.getErrorStream().println(this.name() +
                    ": cannot create directory \"" + name + "\"");
        }

        return result;
    }

    /**
     * Returns version of command mkdir in format:
     *
     * <pre>
     *     mkdir (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of cat command.
     */
    public String getVersion() {
        return this.name() + " (PowerShell utils) " + Mkdir.VERSION;
    }

    /**
     * Returns string which represents usage of this command which has format:
     * 
     * <pre>
     *     mkdir: usage: mkdir [OPTION]... DIRECTORY...
     * </pre>
     *
     * @return String which represents usage of this command.
     */
    @Override
    public String toString() {
        return this.name() + ": usage: " +
                this.name() + " [OPTION]... DIRECTORY...";
    }
}
