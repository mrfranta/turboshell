package system.turboshell.command.cmd;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;

/**
 * Class {@code Jobs} represents command {@code jobs} which prints information 
 * about currently running jobs in the executing shell.
 * 
 * @author pkopac (Petr Kopač)
 * @version 1.0
 */
public class Jobs extends AbstractShellCommand {
    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O
     * and working directory.
     */
    public Jobs(final ShellCore shellCore) {
        super(shellCore);
    }

    /**
     * Prints information about currently running jobs in the executing shell.
     *
     * @return Always 0.
     */
    public int execute() {
        this.getOutputStream().append(this.core.getJobsInfo());

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Returns shell command specific usage.
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "";
    }
}
