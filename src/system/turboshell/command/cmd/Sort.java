package system.turboshell.command.cmd;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.InvalidOptionException;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.parser.CommandStructure;

/**
 * Class {@code Sort} represents command {@code sort}. Sorts the lines of input.
 * Internally uses {@code cat} for input concatenation if needed.
 *
 * @author pkopac (Petr Kopač)
 * @version 1.0
 *
 * @since 0.4.11
 */
public class Sort extends AbstractShellCommand {
    /**
     * List of source files which will be read.
     */
    private List<String> sources;
    public static final String VERSION = "1.0";
    private boolean errorField;
    private boolean ignoreLeadingBlanks;
    private boolean ignoreCase;
    private boolean reverse;
    private Cat cat;

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O
     * and working directory.
     */
    public Sort(final ShellCore shellCore) {
        super(shellCore);
    }

    // JDK1.5 compatible @Override
    public int execute() {
        String[] args = this.getArguments();
        PrintStream output = this.getOutputStream();
        InputStream input = this.getInputStream();
        PrintStream error = this.getErrorStream();

        try {
            String res = this.parseArgs(args);
            if (res != null) {
                output.println(res);

                return ShellCommand.EXIT_SUCCESS;
            }

            if (this.errorField && this.sources.isEmpty()) {
                return ShellCommand.EXIT_FAILURE;
            }

            /* Reuse cat for concatenating */
            String inputConcatenatedString = this.reuseCat(input, error);
            String[] array = inputConcatenatedString.split("\n");
            Arrays.sort(array, new InternalComparator());
            for (String string : array) {
                output.println(string);
            }
        } catch (Exception exc) {
            this.printError(exc.getMessage());

            return ShellCommand.EXIT_FAILURE;
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Parses arguments which was given to command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints
     * something.
     * @throws ShellCommandException whether occurs some error in arguments
     * parsing.
     */
    private String parseArgs(final String[] arguments) throws ShellCommandException {
        this.sources = new ArrayList<String>();
        boolean stdinInQueue = false, ignoreParameters = false;
        String res = null;
        for (int i = 0; i < arguments.length; i++) {
            String arg = CommandStructure.removeEscapes(arguments[i]);
            if (arg.startsWith("-") && !arg.equals("-")) {
                // take the first occurance
                if (arg.equals("-") && !stdinInQueue) {
                    this.sources.add(null);
                    stdinInQueue = true;
                }
                else if (arg.equals("--")) {
                    ignoreParameters = true;
                } // long parameters
                else if (arg.startsWith("--") && !ignoreParameters) {
                    res = this.parseArgsInternal(arg.substring(2));
                    if (res != null) {
                        return res;
                    }
                }
                else if (!ignoreParameters) { // parameters
                    char[] charAr = arg.substring(1).toCharArray();
                    for (char c : charAr) {
                        switch (c) {
                            case 'b':
                                this.parseArgsInternal("ignore-leading-blanks");
                                break;

                            case 'f':
                                this.parseArgsInternal("ignore-case");
                                break;

                            case 'r':
                                this.parseArgsInternal("reverse");
                                break;

                            default:
                                throw new InvalidOptionException(this, c);
                        }
                    }
                }
            }
            else {
                File source = this.core.getFile(arg);
                File output = this.getOutputFile();
                if (output != null && output.getAbsoluteFile().equals(
                        source.getAbsoluteFile())) {
                    if (!this.isOutputAppend()) {
                        this.printError(arg + ": input file is output file");
                        this.errorField = true;

                        continue;
                    }
                }

                if (source.isDirectory()) {
                    this.printError(arg + ": is a directory");
                    this.errorField = true;

                    continue;
                }

                if (!source.exists()) {
                    this.printError(arg + ": doesn't exist");
                    this.errorField = true;

                    continue;
                }

                this.sources.add(arg);
            }
        }

        return null;
    }

    /**
     * Long version of arguments set the feature flags.
     *
     * @param arg
     * @return
     * @throws ShellCommandException
     */
    private String parseArgsInternal(final String arg) throws ShellCommandException {
        if (arg.equals("ignore-leading-blanks")) {
            this.ignoreLeadingBlanks = true;
            return null;
        }
        else if (arg.equals("ignore-case")) {
            this.ignoreCase = true;
            return null;
        }
        else if (arg.equals("reverse")) {
            this.reverse = true;
            return null;
        }
        else {
            throw new InvalidOptionException(this, arg);
        }
    }

    /**
     * Returns version of command sort in format:
     *
     * <pre>
     *     sort (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of sort command.
     */
    public String getVersion() {
        return "sort (PowerShell utils) " + Sort.VERSION;
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [OPTION] [FILE]...
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[OPTION] [FILE]...";
    }

    /**
     * Wrapper method around the cat command.
     *
     * @param input
     * @param error
     * @return
     * @throws ShellCommandException
     */
    private String reuseCat(final InputStream input, final PrintStream error) throws ShellCommandException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        CommandStructure s = new CommandStructure("cat");
        s.setParameters(this.sources);
        this.cat = new Cat(this.core);
        this.cat.init(s);
        this.cat.setErrorStream(error);
        this.cat.setInputStream(input);
        this.cat.setOutputStream(ps);
        this.cat.execute();

        return new String(baos.toByteArray());

    }

    @Override
    public void done() {
        if (this.cat != null) {
            this.cat.done();
        }

        super.done();
    }

    /**
     * Comparator, which respects the feature flags.
     */
    private class InternalComparator implements Comparator<String> {
        Collator coll = Collator.getInstance(Locale.getDefault());
        {
            this.coll.setStrength(Collator.TERTIARY);
        }

        public int compare(String o1, String o2) {
            if (Sort.this.ignoreLeadingBlanks) {
                o1 = o1.trim();
                o2 = o2.trim();
            }

            if (Sort.this.ignoreCase) {
                o1 = o1.toLowerCase();
                o2 = o2.toLowerCase();
            }

            if (!Sort.this.reverse) {
                return this.coll.compare(o1, o2);
            }
            else {
                return this.coll.compare(o2, o1);
            }

        }
    }
}
