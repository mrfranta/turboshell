/**
 * This Java file was created 12.11.2013 12:26:50.
 */
package system.turboshell.command.cmd;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import system.turboshell.ShellCore;
import system.turboshell.ShellCoreException;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.gui.ErrorDialog;
import system.turboshell.gui.ShellFrame;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessStatus;
import system.turboshell.process.manager.ShellProcessManager;
import system.turboshell.process.manager.ShellProcessManagerException;

/**
 * <b>Shell</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.4.1
 */
public class Shell extends AbstractShellCommand {
    private ShellFrame frame;
    private ShellCoreException frameException;
    private File workingDirectory;

    private final List<ShellCommand> waitingCommands;

    public Shell() {
        this(null);
    }

    public Shell(final ShellCore core) {
        super(null);

        if (core != null) {
            this.workingDirectory = core.getWorkingDirectory();
        }

        this.frame = null;
        this.frameException = null;
        this.waitingCommands = new ArrayList<ShellCommand>(1);
    }

    private ShellFrame getShellFrame() {
        return this.frame;
    }

    // JDK1.5 compatible @Override
    public synchronized int execute() {
        SwingUtilities.invokeLater(new Runnable() {
            // JDK1.5 compatible @Override
            public void run() {
                try {
                    ShellCore core = new ShellCore(Shell.this);
                    if (Shell.this.workingDirectory != null) {
                        core.setWorkingDirectory(Shell.this.workingDirectory);
                    }

                    Shell.this.frame = new ShellFrame(core);
                    Shell.this.frame.open();

                } catch (ShellCoreException exc) {
                    Shell.this.frameException = exc;
                } finally {
                    synchronized (Shell.this) {
                        Shell.this.notify();
                    }
                }
            }
        });

        try {
            this.wait();
        } catch (InterruptedException exc) {
            // no action
        }

        if (this.frameException != null) {
            ErrorDialog.showErrorDialog(this.frame, this.frameException);
        }

        try {
            while (true) {
                this.wait();
                for (int i = 0; i < this.waitingCommands.size(); i++) {
                    this.startCommand(this.waitingCommands.remove(i));
                }
            }
        } catch (InterruptedException exc) {
            // no action
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    private void startCommand(final ShellCommand command) {
        VMM vmm = VMM.getInstance();
        if (command.getPipe() == null) {
            ShellProcess process = command.getProcess();
            vmm.startProcess(this.getProcess(), process);

            return;
        }

        ArrayList<ShellProcess> dependent = new ArrayList<ShellProcess>();
        dependent.add(command.getProcess());
        for (ShellCommand pipe = command.getPipe(); pipe != null; pipe = pipe.getPipe()) {
            dependent.add(pipe.getProcess());
        }
        ShellProcess[] array = new ShellProcess[dependent.size()];
        vmm.startProcesses(this.getProcess(), dependent.toArray(array));
    }

    @Override
    protected void actionAtKill() {
        ShellFrame frame = this.getShellFrame();
        if (frame.isVisible()) {
            frame.setVisible(false);
        }

        try {
            VMM vmm = VMM.getInstance();
            ShellProcessManager manager = vmm.getProcessManager();
            ShellProcess[] subprocesses = manager.getSubprocesses(vmm);
            for (ShellProcess process : subprocesses) {
                if (process.name().equals("shell") &&
                        process != this.getProcess() &&
                        process.getStatus() == ShellProcessStatus.RUNNING) {
                    return;
                }
            }

            vmm.shutdown();
        } catch (ShellProcessManagerException exc) {
            // impossible
        }
    }

    public synchronized void executeCommand(final ShellCommand command) {
        if (command == null) {
            return;
        }

        this.waitingCommands.add(command);
        this.notify();
    }
}
