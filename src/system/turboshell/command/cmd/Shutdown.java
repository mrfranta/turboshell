/**
 * This Java file was created 20.11.2013 1:55:07.
 */
package system.turboshell.command.cmd;

import java.io.PrintStream;

import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;

/**
 * Class {@code Shutdown} represents command {@code shutdown} which brings the 
 * system down. This command also allows system reboot.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.4.14
 */
public class Shutdown extends AbstractShellCommand {
    // /** Information whether will be system rebooted. */
    // private boolean reboot;
    /** Information whether was found some error during parsing arguments. */
    private boolean error;

    /** Version of this command. */
    public static final String VERSION = "1.0";

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Shutdown(final ShellCore shellCore) {
        super(shellCore);

        // this.reboot = false;
        this.error = false;
    }

    /**
     * Executes command. Parses arguments and shutdowns system.
     *
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    // JDK1.5 compatible @Override
    public int execute() {
        if (!this.parseArgs(this.getArguments())) {
            return ShellCommand.EXIT_SUCCESS;
        }

        if (this.error) {
            return ShellCommand.EXIT_FAILURE;
        }

        // if (this.reboot) {
        // this.getOutputStream().println("The system is going to reboot NOW!");
        //
        // VMM.restartVMM();
        // return ShellCommand.EXIT_SUCCESS;
        // }

        this.getOutputStream().println("The system is going to shutdown NOW!");

        VMM.shutdownVMM();

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Parses arguments which was given to command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running.
     */
    private boolean parseArgs(final String[] arguments) {
        for (String arg : arguments) {
            // if (arg.equals("-r") || arg.equals("--reboot")) {
            // this.reboot = true;
            // }
            // else
            if (arg.equals("--help")) {
                this.printHelp();

                return false;
            }
            else if (arg.equals("--version")) {
                this.printVersion();

                return false;
            }
            else {
                PrintStream error = this.getErrorStream();
                error.println(this.name() + ": " + arg + ": invalid option");
                error.println("Try '" + this.name() +
                        " --help' for more information.");

                this.error = true;
                return true;
            }
        }

        return true;
    }

    /** 
     * Prints help for this command.
     */
    protected void printHelp() {
        this.getOutputStream().println(this.getManual());
    }

    /**
     * Prints version of this command.
     */
    protected void printVersion() {
        this.getOutputStream().println(this.getVersion());
    }

    /**
     * Returns version of command shutdown in format:
     *
     * <pre>
     *     shutdown (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of cat command.
     */
    public String getVersion() {
        return this.name() + " (PowerShell utils) " + Shutdown.VERSION;
    }

    /**
     * Returns string which represents usage of this command which has format:
     * 
     * <pre>
     *     shutdown: usage: shutdown [OPTION]...
     * </pre>
     *
     * @return String which represents usage of this command.
     */
    @Override
    public String toString() {
        return this.name() + ": usage: " +
                this.name() + " [OPTION]...";
    }
}
