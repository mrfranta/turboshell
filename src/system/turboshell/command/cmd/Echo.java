package system.turboshell.command.cmd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;

/**
 * Class {@code Echo} represents command {@code echo} which prints arguments.
 * 
 * @author pkopac (Petr Kopač)
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.5.0
 */
public class Echo extends AbstractShellCommand {
    /** Message to print. */
    private String msg;

    /** Information whether will be prints new lines. */
    private boolean printNewLine = true;
    /** Information whether will be interpreted escape sequences. */
    private boolean interpreteES = false;
    /** Information whether will be printed help. */
    private boolean printHelp = false;

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Echo(final ShellCore shellCore) {
        super(shellCore);

        this.msg = "";
    }

    /**
     * Prints the arguments.
     * 
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    // JDK1.5 compatible @Override
    public int execute() {
        this.parseArgs(this.getArguments());

        if (this.printHelp) {
            this.getOutputStream().println(this.getManual());

            return ShellCommand.EXIT_SUCCESS;
        }

        if (this.interpreteES) {
            this.msg = this.msg.replace("\\\\", "\\");
            this.msg = this.msg.replace("\\n", "\n");
            this.msg = this.msg.replace("\\b", "\b");
            this.msg = this.msg.replace("\\f", "\f");
            this.msg = this.msg.replace("\\r", "\r");
            this.msg = this.msg.replace("\\t", "\t");

            String substring = "";
            try {
                StringBuilder sb = new StringBuilder(this.msg.length());

                Pattern pattern = Pattern.compile("\\\\(0[0-8]{3})|\\\\([01][0-8]{2})");
                String[] parts = pattern.split(this.msg);
                Matcher matcher = pattern.matcher(this.msg);

                int i = 0;
                while (matcher.find()) {
                    int start = matcher.start();
                    int end = matcher.end();

                    substring = this.msg.substring(start + 1, end);
                    char c = (char) Integer.parseInt(substring, 8);

                    if (i < parts.length) {
                        sb.append(parts[i]);
                    }
                    sb.append(c);

                    i++;
                }

                if (i < parts.length) {
                    sb.append(parts[i]);
                }

                this.msg = sb.toString();
            } catch (NumberFormatException exc) {
                this.printError(substring + ": invalid format of octal ASCII character.");

                return ShellCommand.EXIT_FAILURE;
            }
        }

        if (this.printNewLine) {
            this.getOutputStream().println(this.msg);
        }
        else {
            this.getOutputStream().print(this.msg);
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Parses arguments which was given to command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints 
     * something.
     * @throws ShellCommandException whether occurs some error in arguments parsing.
     */
    private void parseArgs(final String[] arguments) {
        int startMsg = 0;
        boolean startedMsg = false;
        for (String arg : arguments) {
            if (arg.equals("--help")) {
                this.printHelp = true;
                return;
            }

            if (arg.equals("-")) {
                break;
            }

            if (arg.startsWith("-")) {
                char[] params = arg.substring(1).toCharArray();
                for (char ch : params) {
                    if (ch == 'e') {
                        this.interpreteES = true;
                    }
                    else if (ch == 'E') {
                        this.interpreteES = false;
                    }
                    else if (ch == 'n') {
                        this.printNewLine = false;
                    }
                    else {
                        startedMsg = true;
                        break; // interprete as msg
                    }
                }
            }
            else {
                break;
            }

            if (startedMsg) {
                break;
            }

            startMsg++;
        }

        for (int i = startMsg; i < arguments.length; i++) {
            this.msg += (this.msg.equals("")) ? "" : " ";
            this.msg += arguments[i];
        }
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [OPTION]... [STRING]...
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[OPTION]... [STRING]...";
    }
}
