package system.turboshell.command.cmd;

import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessPosition;
import system.turboshell.process.manager.ShellProcessManager;

/**
 * Class {@code Bg} represents {@code bg} which sends a job to background. 
 * If no ID specified, uses the last executed process.
 *
 * @author pkopac (Petr Kopač)
 * @version 1.0
 */
public class Bg extends AbstractShellCommand {
    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O
     * and working directory.
     */
    public Bg(final ShellCore shellCore) {
        super(shellCore);
    }

    /**
     * Uses argument parsing from {@code Fg}, which contains also checking of
     * errors and adds checking, whether the process is already on background or
     * not. Then standard {@code ShellCore} method is used to change the position
     * of the job.
     *
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    public int execute() {
        ShellProcess proc;
        try {
            proc = Fg.parseArgsAndFindProcess(this.getArguments(), this.core);
            if (proc.getPosition() == ShellProcessPosition.BACKGROUND) {
                throw new ShellCommandException(this,
                        "job already in background");
            }

            ShellProcessManager manager = VMM.getInstance().getProcessManager();
            if (!manager.getCaller(proc).equals(this.core.getShellProcess())) {
                throw new ShellCommandException(this,
                        "job doesn't belong to this shell");
            }
        } catch (Exception exc) {
            this.printError(exc);

            return ShellCommand.EXIT_FAILURE;
        }

        this.core.sendToBackground(proc);

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [PID...]
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[PID...]";
    }
}
