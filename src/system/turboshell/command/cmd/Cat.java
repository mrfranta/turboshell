/**
 * This Java file was created 12.11.2013 13:46:52.
 */
package system.turboshell.command.cmd;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.InvalidOptionException;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.parser.CommandStructure;

/**
 * Class {@code Cat} represents command {@code cat} which prints input onto
 * output.
 *
 * @author pkopac (Petr Kopač)
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 *
 * @since 0.4.11
 */
public class Cat extends AbstractShellCommand {
    /**
     * List of source files which will be read.
     */
    private List<File> sources;
    /**
     * Information whether will be written char '$' for EOF.
     */
    private boolean showEnds;
    /**
     * Information whether will be numbered lines of output.
     */
    private boolean number;
    /**
     * Information whether will be written char '^I' instead tab.
     */
    private boolean showTabs;
    /**
     * Information whether will be written ^-notations for non printing
     * characters.
     */
    private boolean showNonPrinting;
    /**
     * Information whether parameters contains error.
     */
    private boolean error;
    /**
     * String representing version of this command.
     */
    public static final String VERSION = "1.0";

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O
     * and working directory.
     */
    public Cat(final ShellCore shellCore) {
        super(shellCore);

        this.showEnds = false;
        this.number = false;
        this.showTabs = false;
        this.showNonPrinting = false;
        this.error = false;
    }

    /**
     * Parses user input, fetches streams and pushes data from the sources into
     * the output.
     *
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    // JDK1.5 compatible @Override
    public int execute() {
        String[] args = this.getArguments();
        PrintStream output = this.getOutputStream();
        InputStream input = this.getInputStream();

        try {
            String res = this.parseArgs(args);
            if (res != null) {
                output.println(res);

                return ShellCommand.EXIT_SUCCESS;
            }

            if (this.error && this.sources.isEmpty()) {
                return ShellCommand.EXIT_FAILURE;
            }

            this.processIO(input, output);
        } catch (Exception exc) {
            this.printError(exc);

            return ShellCommand.EXIT_FAILURE;
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Parses arguments, which were given to the command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints
     * something.
     * @throws ShellCommandException whether occurs some error in arguments
     * parsing.
     */
    private String parseArgs(final String[] arguments) throws ShellCommandException {
        this.sources = new ArrayList<File>();
        boolean stdinInQueue = false, ignoreParameters = false;
        String res = null;
        for (int i = 0; i < arguments.length; i++) {
            String arg = CommandStructure.removeEscapes(arguments[i]);
            if (arg.startsWith("-") && !arg.equals("-")) {
                // take the first occurance
                if (arg.equals("-") && !stdinInQueue) {
                    this.sources.add(null);
                    stdinInQueue = true;
                }
                else if (arg.equals("--")) {
                    ignoreParameters = true;
                } // long parameters
                else if (arg.startsWith("--") && !ignoreParameters) {
                    res = this.parseArgsInternal(arg.substring(2));
                    if (res != null) {
                        return res;
                    }
                }
                else if (!ignoreParameters) { // parameters
                    char[] charAr = arg.substring(1).toCharArray();

                    for (char c : charAr) {
                        switch (c) {
                            case 'A':
                                this.parseArgsInternal("show-all");
                                break;

                            case 'n':
                                this.parseArgsInternal("number");
                                break;

                            case 'v':
                                this.parseArgsInternal("show-nonprinting");
                                break;

                            case 't':
                                this.parseArgsInternal("show-nonprinting");
                            case 'T':
                                this.parseArgsInternal("show-tabs");
                                break;

                            case 'e':
                                this.parseArgsInternal("show-nonprinting");

                            case 'E':
                                this.parseArgsInternal("show-ends");
                                break;

                            default:
                                throw new InvalidOptionException(this, c);
                        }
                    }
                }
            }
            else {
                File source = this.core.getFile(arg);
                File output = this.getOutputFile();
                if (output != null && output.getAbsoluteFile().equals(
                        source.getAbsoluteFile())) {
                    if (!this.isOutputAppend()) {
                        this.printError(arg + ": input file is output file");

                        this.error = true;

                        continue;
                    }
                }

                if (source.isDirectory()) {
                    this.printError(arg + ": is a directory");
                    this.error = true;

                    continue;
                }

                if (!source.exists()) {
                    this.printError(arg + ": doesn't exist");
                    this.error = true;

                    continue;
                }

                this.sources.add(this.core.getFile(arg));
            }
        }

        return null;
    }

    /**
     * Long versions of arguments. Sets the internal feature flags.
     *
     * @param arg argument which will be parsed.
     * @return Information whether will be help or version printed.
     * @throws ShellCommandException if unknown parameter is given.
     */
    private String parseArgsInternal(final String arg) throws ShellCommandException {
        if (arg.equals("show-all")) {
            this.showNonPrinting = true;
            this.showEnds = true;
            this.showTabs = true;
            return null;
        }
        else if (arg.equals("show-ends")) {
            this.showEnds = true;
            return null;
        }
        else if (arg.equals("number")) {
            this.number = true;
            return null;
        }
        else if (arg.equals("show-tabs")) {
            this.showTabs = true;
            return null;
        }
        else if (arg.equals("show-nonprinting")) {
            this.showNonPrinting = true;
            return null;
        }
        else if (arg.equals("help")) {
            return this.getManual();
        }
        else if (arg.equals("version")) {
            return this.getVersion();
        }
        else {
            throw new InvalidOptionException(this, arg);
        }
    }

    /**
     * Concatenates all input into output.
     *
     * @param input input stream.
     * @param output output print stream.
     * @throws IOException if some I/O error occurs.
     */
    private void processIO(final InputStream input,
                           final PrintStream output) throws IOException {
        for (File s : this.sources) {
            if (s == null) {
                this.readNPrint(input, output);
            }
            else {
                FileInputStream is = new FileInputStream(s);
                this.readNPrint(is, output);
                is.close();
            }
        }
        if (this.sources.isEmpty()) {
            this.readNPrint(input, output);
        }
    }

    /**
     * Takes one chunk of data from input and transfers it into output, while
     * doing different types of filtering.
     *
     * @param input input stream.
     * @param output output print stream.
     * @throws IOException if some I/O error occurs.
     */
    private void readNPrint(final InputStream input, final PrintStream output) throws IOException {
        int b = -1;
        int tab = '\t';
        int lineNr = 1;
        boolean isNewLine = true;

        Reader reader = null;
        if (input != this.core.stdin) {
            reader = this.getReader(input);
        }

        while (true) {
            if (reader != null) {
                b = reader.read();
            }
            else {
                b = input.read();
            }

            if (b <= -1) {
                break;
            }

            if (this.number) {
                if (isNewLine) {
                    output.format("%6d  ", lineNr++);

                    isNewLine = false;
                }

                if (b == '\n') {
                    isNewLine = true;
                }
            }

            if (this.showTabs && tab == b) {
                output.print("^I");
            }
            else if (this.showEnds && b == '\n') {
                output.print('$');
                output.print((char) b);
            }
            else if (this.showNonPrinting && (b < 32 || b == 127)) {
                if (b == 127) {
                    output.print("^?");
                    continue;
                }

                if (b != '\n' && b != '\t') {
                    output.print('^');
                    output.print((char) (b + '@'));
                    continue;
                }

                output.print((char) b);
            }
            else {
                output.print((char) b);
            }
        }

        if (reader != null) {
            reader.close();
        }
    }

    /**
     * Returns version of command cat in format:
     *
     * <pre>
     *     cat (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of cat command.
     */
    public String getVersion() {
        return "cat (PowerShell utils) " + Cat.VERSION;
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [OPTION] [FILE]...
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[OPTION] [FILE]...";
    }
}
