/**
 * This Java file was created 12.11.2013 13:25:52.
 */
package system.turboshell.command.cmd;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.stdio.ConsolePrintStream;

/**
 * Class {@code Clear} represents command {@code clear} which cleans up console of 
 * calling shell. This command ignores all arguments.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.5
 * 
 * @since 0.2.1
 */
public class Clear extends AbstractShellCommand {
    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Clear(final ShellCore shellCore) {
        super(shellCore);
    }

    /**
     * Executes command {@code clear} which ignores all arguments. This command 
     * cleans up console of calling shell.
     * 
     * @return Return status of command execution which is always {@code 0} 
     * (successful execution).
     */
    // JDK1.5 compatible @Override
    public int execute() {
        ((ConsolePrintStream) this.core.stdout).clear();

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Returns shell command specific usage.
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "";
    }
}
