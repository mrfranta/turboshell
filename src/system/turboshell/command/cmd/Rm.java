/**
 * This Java file was created 25.11.2013 23:33:41.
 */
package system.turboshell.command.cmd;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.parser.CommandStructure;

/**
 * Class {@code Rm} represents command {@code rm} which remove files or 
 * directories.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.5.5
 */
public class Rm extends AbstractShellCommand {
    /** 
     * Information whether will be file removed with force (ignores non-existing 
     * files).
     */
    private boolean force;
    /** Information about user prompting.  */
    private Prompt prompt;
    /** Information whether will be files removed recursively. */
    private boolean recurse;
    /** 
     * Information whether will be printed informations about file removing.
     */
    private boolean verbose;

    /** List of files to remove. */
    private List<File> files;
    /** List of paths for files which was given as parameters. */
    private List<String> names;

    /** Version of this command. */
    public static final String VERSION = "1.0";

    /**
     * Enumerate {@code Prompt} represents frequency of user prompting 
     * for file removing.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.0
     * 
     * @since 0.5.5
     */
    public static enum Prompt {
        /** Never prompt. */
        NEVER("never"),
        /** Prompt only once. */
        ONCE("once"),
        /** Prompt always. */
        ALWAYS("always");

        /** String value of prompt for method {@link Prompt#fromValue(String)}. */
        private String value;

        /**
         * Constructs field of enumeration.
         *
         * @param value string value of prompt.
         */
        private Prompt(final String value) {
            this.value = value;
        }

        /**
         * Returns string value of prompt.
         *
         * @return String value of prompt.
         */
        public String getValue() {
            return this.value;
        }

        /**
         * Returns string value of prompt.
         *
         * @return String value of prompt.
         */
        @Override
        public String toString() {
            return this.value;
        }

        /**
         * Returns prompt which has specified string value.
         *
         * @param value string value of prompt.
         * @return Prompt with specified string value.
         * 
         * @throws IllegalArgumentException whether was entered non-existing 
         * value.
         */
        public static Prompt fromValue(final String value) {
            for (Prompt p : Prompt.values()) {
                if (p.getValue().equalsIgnoreCase(value)) {
                    return p;
                }
            }

            throw new IllegalArgumentException("Invalid prompt type");
        }
    }

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Rm(final ShellCore shellCore) {
        super(shellCore);

        this.prompt = Prompt.NEVER;
        this.files = new ArrayList<File>();
        this.names = new ArrayList<String>();
    }

    /**
     * Executes command. Checks whether the command has some arguments. If 
     * command has no arguments, it will write error. Then parses arguments and 
     * removes files which was given as arguments.
     *
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    // JDK1.5 compatible @Override
    public int execute() {
        String[] args = this.getArguments();
        PrintStream error = this.getErrorStream();

        if (args.length == 0) {
            error.println(this.name() + ": missing operand");
            error.println("Try '" + this.name() + " --help' for more information.");

            return ShellCommand.EXIT_FAILURE;
        }

        try {
            String res = this.parseArgs(args);
            if (res != null) {
                this.getOutputStream().println(res);

                return ShellCommand.EXIT_SUCCESS;
            }
        } catch (ShellCommandException exc) {
            this.getErrorStream().println(exc.getMessage());

            return ShellCommand.EXIT_FAILURE;
        }

        this.removeFiles();

        return ShellCommand.EXIT_SUCCESS;
    }

    /** 
     * Prints help for this command.
     */
    protected void printHelp() {
        this.getOutputStream().println(this.getManual());
    }

    /**
     * Prints version of this command.
     */
    protected void printVersion() {
        this.getOutputStream().println(this.getVersion());
    }

    /**
     * Parses arguments which was given to command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints 
     * something.
     */
    private String parseArgs(final String[] args) throws ShellCommandException {
        String res = null;
        for (int i = 0; i < args.length; i++) {
            String arg = CommandStructure.removeEscapes(args[i]);
            if (arg.startsWith("--")) {
                res = this.parseArgsInternal(arg.substring(2));
                if (res != null) {
                    return res;
                }
            }
            else if (arg.startsWith("-") && !arg.equals("-")) {
                char[] charAr = arg.substring(1).toCharArray();
                for (char c : charAr) {
                    switch (c) {
                        case 'f':
                            this.parseArgsInternal("force");
                            break;

                        case 'i':
                            if (!this.force) {
                                this.prompt = Prompt.ALWAYS;
                            }
                            break;

                        case 'I':
                            if (!this.force) {
                                this.prompt = Prompt.ONCE;
                            }
                            break;

                        case 'r':
                        case 'R':
                            this.parseArgsInternal("recursive");
                            break;

                        case 'p':
                            this.parseArgsInternal("parents");
                            break;

                        case 'v':
                            this.parseArgsInternal("verbose");
                            break;

                        default:
                            throw new ShellCommandException(this.name() +
                                    ": invalid option '" + c + "'\n" +
                                    "Try '" + this.name() +
                                    " --help' for more information.");
                    }
                }
            }
            else {
                this.files.add(this.core.getFile(arg));
                this.names.add(arg);
            }
        }

        return null;
    }

    private String parseArgsInternal(final String arg) throws ShellCommandException {
        final String interactive = "--interactive";

        if (arg.equals("force")) {
            this.force = true;
            this.prompt = Prompt.NEVER;
            return null;
        }
        else if (arg.startsWith(interactive) && !this.force) {
            if (arg.equals(interactive)) {
                this.prompt = Prompt.ALWAYS;
            }
            else {
                try {
                    String when = arg.substring(interactive.length() + 1);
                    this.prompt = Prompt.fromValue(when);
                } catch (RuntimeException exc) {
                    throw new ShellCommandException(this.name() +
                            ": invalid form of interactive parameter.\n" +
                            "Try '" + this.name() +
                            " --help' for more information.");
                }
            }

            return null;
        }
        else if (arg.equals("recursive")) {
            this.recurse = true;
            return null;
        }
        else if (arg.equals("verbose")) {
            this.verbose = true;
            return null;
        }
        else if (arg.equals("help")) {
            return this.getManual();
        }
        else if (arg.equals("version")) {
            return this.getVersion();
        }
        else {
            throw new ShellCommandException(this.name() +
                    ": invalid option '" + arg + "'\n" +
                    "Try '" + this.name() +
                    " --help' for more information.");
        }
    }

    /**
     * Removes files which was given as parameters.
     */
    private void removeFiles() {
        int i = -1;
        for (File file : this.files) {
            i++;
            String name = this.names.get(i);

            if (file.isDirectory() && !this.recurse) {
                this.getErrorStream().println(this.name() +
                        ": cannot remove \"" + name + "\": Is a directory");

                continue;
            }

            if (!file.exists()) {
                if (!this.force) {
                    this.getErrorStream().println(this.name() +
                            ": cannot remove \"" + name +
                            "\": No such file or directory");
                }

                continue;
            }

            boolean result = true;
            if (i == 0 && this.prompt == Prompt.ONCE) {
                if (this.recurse || this.files.size() > 0) {
                    if (!this.prompt(file, name)) {
                        return;
                    }
                }
            }

            if (this.prompt == Prompt.ALWAYS) {
                result = this.prompt(file, name);
            }

            if (!result) {
                continue;
            }

            if (this.recurse) {
                this.removeRecursively(file);
            }
            else {
                this.rm(file, name);
            }
        }
    }

    /**
     * Prompts user for removing file with specified name.
     *
     * @param file descriptor of file which will be removed.
     * @param name name of file which will be removed.
     * @return Information whether want user remove file with specified name.
     */
    private boolean prompt(final File file, final String name) {
        this.core.stdout.print(this.name() + ": remove " +
                (file.isDirectory() ? "directory" : "file") +
                "\"" + name + "\" (Y/N/A)? ");
        try {
            StringBuffer answer = new StringBuffer();
            for (int c = this.core.stdin.read(); c >= 0; c = this.core.stdin.read()) {
                if (c == '\n') {
                    break;
                }

                answer.append((char) c);
            }

            String answerStr = answer.toString();
            if (answerStr.equalsIgnoreCase("y") || answerStr.equalsIgnoreCase("yes")) {
                return true;
            }
            if (answerStr.equalsIgnoreCase("a") || answerStr.equalsIgnoreCase("all")) {
                this.prompt = Prompt.NEVER;
                return true;
            }
        } catch (IOException exc) {
            // impossible
        }

        return false;
    }

    /**
     * Removes recursively files in specified directory.
     *
     * @param dir directory which will be removed with all sub-directories 
     * and files.
     */
    private void removeRecursively(final File dir) {
        File[] files = dir.listFiles();
        if (files != null) { // some JVMs return null for empty dirs
            for (File file : files) {
                if (file.isDirectory()) {
                    this.removeRecursively(file);
                }
                else {
                    if (this.prompt == Prompt.ALWAYS) {
                        if (!this.prompt(file, file.getName())) {
                            continue;
                        }
                    }

                    this.rm(file, file.getName());
                }
            }
        }

        this.rm(dir, dir.getName());
    }

    /**
     * Removes specified file and if is activated verbose, it will prints 
     * information or error message.
     *
     * @param file file to remove.
     * @param name path for file which was given as parameter; or name of file.
     * @return Information whether was directory created.
     */
    private void rm(final File file, final String name) {
        try {
            if (file.isDirectory()) {
                File[] list = file.listFiles();
                if (list != null && list.length > 0) {
                    throw new IOException("Directory isn't empty");
                }
            }

            if (!file.exists() || this.force) {
                return;
            }

            // JDK1.7 Files.delete(file.toPath();
            if (!file.delete()) {
                this.getOutputStream().println(this.name() +
                        ": cannot remove file \"" + name + "\"");

                return;
            }
            // end
        } catch (IOException exc) {
            this.getOutputStream().println(this.name() +
                    ": cannot remove file \"" + name + "\": " +
                    exc.getMessage());

            return;
        }

        if (this.verbose) {
            this.getOutputStream().println(this.name() +
                    ": removed \"" + name + "\"");
        }
    }

    /**
     * Returns version of command rm in format:
     *
     * <pre>
     *     rm (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of cat command.
     */
    public String getVersion() {
        return this.name() + " (PowerShell utils) " + Rm.VERSION;
    }

    /**
     * Returns string which represents usage of this command which has format:
     * 
     * <pre>
     *     rm: usage: rm [OPTION]... FILE...
     * </pre>
     *
     * @return String which represents usage of this command.
     */
    @Override
    public String toString() {
        return this.name() + ": usage: " +
                this.name() + " [OPTION]... FILE...";
    }
}
