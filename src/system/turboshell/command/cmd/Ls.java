/**
 * This Java file was created 12.11.2013 14:01:45.
 */
package system.turboshell.command.cmd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.utils.StringUtilities;

/**
 * <b>Ls</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 *
 * @since 0.2.0
 */
public class Ls extends AbstractShellCommand {
    private File[] fileArgs;
    private boolean parents = false;
    private boolean hidden = false;
    private boolean details = false;
    private boolean comma = false;
    private boolean quote = false;
    private boolean si = false;
    private boolean humanReadable = false;
    private boolean error = false;
    private long maxFileSize = -1;

    /**
     * Stores information about one file.
     */
    private class FileInfo {

        private final String name;
        private String permissions;
        private long size;
        private Date lastModify;

        public FileInfo(final String name, final File file) {
            if (Ls.this.details) {
                this.permissions = "";
                this.permissions += file.isDirectory() ? "d" : "-";
                this.permissions += file.canRead() ? "r" : "-";
                this.permissions += file.canWrite() ? "w" : "-";
                // file.canExecute() is in JRE 1.5 missing
                this.permissions += file.isDirectory() ? "-" : "x";
                this.size = file.length();
                String strSize = "" + this.size;
                Ls.this.maxFileSize = Math.max(strSize.length(),
                        Ls.this.maxFileSize);
                this.lastModify = new Date(file.lastModified());
            }
            else {
                this.size = -1;
                this.lastModify = null;
                this.permissions = null;
            }

            if (Ls.this.quote) {
                this.name = "\"" + name + "\"";
            }
            else {
                this.name = name;
            }
        }

        @Override
        /**
         * Prints meaningful representation of object. Influenced by what Ls
         * feature flags are on.
         */
        public String toString() {
            String str = "";

            if (this.permissions != null) {
                str += this.permissions + "   ";
            }

            if (this.size > -1) {
                if (Ls.this.humanReadable) {
                    str += this.humanReadableByteCount(this.size, Ls.this.si);
                    str += "   ";
                }
                else {
                    str += String.format("%" + Ls.this.maxFileSize + "d",
                            this.size) + "   ";
                }
            }

            if (this.lastModify != null) {
                SimpleDateFormat format = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm");
                str += format.format(this.lastModify) + "   ";
            }

            return str + this.name;
        }

        /**
         * Converts number in bytes to pretty-printed kB,MB...EB.
         *
         * @param bytes
         * @param si
         * @return
         */
        private String humanReadableByteCount(final long bytes, final boolean si) {
            int unit = si ? 1000 : 1024;
            int width = si ? 1 : 2;

            if (bytes < unit) {
                return String.format(
                        "%" + Ls.this.maxFileSize + ".1f %sB",
                        (double) bytes, StringUtilities.repeat(" ", width));
            }
            int exp = (int) (Math.log(bytes) / Math.log(unit));
            String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");

            return String.format(
                    "%" + Ls.this.maxFileSize + ".1f %sB",
                    bytes / Math.pow(unit, exp), pre);
        }
    }

    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O
     * and working directory.
     */
    public Ls(final ShellCore shellCore) {
        super(shellCore);
    }

    // JDK1.5 compatible @Override
    /**
     * Parse arguments and print list of files.
     *
     * @return
     */
    public int execute() {
        PrintStream errorStream = this.getErrorStream();
        try {
            if (!this.parseArgs(this.getArguments())) {
                return ShellCommand.EXIT_SUCCESS;
            }
        } catch (Exception ex) {
            errorStream.println("ls: " + ex.getMessage());
            return 2;
        }

        if (this.fileArgs.length == 0 && this.error) {
            return ShellCommand.EXIT_FAILURE;
        }

        if (this.fileArgs.length == 0) {
            try {
                this.printFile(this.core.getWorkingDirectory());
            } catch (FileNotFoundException ex) {
                // don't care
            }
        }
        else if (this.fileArgs.length == 1) {
            try {
                this.printFile(this.fileArgs[0]);
            } catch (FileNotFoundException ex) {
                errorStream.println("ls: " + ex.getMessage()
                        + ": No such file or directory");
            }
        }
        else {
            for (File file : this.fileArgs) {
                try {
                    this.getOutputStream().println("" + file + ":");
                    this.printFile(file);
                } catch (FileNotFoundException e) {
                    errorStream.println("ls: " + e.getMessage()
                            + ": No such file or directory");
                }
            }
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Print information about file. If it's a folder, print its children.
     *
     * @param f
     * @throws FileNotFoundException
     */
    private void printFile(final File f) throws FileNotFoundException {
        if (!f.exists()) {
            throw new FileNotFoundException(f.toString());
        }

        File parent = f.getParentFile();
        if (f.isDirectory()) {
            File[] listFiles = f.listFiles();
            if (this.parents) {
                listFiles = this.doParents(listFiles, f, parent);
            }
            List<FileInfo> files = this.createFileList(listFiles, f, parent);
            if (files.isEmpty()) {
                return;
            }
            FileInfo last = files.get(files.size() - 1);
            for (FileInfo info : files) {
                this.printOneFile(info, last.equals(info));
            }
        }
        else {
            this.printOneFile(new FileInfo(f.getName(), f), true);
        }
    }

    /**
     * Standard parsing of arguments to switch feature flags.
     *
     * @param arguments
     * @return
     */
    private boolean parseArgs(final String[] arguments) throws Exception {
        ArrayList<File> list = new ArrayList<File>();
        boolean ignoreParams = false;
        for (String arg : arguments) {
            if (ignoreParams) {
                list.add(this.core.getFile(arg));
            }
            else if ("--".equals(arg)) {
                ignoreParams = true;
            }
            else if (arg.startsWith("-")) {
                if ("-a".equals(arg) || "--all".equals(arg)) {
                    this.hidden = true;
                    this.parents = true;
                }
                else if ("-A".equals(arg) || "--almost-all".equals(arg)) {
                    this.hidden = true;
                    this.parents = false;
                }
                else if ("--help".equals(arg)) {
                    this.printHelp();
                    return false;
                }
                else if ("-Q".equals(arg) || "--quote-name".equals(arg)) {
                    this.quote = true;
                }
                else if ("-l".equals(arg)) {
                    this.details = true;
                }
                else if ("-h".equals(arg) || "--human-readable".equals(arg)) {
                    this.humanReadable = true;
                }
                else if ("--si".equals(arg)) {
                    this.si = true;
                }
                else if ("-m".equals(arg)) {
                    this.comma = true;
                    this.details = false;
                }
                else {
                    throw new Exception("uknown parameter " + arg);
                }
            }
            else {
                if (arg.contains("\\")) {
                    this.getErrorStream().println(this.name() + ": " + arg
                            + ": invalid file separator");
                    this.error = true;

                    continue;
                }

                list.add(this.core.getFile(arg));
            }
        }
        if (this.comma) { // -m takes precedence
            this.details = false;
        }
        this.fileArgs = list.toArray(new File[list.size()]);

        return true;
    }

    private void printHelp() {
        this.getOutputStream().println(this.getManual());
    }

    /**
     * Creates list of files, filtered if {@code hidden}.
     *
     * @param listFiles
     * @param dir
     * @param parent
     * @return
     */
    private List<FileInfo> createFileList(final File[] listFiles,
                                          final File dir,
                                          final File parent) {
        List<FileInfo> files = new ArrayList<FileInfo>();
        if (listFiles == null) {
            return files;
        }

        for (File file : listFiles) {
            if (file == null) {
                continue;
            }

            String name = file.getName();
            if ((file.isHidden() || name.startsWith(".")) && !this.hidden) {
                continue;
            }

            FileInfo info;
            if (file == parent) {
                info = new FileInfo("..", file);
            }
            else if (file == dir) {
                info = new FileInfo(".", file);
            }
            else {
                name = file.getName();
                info = new FileInfo(name, file);
            }

            files.add(info);
        }
        return files;
    }

    private File[] doParents(final File[] listFiles,
                             final File dir,
                             final File parent) {
        int length = listFiles.length + 1;
        if (parent != null) {
            length++;
        }

        File[] newListFiles = new File[length];

        int pos = 0;
        newListFiles[pos++] = dir;

        if (parent != null) {
            newListFiles[pos++] = parent;
        }

        System.arraycopy(listFiles, 0, newListFiles, pos, listFiles.length);

        return newListFiles;
    }

    private void printOneFile(final FileInfo info, final boolean isLast) {
        if (this.comma && !isLast) {
            this.getOutputStream().print(info + ", ");
        }
        else {
            this.getOutputStream().println(info);
        }
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [OPTION]... [FILE]...
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[OPTION]... [FILE]...";
    }
}
