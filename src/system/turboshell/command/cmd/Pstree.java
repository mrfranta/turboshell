/**
 * This Java file was created 29.11.2013 8:17:58.
 */
package system.turboshell.command.cmd;

import java.util.Comparator;

import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.parser.CommandStructure;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessNameComparator;
import system.turboshell.process.ShellProcessPIDComparator;
import system.turboshell.process.manager.ShellProcessManager;
import system.turboshell.process.manager.ShellProcessManagerException;

/**
 * <b>Pstree</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.6.10
 */
public class Pstree extends AbstractShellCommand {

    /** Version of this command. */
    public static final String VERSION = "1.0";

    private boolean showAll;
    private boolean showPID;
    private boolean sortPID;
    private boolean status;
    private long pid;

    public Pstree(final ShellCore shellCore) {
        super(shellCore);

        this.showAll = false;
        this.showPID = false;
        this.sortPID = false;
        this.status = false;
        this.pid = -1;
    }

    /** 
     * <b>Method execute</b>.
     *
     * @return
     * @see system.turboshell.command.ShellCommand#execute()
     */
    public int execute() {
        String[] args = this.getArguments();
        if (args.length > 0) {
            try {
                String res = this.parseArgs(args);
                if (res != null) {
                    this.getOutputStream().println(res);

                    return ShellCommand.EXIT_SUCCESS;
                }
            } catch (ShellCommandException exc) {
                this.getErrorStream().println(exc.getMessage());

                return ShellCommand.EXIT_FAILURE;
            }
        }

        try {
            this.printTree();
        } catch (ShellProcessManagerException exc) {
            this.getErrorStream().println(this.name() + ": " +
                    exc.getMessage());
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /** 
     * Prints help for this command.
     */
    protected void printHelp() {
        this.getOutputStream().println(this.getManual());
    }

    /**
     * Prints version of this command.
     */
    protected void printVersion() {
        this.getOutputStream().println(this.getVersion());
    }

    private void printTree() throws ShellProcessManagerException {
        VMM vmm = VMM.getInstance();
        ShellProcessManager manager = vmm.getProcessManager();

        Comparator<ShellProcess> comparator = null;
        if (this.sortPID) {
            comparator = new ShellProcessPIDComparator();
        }
        else {
            comparator = new ShellProcessNameComparator();
        }

        if (this.pid == -1) {
            this.pid = vmm.getPID();
        }

        String tree = manager.getProcessTree(this.pid, this.showAll,
                this.showPID, this.status, comparator);

        this.getOutputStream().print(tree);

    }

    /**
     * Parses arguments which was given to command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints 
     * something.
     */
    private String parseArgs(final String[] args) throws ShellCommandException {
        String res = null;
        for (int i = 0; i < args.length; i++) {
            String arg = CommandStructure.removeEscapes(args[i]);
            if (arg.startsWith("--")) {
                res = this.parseArgsInternal(arg.substring(2));
                if (res != null) {
                    return res;
                }
            }
            else if (arg.startsWith("-")) {
                char[] charAr = arg.substring(1).toCharArray();
                if (charAr.length == 0) {
                    throw new ShellCommandException(this.name() +
                            ": invalid option ''\n" +
                            "Try '" + this.name() +
                            " --help' for more information.");
                }

                for (char c : charAr) {
                    switch (c) {
                        case 'a':
                            this.parseArgsInternal("show-all");
                            break;

                        case 'p':
                            this.parseArgsInternal("show-pid");
                            break;

                        case 'n':
                            this.parseArgsInternal("sort-pid");
                            break;

                        case 's':
                            this.parseArgsInternal("show-status");
                            break;

                        default:
                            throw new ShellCommandException(this.name() +
                                    ": invalid option '" + c + "'\n" +
                                    "Try '" + this.name() +
                                    " --help' for more information.");
                    }
                }
            }
            else {
                if (this.pid == -1) {
                    try {
                        this.pid = Long.parseLong(arg);
                    } catch (NumberFormatException exc) {
                        throw new ShellCommandException(this.name() +
                                ": invalid format of process PID.");
                    }
                }
                else {
                    throw new ShellCommandException(this.name() +
                            ": invalid option '" + arg + "'\n" +
                            "Try '" + this.name() +
                            " --help' for more information.");
                }
            }
        }

        return null;
    }

    private String parseArgsInternal(final String arg) throws ShellCommandException {
        if (arg.equals("show-all")) {
            this.status = true;
            this.showAll = true;
        }
        else if (arg.equals("show-pid")) {
            this.showPID = true;
        }
        else if (arg.equals("sort-pid")) {
            this.sortPID = true;
        }
        else if (arg.equals("show-status")) {
            this.status = true;
        }
        else if (arg.equals("help")) {
            return this.getManual();
        }
        else if (arg.equals("version")) {
            return this.getVersion();
        }
        else {
            throw new ShellCommandException(this.name() +
                    ": invalid option '" + arg + "'\n" +
                    "Try '" + this.name() +
                    " --help' for more information.");
        }

        return null;
    }

    /**
     * Returns version of command pstree in format:
     *
     * <pre>
     *     pstree (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of cat command.
     */
    public String getVersion() {
        return this.name() + " (PowerShell utils) " + Pstree.VERSION;
    }

    /**
     * Returns string which represents usage of this command which has format:
     * 
     * <pre>
     *     pstree: usage: pstree [OPTION]... [PID]
     * </pre>
     *
     * @return String which represents usage of this command.
     */
    @Override
    public String toString() {
        return this.name() + ": usage: " +
                this.name() + " [OPTION]... [PID]";
    }
}
