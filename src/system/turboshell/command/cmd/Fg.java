package system.turboshell.command.cmd;

import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessPosition;
import system.turboshell.process.ShellProcessStatus;
import system.turboshell.process.manager.ShellProcessManager;

/**
 * Sends a process to the foreground.
 *
 * @author pkopac
 */
public class Fg extends AbstractShellCommand {

    /**
     * Simple constructor from superclass.
     *
     * @param shellCore
     */
    public Fg(final ShellCore shellCore) {
        super(shellCore);
    }

    /**
     * Checks user input for errors, then sends the job into foreground.
     *
     * @return retcode, 0 if successful
     */
    public int execute() {
        ShellProcess proc;
        try {
            proc = Fg.parseArgsAndFindProcess(this.getArguments(), this.core);

            if (proc.getPosition().equals(ShellProcessPosition.FOREGROUND)) {
                throw new Exception("job already in foreground");
            }

        } catch (Exception exc) {
            this.printError(exc);

            return ShellCommand.EXIT_FAILURE;
        }

        this.core.sendToForeground(proc);

        return ShellCommand.EXIT_SUCCESS;

    }

    /**
     * Reusable code used for user input checking in the jobs scope. If all
     * checks successful, returns a process wanted by the user.
     *
     * @param argsAr arguments from the command line
     * @param core
     * @return the wanted process
     * @throws Exception if user input is somehow invalid
     */
    public static ShellProcess parseArgsAndFindProcess(final String[] argsAr,
                                                       final ShellCore core) throws Exception {
        int targetPid = -1;
        int targetJob = -1;
        if (argsAr.length != 0) {
            try {
                if (argsAr[0].startsWith("%")) {
                    targetJob = Integer.parseInt(argsAr[0].substring(1));
                }
                else {
                    targetPid = Integer.parseInt(argsAr[0]);
                }
            } catch (NumberFormatException e) {
                throw new ShellCommandException(argsAr[0] + ": No such job");
            }
        }

        ShellProcess process;
        if (targetJob != -1) {
            process = core.getJob(targetJob);
            if (process == null) {
                throw new ShellCommandException("%" + targetJob +
                        ": No such job");
            }
        }
        else if (targetPid != -1) {
            process = core.getProcess(targetPid);
            if (process == null) {
                throw new ShellCommandException(targetPid + ": No such job");
            }
        }
        else {
            process = core.getJob();
            if (process == null) {
                throw new ShellCommandException("current: No such job");
            }
        }

        if (process.getStatus().equals(ShellProcessStatus.FINISHED)) {
            throw new ShellCommandException("job has terminated");
        }

        ShellProcessManager manager = VMM.getInstance().getProcessManager();
        if (!manager.getCaller(process).equals(core.getShellProcess())) {
            throw new ShellCommandException("job doesn't belong to this shell");
        }

        return process;
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [PID...]
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[PID...]";
    }
}
