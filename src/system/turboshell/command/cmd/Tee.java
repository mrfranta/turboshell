/**
 * This Java file was created 29.11.2013 3:13:03.
 */
package system.turboshell.command.cmd;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.InvalidOptionException;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.parser.CommandStructure;

/**
 * <b>Tee</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 */
public class Tee extends AbstractShellCommand {
    /** List of source files which will be read. */
    private List<PrintStream> outputs;
    /** Buffer for copying data from input again into standard output. */
    private StringBuffer buffer;
    /** Number of copies of input. */
    private int copies;
    /** Information whether will be output appended. */
    private boolean append;

    /** Version of this command. */
    public static final String VERSION = "1.0";

    public Tee(final ShellCore shellCore) {
        super(shellCore);

        this.outputs = new ArrayList<PrintStream>();
        this.append = false;
        this.copies = 0;
    }

    /** 
     * <b>Method execute</b>.
     *
     * @return
     * @see system.turboshell.command.ShellCommand#execute()
     */
    public int execute() {
        String[] args = this.getArguments();
        if (args.length > 0) {
            try {
                String res = this.parseArgs(args);
                if (res != null) {
                    this.getOutputStream().println(res);

                    return ShellCommand.EXIT_SUCCESS;
                }
            } catch (ShellCommandException exc) {
                this.getErrorStream().println(exc.getMessage());

                return ShellCommand.EXIT_FAILURE;
            }
        }

        try {
            this.processIO(this.getInputStream(), this.getOutputStream());
        } catch (IOException exc) {
            this.printError(exc);

            return ShellCommand.EXIT_FAILURE;
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    @Override
    public void done() {
        super.done();

        for (PrintStream stream : this.outputs) { // clean
            stream.flush();
            stream.close();
        }

        this.outputs.clear();
    }

    /** 
     * Prints help for this command.
     */
    protected void printHelp() {
        this.getOutputStream().println(this.getManual());
    }

    /**
     * Prints version of this command.
     */
    protected void printVersion() {
        this.getOutputStream().println(this.getVersion());
    }

    /**
     * Parses arguments which was given to command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints 
     * something.
     */
    private String parseArgs(final String[] args) throws ShellCommandException {
        List<String> outputs = new ArrayList<String>();
        String res = null;
        for (int i = 0; i < args.length; i++) {
            String arg = CommandStructure.removeEscapes(args[i]);
            if (arg.startsWith("--")) {
                res = this.parseArgsInternal(arg.substring(2));
                if (res != null) {
                    return res;
                }
            }
            else if (arg.equals("-")) {
                if (this.copies == 0) {
                    this.buffer = new StringBuffer();
                }

                this.copies++;

            }
            else if (arg.startsWith("-")) {
                char[] charAr = arg.substring(1).toCharArray();
                for (char c : charAr) {
                    switch (c) {
                        case 'a':
                            this.parseArgsInternal("append");
                            break;

                        default:
                            throw new InvalidOptionException(this, c);
                    }
                }
            }
            else {
                outputs.add(arg);
            }
        }

        for (String output : outputs) {
            try {
                File outputFile = this.core.getFile(output);
                outputFile.createNewFile();
                PrintStream stream = new PrintStream(new BufferedOutputStream(
                        new FileOutputStream(outputFile, this.append)), false,
                        "UTF-8");
                this.outputs.add(stream);
            } catch (IOException exc) {
                throw new ShellCommandException(this,
                        output + ": cannot open " +
                                "stream for file: " + exc.getMessage(), exc);
            }
        }

        return null;
    }

    private String parseArgsInternal(final String arg) throws ShellCommandException {
        if (arg.equals("append")) {
            this.append = true;
            return null;
        }
        else if (arg.equals("help")) {
            return this.getManual();
        }
        else if (arg.equals("version")) {
            return this.getVersion();
        }
        else {
            throw new InvalidOptionException(this, arg);
        }
    }

    private void processIO(final InputStream input,
                           final PrintStream output) throws IOException {
        int b = -1;

        StringBuffer lineBuffer = new StringBuffer();
        boolean stdin = (input == this.core.stdin);
        while ((b = input.read()) != -1) {
            output.write(b);

            if (stdin) {
                lineBuffer.append((char) b);

                if (b == '\n') {
                    for (int i = 0; i < this.copies; i++) {
                        output.print(lineBuffer);
                    }
                    lineBuffer.setLength(0);
                }
            }

            for (PrintStream fileOutput : this.outputs) {
                fileOutput.write(b);
            }

            if (this.copies > 0 && !stdin) {
                this.buffer.append((char) b);
            }
        }

        if (!stdin) {
            for (int i = 0; i < this.copies; i++) {
                output.print(this.buffer.toString());
            }

            if (this.copies > 0) {
                this.buffer.setLength(0);
            }
        }
    }

    /**
     * Returns version of command tee in format:
     *
     * <pre>
     *     tee (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of cat command.
     */
    public String getVersion() {
        return this.name() + " (PowerShell utils) " + Tee.VERSION;
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [OPTION] [FILE]...
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[OPTION].. [FILE]..";
    }
}
