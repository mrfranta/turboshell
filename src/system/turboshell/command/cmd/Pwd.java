/**
 * This Java file was created 12.11.2013 13:44:05.
 */
package system.turboshell.command.cmd;

import java.io.File;
import java.io.PrintStream;

import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;

/**
 * <b>Pwd</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.5
 * 
 * @since 0.2.0
 */
public class Pwd extends AbstractShellCommand {
    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Pwd(final ShellCore shellCore) {
        super(shellCore);
    }

    // JDK1.5 compatible @Override
    public int execute() {
        String[] args = this.getArguments();
        PrintStream output = this.getOutputStream();

        for (String arg : args) {
            if (arg.equals("--help")) {
                output.println(this.getManual());

                return ShellCommand.EXIT_SUCCESS;
            }
            else {
                PrintStream error = this.getErrorStream();
                error.println(this.name() + ": " + arg + ": invalid option");
                error.println(this.toString());

                return ShellCommand.EXIT_FAILURE;
            }
        }

        File workingDirectory = this.core.getWorkingDirectory();

        String path = workingDirectory.getPath();
        path = path.replaceAll("[/\\\\]", VMM.getProperty("file.separator"));

        output.println(path);

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Returns string which represents usage of this command which has format:
     * 
     * <pre>
     *     pwd: usage: pwd [OPTIONS]
     * </pre>
     *
     * @return String which represents usage of this command.
     */
    @Override
    public String toString() {
        return this.name() + ": usage: pwd [OPTIONS]";
    }
}
