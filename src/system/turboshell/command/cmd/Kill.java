/**
 * This Java file was created 4.12.2013 1:50:37.
 */
package system.turboshell.command.cmd;

import java.util.ArrayList;
import java.util.List;

import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.TooFewArgumentsException;
import system.turboshell.command.parser.CommandStructure;
import system.turboshell.process.manager.ShellProcessManager;

/**
 * Class {@code Kill} represents command {@code kill} which kills processes 
 * with entered PID's.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.7.6
 */
public class Kill extends AbstractShellCommand {
    private boolean printHelp;
    private List<Long> pids;

    /**
     * <b>Constructor</b>.
     *
     * @param shellCore
     */
    public Kill(final ShellCore shellCore) {
        super(shellCore);

        this.printHelp = false;
        this.pids = new ArrayList<Long>();
    }

    /** 
     * Kills processes with entered PID's.
     *
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    public int execute() {
        try {
            this.parseArgs(this.getArguments());
            if (this.printHelp) {
                this.getOutputStream().println(this.getManual());

                return ShellCommand.EXIT_SUCCESS;
            }

            VMM vmm = VMM.getInstance();
            ShellProcessManager manager = vmm.getProcessManager();
            for (Long pid : this.pids) {
                manager.kill(pid);
            }

            this.pids.clear();
        } catch (Exception exc) {
            this.printError(exc);

            return ShellCommand.EXIT_FAILURE;
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Parses arguments, which were given to the command.
     *
     * @param arguments arguments to parse.
     * @return Information whether will command continue in running or prints
     * something.
     * @throws ShellCommandException whether occurs some error in arguments
     * parsing.
     */
    private void parseArgs(final String[] arguments) throws Exception {
        if (arguments.length == 0) {
            throw new TooFewArgumentsException(this);
        }
        if (arguments[0].equals("--help")) {
            this.printHelp = true;
            return;
        }

        for (String arg : arguments) {
            try {
                this.pids.add(Long.parseLong(CommandStructure.removeEscapes(arg)));
            } catch (NumberFormatException exc) {
                throw new ShellCommandException(this, "could not parse pid: '" +
                        arguments[0] + "'", exc);
            }
        }

    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     PID...
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "PID...";
    }
}
