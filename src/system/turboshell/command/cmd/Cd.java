/**
 * This Java file was created 12.11.2013 13:46:52.
 */
package system.turboshell.command.cmd;

import java.io.PrintStream;

import system.turboshell.ShellCore;
import system.turboshell.ShellCoreException;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.parser.CommandStructure;

/**
 * Class {@code Cd} represents command {@code cd} which accepts folder name as 
 * the first argument. Any other arguments are ignored. If no arguments 
 * supplied, goes to user home, the same happens for tilda sign.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @author pkopac (Petr Kopač)
 * @version 1.4
 * 
 * @since 0.3.1
 */
public class Cd extends AbstractShellCommand {
    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Cd(final ShellCore shellCore) {
        super(shellCore);
    }

    /**
     * Executes command {@code cd} which accepts only first parameter. This 
     * parameter can be {@code --help} for help or {@code [dir]} which 
     * represents new working directory for calling shell. If no arguments 
     * supplied, goes to user home, the same happens for tilda sign.
     * 
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    // JDK1.5 compatible @Override
    public int execute() {
        String[] args = this.getArguments();
        PrintStream output = this.getOutputStream();
        PrintStream error = this.getErrorStream();

        try {
            if (args.length == 0) {
                this.core.setUserHomeDirectory();
            }
            else {
                String arg = CommandStructure.removeEscapes(args[0]);
                if (arg.startsWith("-")) {
                    if (arg.equals("--help")) {
                        output.println(this.getManual());

                        return ShellCommand.EXIT_SUCCESS;
                    }
                    else {
                        this.printError(arg + ": invalid option");
                        error.println(this.toString());

                        return ShellCommand.EXIT_FAILURE;
                    }
                }

                this.core.setWorkingDirectory(arg);
            }

            for (int i = 1; i < args.length; i++) {
                output.println(this.name() + ": parameter " +
                        CommandStructure.removeEscapes(args[i]) + " was ignored");
            }
        } catch (ShellCoreException exc) {
            error.println("shell: " + this.name() + ": " + exc.getMessage());

            return ShellCommand.EXIT_FAILURE;

        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     [OPTIONS] [dir]
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "[OPTIONS] [dir]";
    }
}
