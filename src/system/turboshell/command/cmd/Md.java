/**
 * This Java file was created 25.11.2013 22:14:35.
 */
package system.turboshell.command.cmd;

import system.turboshell.ShellCore;

/**
 * Class {@code Md} represents command {@code md} which is alias for 
 * {@code mkdir}. This command creates the directory(ies), if they do 
 * not already exist.
 * 
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.5.4
 */
public class Md extends Mkdir {
    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Md(final ShellCore shellCore) {
        super(shellCore);
    }

    /**
     * Returns version of command md in format:
     *
     * <pre>
     *     md (PowerShell utils) [version]
     * </pre>
     *
     * @return String representing version of cat command.
     */
    @Override
    public String getVersion() {
        return super.getVersion();
    }

    /**
     * Returns string which represents usage of this command which has format:
     * 
     * <pre>
     *     md: usage: md [OPTION]... DIRECTORY...
     * </pre>
     *
     * @return String which represents usage of this command.
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
