/**
 * This Java file was created 20.11.2013 1:58:54.
 */
package system.turboshell.command.cmd;

import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.manager.ShellProcessManager;
import system.turboshell.process.manager.ShellProcessManagerException;

/**
 * Class {@code Exit} represents command {@code exit} which closes calling 
 * shell. If there is no opened shell, the VMM is terminated.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 * 
 * @since 0.5.0
 */
public class Exit extends AbstractShellCommand {
    /**
     * Constructs command for specified shell core.
     *
     * @param shellCore shell core for command, which will provide standard I/O 
     * and working directory.
     */
    public Exit(final ShellCore shellCore) {
        super(shellCore);
    }

    /**
     * Closes calling shell. If there is no opened shell, the VMM is terminated.
     *
     * @return Always 0.
     */
    // JDK1.5 compatible @Override
    public int execute() {
        VMM vmm = VMM.getInstance();
        try {
            ShellProcessManager manager = vmm.getProcessManager();
            ShellProcess caller = manager.getCaller(this.getProcess());
            caller.kill();
        } catch (ShellProcessManagerException exc) {
            // impossible
        }

        return ShellCommand.EXIT_SUCCESS;
    }
}
