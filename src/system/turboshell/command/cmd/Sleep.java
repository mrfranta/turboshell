package system.turboshell.command.cmd;

import system.turboshell.ShellCore;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;

/**
 * Class {@code Sleep} represents command {@code sleep}. It waits a given amount
 * of time.
 *
 * @author pkopac (Petr Kopač)
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 *
 * @since 0.5.0
 */
public class Sleep extends AbstractShellCommand {

    private boolean suffix = false;
    private long amountOfTime = 0;
    private boolean printHelp = false;
    /**
     * Second in milliseconds.
     */
    public static final int SECOND_IN_MILLIS = 1000;
    /**
     * Minute in milliseconds.
     */
    public static final int MINUTE_IN_MILLIS = Sleep.SECOND_IN_MILLIS * 60;
    /**
     * Hour in milliseconds.
     */
    public static final int HOUR_IN_MILLIS = Sleep.MINUTE_IN_MILLIS * 60;
    /**
     * Day in milliseconds.
     */
    public static final int DAY_IN_MILLIS = Sleep.HOUR_IN_MILLIS * 24;

    public Sleep(final ShellCore shellCore) {
        super(shellCore);
    }

    // JDK1.5 compatible @Override
    /**
     * Really simple. Doesn't take user input, just one argument denoting how
     * much time to wait.
     *
     * @return
     */
    public int execute() {
        try {
            this.parseArgs(this.getArguments());
            if (this.printHelp) {
                this.getOutputStream().println(this.getManual());
                return ShellCommand.EXIT_SUCCESS;
            }
            synchronized (this) {
                try {
                    this.wait(this.amountOfTime);
                } catch (InterruptedException eee) {
                    return ShellCommand.EXIT_SUCCESS;
                    // probably user action
                }
            }
        } catch (Exception exc) {
            this.printError(exc);

            return ShellCommand.EXIT_FAILURE;
        }

        return ShellCommand.EXIT_SUCCESS;
    }

    /**
     * Parse the time to wait.
     *
     * @param arguments
     * @throws Exception
     */
    private void parseArgs(final String[] arguments) throws Exception {
        if (arguments.length == 0) {
            throw new ShellCommandException("too few arguments\n"
                    + "Try 'sleep -- help' for more information.");
        }
        if (arguments[0].equals("--help")) {
            this.printHelp = true;
            return;
        }

        int multiplier = Sleep.SECOND_IN_MILLIS;
        this.suffix = true;
        if (arguments[0].endsWith("s")) {
            // implicit
        }
        else if (arguments[0].endsWith("m")) {
            multiplier = Sleep.MINUTE_IN_MILLIS;
        }
        else if (arguments[0].endsWith("h")) {
            multiplier = Sleep.HOUR_IN_MILLIS;
        }
        else if (arguments[0].endsWith("d")) {
            multiplier = Sleep.DAY_IN_MILLIS;
        }
        else if (arguments[0].matches("[0-9]*$")) {
            // that's also fine, it's seconds
            this.suffix = false;
        }
        else {
            // fuck you, can't you insert time??
            throw new ShellCommandException("could not parse time: '"
                    + arguments[0] + "'");
        }

        try {
            if (!this.suffix) {
                this.amountOfTime = Integer.parseInt(arguments[0]);
            }
            else {
                this.amountOfTime = Integer.parseInt(arguments[0].substring(0,
                        arguments[0].length() - 1));
            }
        } catch (NumberFormatException exc1) {
            try {
                this.amountOfTime = Integer.parseInt(arguments[0]);
            } catch (NumberFormatException exc2) {
                throw new ShellCommandException("could not parse time: '"
                        + arguments[0] + "'");
            }
        }

        this.amountOfTime *= multiplier;
    }

    /**
     * Returns shell command specific usage which has format:
     * 
     * <pre>
     *     NUMBER[SUFFIX]... or OPTION
     * </pre>
     *
     * @return Shell command specific usage.
     */
    @Override
    protected String getUsageString() {
        return "NUMBER[SUFFIX]... or OPTION";
    }
}
