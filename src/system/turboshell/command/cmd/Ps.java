/**
 * This Java file was created 4.12.2013 2:53:40.
 */
package system.turboshell.command.cmd;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import system.turboshell.ShellCore;
import system.turboshell.VMM;
import system.turboshell.command.AbstractShellCommand;
import system.turboshell.command.ShellCommand;
import system.turboshell.command.ShellCommandException;
import system.turboshell.command.parser.CommandStructure;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessStatus;
import system.turboshell.process.manager.ShellProcessManager;
import system.turboshell.process.manager.ShellProcessTreeException;

/**
 * <b>Ps</b>.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @author pkopac
 * @version 1.0
 */
public class Ps extends AbstractShellCommand {

	/**
	 * Version of this command.
	 */
	public static final String VERSION = "1.0";
	private boolean sortName;

	public Ps(final ShellCore shellCore) {
		super(shellCore);
	}

	/**
	 * <b>Method execute</b>.
	 *
	 * @return
	 * @see system.turboshell.command.ShellCommand#execute()
	 */
	public int execute() {
		String[] args = this.getArguments();
		if (args.length > 0) {
			try {
				String res = this.parseArgs(args);
				if (res != null) {
					this.getOutputStream().println(res);

					return ShellCommand.EXIT_SUCCESS;
				}
			} catch (ShellCommandException exc) {
				this.getErrorStream().println(exc.getMessage());

				return ShellCommand.EXIT_FAILURE;
			}
		}

		try {
			this.printProcesses();
		} catch (ShellProcessTreeException exc) {
			this.getErrorStream().println(this.name() + ": "
					  + exc.getMessage());
		}

		return ShellCommand.EXIT_SUCCESS;
	}

	/**
	 * Prints version of this command.
	 */
	protected void printVersion() {
		this.getOutputStream().println(this.getVersion());
	}

	private void printProcesses() throws ShellProcessTreeException {
		VMM vmm = VMM.getInstance();
		ShellProcessManager manager = vmm.getProcessManager();
		ShellProcess[] processes = manager.getProcesses();
		List<ShellProcess> l = new ArrayList<ShellProcess>();

		Collections.addAll(l, processes);
		Collections.sort(l, new InternalProcessComparator());
		printHeader();
		for (Iterator<ShellProcess> it = l.iterator(); it.hasNext();) {
			ShellProcess p = it.next();
			if (p.getStatus().equals(ShellProcessStatus.FINISHED)) {
				continue;
			}
			printProcess(p);
		}
	}

	/**
	 * Parses arguments which was given to command.
	 *
	 * @param arguments arguments to parse.
	 * @return Information whether will command continue in running or prints
	 * something.
	 */
	private String parseArgs(final String[] args) throws ShellCommandException {
		String res;
		for (int i = 0; i < args.length; i++) {
			String arg = CommandStructure.removeEscapes(args[i]);
			if (arg.startsWith("--")) {
				res = this.parseArgsInternal(arg.substring(2));
				if (res != null) {
					return res;
				}
			} else if (arg.startsWith("-")) {
				char[] charAr = arg.substring(1).toCharArray();
				if (charAr.length == 0) {
					throw new ShellCommandException(this.name()
							  + ": invalid option ''\n"
							  + "Try '" + this.name()
							  + " --help' for more information.");
				}

				for (char c : charAr) {
					switch (c) {
						case 'p':
							//default, do nothing
							break;
						case 'n':
							sortName = true;
							break;
						default:
							throw new ShellCommandException(this.name()
									  + ": invalid option '" + c + "'\n"
									  + "Try '" + this.name()
									  + " --help' for more information.");
					}
				}
			} else {
			}
		}

		return null;
	}

	private String parseArgsInternal(final String arg) throws ShellCommandException {
		if (arg.equals("help")) {
			return this.getManual();
		} else if (arg.equals("sort-name")) {
			sortName = true;
		} else if (arg.equals("sort-pid")) {
			//do nothing, default :)
		} else {
			throw new ShellCommandException(this.name()
					  + ": invalid option '" + arg + "'\n"
					  + "Try '" + this.name()
					  + " --help' for more information.");
		}
		return null;
	}

	/**
	 * Returns version of command ps in format:
	 *
	 * <pre>
	 *     ps (PowerShell utils) [version]
	 * </pre>
	 *
	 * @return String representing version of cat command.
	 */
	public String getVersion() {
		return this.name() + " (PowerShell utils) " + Ps.VERSION;
	}

	/**
	 * Returns string which represents usage of this command which has format:
	 *
	 * <pre>
	 *     ps: usage: ps [OPTION]... [PID]
	 * </pre>
	 *
	 * @return String which represents usage of this command.
	 */
	@Override
	public String toString() {
		return this.name() + ": usage: "
				  + this.name() + " [OPTION]... [PID]";
	}

	private void printHeader() {
		getOutputStream().append("     PID     CMD NAME\n");
	}

	private void printProcess(ShellProcess next) {
		PrintStream os = getOutputStream();
		os.format("%8d     %s", next.getPID(), next.name());
		os.append("\n");
	}

	private class InternalProcessComparator implements Comparator<ShellProcess> {

		public InternalProcessComparator() {
		}

		public int compare(ShellProcess o1, ShellProcess o2) {
			if (Ps.this.sortName) {
				return (int) (o2.getPID() - o1.getPID());
			} else {
				return o2.name().compareToIgnoreCase(o1.name());
			}
		}
	}
}
