/**
 * This Java file was created 11.11.2013 22:23:19.
 */
package system.turboshell.command;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import system.turboshell.ShellCore;
import system.turboshell.command.parser.CommandOutput;
import system.turboshell.command.parser.CommandStructure;
import system.turboshell.pipe.PipeInputStream;
import system.turboshell.pipe.PipeOutputStream;
import system.turboshell.process.AbstractShellProcess;
import system.turboshell.process.ShellProcess;
import system.turboshell.process.ShellProcessListener;
import system.turboshell.process.ShellProcessPosition;
import system.turboshell.process.ShellProcessStatus;
import system.utils.FileUtilities;
import system.utils.ResourcesLoader;

/**
 * Class {@code AbstractShellCommand} implementation of {@code ShellCommand}
 * which represent single command in this shell system. Each command has name
 * and it's called with some arguments (parameters). Each shell command uses
 * input stream for reading data from standard input or input file. Also uses
 * print streams for writing data or error messages into standard output/error
 * or output file. All shell commands in this system should extends this class
 * because there is right implementation of most of shell command methods.
 *
 * <p>Shell commands can be piped between them using piped streams.</p>
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @author pkopac (Petr Kopač)
 * @version 1.35
 *
 * @since 0.4.0
 */
public abstract class AbstractShellCommand implements ShellCommand {

    /**
     * Shell core which creates this shell command.
     */
    protected final ShellCore core;
    /**
     * Name of this shell command.
     */
    private final String name;
    /**
     * Manual for this shell command.
     */
    private final String manual;
    /**
     * Arguments for this shell command.
     */
    private String[] args;
    /**
     * Input stream for reading data.
     */
    private InputStream input;
    /**
     * Reader for reading from input stream.
     */
    private Reader reader;
    /**
     * Output stream for writing data.
     */
    private PrintStream output;
    /**
     * Output stream for writing error messages.
     */
    private PrintStream error;
    /**
     * File from which shell command reads.
     */
    private File inputFile;
    /**
     * Information whether will be data appended into output file.
     */
    private boolean append;
    /**
     * File into which shell process writes.
     */
    private File outputFile;
    /**
     * Piped shell command to this command.
     */
    private ShellCommand pipe;
    /**
     * Return status of command execution.
     */
    private int exitStatus;
    /**
     * Shell process which performs this shell command.
     */
    private final ShellProcess process;
    /**
     * Registered listeners of this shell command.
     */
    private List<ShellCommandListener> listeners;
    /**
     * Character set for input and output streams.
     */
    public static final Charset STREAMS_CHARSET = Charset.forName("UTF-8");

    /**
     * Class {@code CommandProcess} represents shell process which performs this
     * shell command.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.35
     *
     * @since 0.4.0
     */
    private class CommandShellProcess extends AbstractShellProcess {

        /**
         * Returns name of this shell process.
         *
         * @return Name of this shell process.
         */
        // JDK1.5 compatible @Override
        public String name() {
            return AbstractShellCommand.this.name;
        }

        /**
         * Invokes method {@link ShellProcessListener#processStarted(ShellProcess)
         * processStarted} in all registered listeners in the case of this process
         * is started its job.
         */
        @Override
        protected void fireProcessStarted() {
            super.fireProcessStarted();

            AbstractShellCommand.this.fireCommandExecuted(AbstractShellCommand.this);
        }

        /**
         * Invokes method {@link ShellProcessListener#processKilled(ShellProcess)
         * processKilled} in all registered listeners in the case of this process
         * was killed.
         */
        @Override
        protected void fireProcessKilled() {
            super.fireProcessKilled();

            AbstractShellCommand.this.fireCommandKilled(AbstractShellCommand.this);
        }

        /**
         * Invokes method {@link ShellProcessListener#processFinished(ShellProcess)
         * processFinished} in all registered listeners in the case of this
         * process is finished its job.
         */
        @Override
        protected void fireProcessFinished() {
            super.fireProcessFinished();

            AbstractShellCommand.this.fireCommnadFinished(AbstractShellCommand.this);
        }

        /**
         * Executes this shell command.
         */
        @Override
        public void processMethod() {
            AbstractShellCommand.this.exitStatus = AbstractShellCommand.this.execute();
            AbstractShellCommand.this.done();
        }

        /**
         * Kills shell process (running of command).
         */
        @Override
        public void kill() {
            if (this.killed) {
                return;
            }

            if (this.getStatus() == ShellProcessStatus.RUNNING) {
                this.interrupt();
                AbstractShellCommand.this.actionAtKill();
                this.killed = true;

                try {
                    this.join(1000);
                } catch (InterruptedException exc) {
                    // One second to wait for closing operations.
                }

                this.setStatus(ShellProcessStatus.FINISHED);
            }
        }
    }

    /**
     * Constructs abstract shell process for specified shell.
     *
     * @param shellCore shell core which creates this shell command.
     */
    public AbstractShellCommand(final ShellCore shellCore) {
        this.core = shellCore;

        this.name = this.initName();
        this.manual = this.initManual();
        this.args = new String[0];

        if (this.core != null) {
            this.input = this.core.stdin;
            this.output = this.core.stdout;
            this.error = this.core.stderr;
        }

        this.pipe = null;
        this.exitStatus = -1;

        this.process = this.initShellProcess();

        this.listeners = new ArrayList<ShellCommandListener>();
    }

    /**
     * Initializes name of this shell command.
     *
     * @return Name of this shell command.
     */
    private String initName() {
        String clazzName = this.getClass().getSimpleName();
        boolean space = false;

        StringBuffer sb = new StringBuffer(clazzName.length());
        sb.append(Character.toLowerCase(clazzName.charAt(0)));
        for (int i = 1; i < clazzName.length(); i++) {
            char c = clazzName.charAt(i);

            if (Character.isUpperCase(c)) {
                space = true;
                sb.append(" ");
                sb.append(Character.toLowerCase(c));

                continue;
            }

            sb.append(c);
        }

        if (space) {
            return "\"" + sb + "\"";
        }

        return sb.toString();
    }

    /**
     * Reads manual for this shell command. Loaded manual is returned. If some
     * error occurs then it will returns empty string.
     *
     * @return Loaded manual for this shell command.
     */
    private String initManual() {
        String manual = "";
        URL manualURL = ResourcesLoader.getManual(this.name() + ".txt");
        if (manualURL != null) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(
                        manualURL.openStream(), "UTF-8"));
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    manual += line + "\n";
                }

                if (manual.length() != 0) {
                    manual = manual.substring(0, manual.length() - 1);
                }
            } catch (IOException exc) {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException exc1) {
                        // ignore
                    }
                }
            }
        }

        return manual;
    }

    /**
     * Initializes and returns shell process which performs this shell command.
     *
     * @return Initialized shell process which performs this shell command.
     */
    private ShellProcess initShellProcess() {
        return new CommandShellProcess();
    }

    /**
     * Returns name of this shell command which is derived from class name.
     *
     * @return Name of this shell command.
     */
    // JDK1.5 compatible @Override
    public String name() {
        return this.name;
    }

    /**
     * Returns loaded manual page for this shell command. If this command has no
     * manual pages, it returns empty string.
     *
     * @return Loaded manual page for this shell command or empty string.
     */
    // JDK1.5 compatible @Override
    public String getManual() {
        return this.manual;
    }

    /**
     * Returns usage of this shell command which has format:
     *
     * <pre>
     *     ${name}: usage: ${name} ${usage}
     * </pre>
     *
     * @return Returns usage of this shell command.
     */
    // JDK1.5 compatible @Override
    public String getUsage() {
        return this.name() + ": usage: " + this.name() + " " + this.getUsageString();
    }

    /**
     * Returns shell command specific usage.
     *
     * @return Shell command specific usage.
     */
    protected String getUsageString() {
        return "";
    }

    /**
     * Prints specified throwable into error stream.
     *
     * @param err throwable to print.
     */
    protected void printError(final Throwable err) {
        PrintStream error = this.getErrorStream();
        if (error == null) {
            return;
        }

        String message = err.getMessage();
        if (message.startsWith(this.name() + ":")) {
            error.println(message);

            return;
        }

        error.println(this.name() + ": " + err.getMessage());
    }

    /**
    * Prints specified error message.
    *
    * @param errorMsg error message to print.
    */
    protected void printError(final String errorMsg) {
        PrintStream error = this.getErrorStream();
        if (error == null) {
            return;
        }

        error.println(this.name() + ": " + errorMsg);
    }

    /**
     * Initializes input, output or error files.
     *
     * @param files array of files which will be initialized.
     * @param create information whether will be files created.
     * @throws ShellCommandException if some error occurs during files
     * initialization.
     */
    private void initFiles(final String[] files, final boolean create) throws ShellCommandException {
        for (String fileName : files) {
            File file = this.core.getFile(fileName);
            if (file.isDirectory()) {
                throw new ShellCommandException(this,
                        FileUtilities.toString(file) + ": is a directory");
            }

            if (!file.exists() && create) {
                try {
                    file.createNewFile();
                } catch (IOException exc) {
                    throw new ShellCommandException(this,
                            FileUtilities.toString(file) + ": "
                                    + exc.getMessage(), exc);
                }
            }
        }
    }

    /**
     * Initializes shell command from specified command structure. In
     * initialization method should be set arguments and streams. Also should be
     * set position of command process.
     *
     * @param structure command structure for initialization of this command.
     * @throws ShellCommandException if some error occurs during initialization.
     * This exception throws streams initialization.
     */
    // JDK1.5 compatible @Override
    public void init(final CommandStructure structure) throws ShellCommandException {
        this.args = structure.getParameters();

        String inputFile = structure.getInput();
        if (inputFile == null) {
            if (this.core != null) {
                this.input = this.core.stdin;
            }
        }
        else {
            this.initFiles(structure.getInputs(), false);
            this.setInputFile(inputFile);
        }

        CommandOutput output = structure.getOutput();
        if (output == null) {
            if (this.core != null) {
                this.output = this.core.stdout;
            }
        }
        else {
            this.initFiles(structure.getOutputFiles(), true);
            this.setOutputFile(output.getOutput(), output.isAppend());
        }

        if (structure.isBackground()) {
            this.getProcess().setPosition(ShellProcessPosition.BACKGROUND);
        }
        else {
            this.getProcess().setPosition(ShellProcessPosition.FOREGROUND);
        }
    }

    /**
     * Returns array of command arguments. This array is independent from
     * arguments in command so that the user can't change their content.
     *
     * @return Array of command arguments.
     */
    // JDK1.5 compatible @Override
    public String[] getArguments() {
        String[] array = new String[this.args.length];
        System.arraycopy(this.args, 0, array, 0, this.args.length);

        return array;
    }

    /**
     * Returns input stream for reading data by shell command.
     *
     * @return Input stream for reading data by shell command.
     */
    // JDK1.5 compatible @Override
    public InputStream getInputStream() {
        return this.input;
    }

    /**
     * Returns reader for input stream of shell command.
     *
     * @return Reader for input stream of shell command.
     */
    protected Reader getReader() {
        return this.reader;
    }

    /**
     * Returns reader for specified input stream.
     *
     * @return Reader for specified input stream.
     */
    protected Reader getReader(final InputStream input) {
        return new InputStreamReader(input,
                AbstractShellCommand.STREAMS_CHARSET);
    }

    /**
     * Sets input stream for shell command which can reads data from it.
     *
     * @param input input stream for shell command.
     */
    // JDK1.5 compatible @Override
    public void setInputStream(final InputStream input) {
        if (input == null && this.core != null) {
            this.input = this.core.stdin;
        }

        this.input = input;
        if (this.input != null) {
            this.reader = this.getReader(this.input);
        }
    }

    /**
     * Returns input file from which shell command could reads data. If this
     * command doesn't have input file, returns {@code null}.
     *
     * @return Input file from which shell command could reads data or
     * {@code null}.
     */
    protected File getInputFile() {
        return this.inputFile;
    }

    /**
     * Sets input file for which will be opened file input stream from which
     * shell command could reads data.
     *
     * @param filePath path for the input file.
     * @throws ShellCommandException if some error occurs with opening stream for
     * file with entered path.
     */
    // JDK1.5 compatible @Override
    public void setInputFile(final String filePath) throws ShellCommandException {
        if (filePath == null) {
            throw new ShellCommandException(this,
                    "name of input file cannot be null");
        }

        this.setInputFile(this.core.getFile(filePath));
    }

    /**
     * Sets input file for which will be opened file input stream from which
     * shell command could reads data.
     *
     * @param file input file.
     * @throws ShellCommandException if some error occurs with opening stream for
     * entered file.
     */
    // JDK1.5 compatible @Override
    public void setInputFile(final File file) throws ShellCommandException {
        if (file == null) {
            throw new ShellCommandException(this, "input file cannot be null");
        }

        if (!file.exists()) {
            throw new ShellCommandException(this, FileUtilities.toString(file)
                    + ": file doesn't exist");
        }

        this.inputFile = file;
        try {
            this.setInputStream(new FileInputStream(this.inputFile));
        } catch (IOException exc) {
            throw new ShellCommandException(this, FileUtilities.toString(file)
                    + ": cannot open input stream", exc);
        }
    }

    /**
     * Returns print stream for writing data by shell command.
     *
     * @return Print stream for writing data by shell command.
     */
    // JDK1.5 compatible @Override
    public PrintStream getOutputStream() {
        return this.output;
    }

    /**
     * Returns output file which shell command could uses for writing data. If
     * this command doesn't have output file, returns {@code null}.
     *
     * @return Output file which shell command could uses for writing data or
     * {@code null}.
     */
    protected File getOutputFile() {
        return this.outputFile;
    }

    /**
     * Returns information whether will be data appended into output file.
     *
     * @return information whether will be data appended into output file.
     */
    protected boolean isOutputAppend() {
        return this.append;
    }

    /**
     * Sets print stream for shell command which could writes data into it.
     *
     * @param output print stream for shell command.
     */
    // JDK1.5 compatible @Override
    public void setOutputStream(final PrintStream output) {
        if (output == null && this.core != null) {
            this.output = this.core.stdout;
        }

        this.output = output;
    }

    /**
     * Sets output file for which will be opened file output stream which could
     * used by shell command for writing data.
     *
     * @param filePath path for the output file.
     * @param append information whether will be data appended into output file.
     * @throws ShellCommandException if some error occurs with opening stream for
     * file with entered path.
     */
    // JDK1.5 compatible @Override
    public void setOutputFile(final String filePath,
                              final boolean append) throws ShellCommandException {
        if (filePath == null) {
            throw new ShellCommandException(this,
                    "name of output file cannot be null");
        }

        this.setOutputFile(this.core.getFile(filePath), append);
    }

    /**
     * Sets output file for which will be opened file output stream which could
     * used by shell command for writing data.
     *
     * @param file output file.
     * @param append information whether will be data appended into output file.
     * @throws ShellCommandException if some error occurs with opening stream for
     * entered file.
     */
    // JDK1.5 compatible @Override
    public void setOutputFile(final File file,
                              final boolean append) throws ShellCommandException {
        if (file == null) {
            throw new ShellCommandException(this, "output file cannot be null");
        }

        this.outputFile = file;
        this.append = append;
        try {
            this.setOutputStream(new PrintStream(new BufferedOutputStream(
                    new FileOutputStream(file, append)), false,
                    AbstractShellCommand.STREAMS_CHARSET.name()));
        } catch (IOException exc) {
            throw new ShellCommandException(this, FileUtilities.toString(file)
                    + ": cannot open output stream", exc);
        }
    }

    /**
     * Returns print stream for writing errors which occurs during shell command
     * execution.
     *
     * @return Print stream for writing shell command errors.
     */
    // JDK1.5 compatible @Override
    public PrintStream getErrorStream() {
        return this.error;
    }

    /**
     * Sets print stream for shell command which could writes errors into it.
     *
     * @param error print stream for shell command.
     */
    // JDK1.5 compatible @Override
    public void setErrorStream(final PrintStream error) {
        if (this.error == null && this.core != null) {
            this.error = this.core.stderr;
        }

        this.error = error;
    }

    /**
     * Sets output file for which will be opened file output stream which could
     * used by shell command for writing error messages.
     *
     * @param filePath path for the output file for error messages.
     * @param append information whether will be data appended into file for
     * error messages.
     * @throws ShellCommandException if some error occurs with opening stream for
     * file with entered path.
     */
    // JDK1.5 compatible @Override
    public void setErrorFile(final String filePath,
                             final boolean append) throws ShellCommandException {
        if (filePath == null) {
            throw new ShellCommandException(this,
                    "name of error output file cannot be null");
        }

        this.setErrorFile(this.core.getFile(filePath), append);
    }

    /**
     * Sets output file for which will be opened file output stream which could
     * used by shell command for writing error messages.
     *
     * @param file output file for error messages.
     * @param append information whether will be data appended into file for
     * error messages.
     * @throws ShellCommandException if some error occurs with opening stream for
     * file with entered path.
     */
    // JDK1.5 compatible @Override
    public void setErrorFile(final File file,
                             final boolean append) throws ShellCommandException {
        if (file == null) {
            throw new ShellCommandException(this,
                    "error output file cannot be null");
        }

        try {
            this.setErrorStream(new PrintStream(new BufferedOutputStream(
                    new FileOutputStream(file, append)), false,
                    AbstractShellCommand.STREAMS_CHARSET.name()));
        } catch (IOException exc) {
            throw new ShellCommandException(this, FileUtilities.toString(file)
                    + ": cannot open error output stream", exc);
        }
    }

    /**
     * Returns shell command which is piped with this shell command.
     *
     * @return Shell command which is piped with this one.
     */
    // JDK1.5 compatible @Override
    public ShellCommand getPipe() {
        return this.pipe;
    }

    /**
     * Sets pipe into specified shell command. If this command reads from
     * standard input stream and {@code command} writes into standard output then
     * will be created pipe streams between these one commands.
     *
     * @param command command which will be piped with this one.
     * @throws ShellCommandException if some error occurs during creating pipe
     * between commands.
     *
     * @see system.turboshell.pipe.PipeInputStream
     * @see system.turboshell.pipe.PipeOutputStream
     */
    // JDK1.5 compatible @Override
    public void setPipe(final ShellCommand command) throws ShellCommandException {
        this.pipe = command;

        try {
            if (this.pipe != null && this.core != null) {
                if (this.output == this.core.stdout
                        && this.pipe.getInputStream() == this.core.stdin) {
                    PipeOutputStream output = new PipeOutputStream();
                    PipeInputStream input = new PipeInputStream(output);

                    this.setOutputStream(new PrintStream(output));
                    this.pipe.setInputStream(input);
                }
            }
        } catch (IOException exc) {
            throw new ShellCommandException(this, "cannot initialize pipe", exc);
        }
    }

    /**
     * Returns shell process which performs this shell command.
     *
     * @return Shell process which performs this shell command.
     */
    // JDK1.5 compatible @Override
    public ShellProcess getProcess() {
        return this.process;
    }

    /**
     * Registers and adds specified shell command listener.
     *
     * @param listener shell command listener which will be added.
     */
    // JDK1.5 compatible @Override
    public void addListener(final ShellCommandListener listener) {
        if (listener == null) {
            return;
        }

        this.listeners.add(listener);
    }

    /**
     * Unregisters and removes specified shell command listener.
     *
     * @param listener shell command listener which will be removed.
     */
    // JDK1.5 compatible @Override
    public void removeListener(final ShellCommandListener listener) {
        if (listener == null) {
            return;
        }

        this.listeners.remove(listener);
    }

    /**
     * Returns all registered listeners for this shell command.
     *
     * @return Array of registered shell command listeners.
     */
    // JDK1.5 compatible @Override
    public ShellCommandListener[] getListeners() {
        ShellCommandListener[] array = new ShellCommandListener[this.listeners.size()];

        return this.listeners.toArray(array);
    }

    /**
     * Invokes method {@link ShellCommandListener#commandExecuted(ShellCommand)
     * commandExecuted} in all registered listeners in the case of this command
     * starts its execution.
     */
    protected void fireCommandExecuted(final ShellCommand command) {
        for (ShellCommandListener listener : this.listeners) {
            listener.commandExecuted(command);
        }
    }

    /**
     * Invokes method {@link ShellCommandListener#commandKilled(ShellCommand)
     * commandKilled} in all registered listeners in the case of this command was
     * killed.
     */
    protected void fireCommandKilled(final ShellCommand command) {
        for (ShellCommandListener listener : this.listeners) {
            listener.commandKilled(command);
        }
    }

    /**
     * Invokes method {@link ShellCommandListener#commandFinished(ShellCommand)
     * commandFinished} in all registered listeners in the case of this command
     * finished its execution.
     */
    protected void fireCommnadFinished(final ShellCommand command) {
        for (ShellCommandListener listener : this.listeners) {
            listener.commandFinished(command);
        }
    }

    /**
     * This method is called in case of this command was killed.
     */
    protected void actionAtKill() {
        this.done();
    }

    /**
     * Returns return status of command execution. It can be {@code 0} in case of
     * successful execution or {@code >1} in case of error. Positive integer
     * represents level of error ({@code 1} is lowest level of error).
     *
     * @return Return status of command execution.
     */
    // JDK1.5 compatible @Override
    public int getExecutionReturnStatus() {
        return this.exitStatus;
    }

    /**
     * Action after command execution. This method primary serves for closing
     * opened streams.
     */
    // JDK1.5 compatible @Override
    public void done() {
        try {
            if (this.output != null) {
                this.output.flush();
                this.output.close();
            }

            if (this.error != null) {
                this.error.flush();
                this.error.close();
            }

            if (this.reader != null) {
                this.reader.close();
            }

            if (this.input != null) {
                this.input.close();
            }
        } catch (IOException exc) {
            // so what?
            // you can this.core.stderr.println("Cannot close streams");
        }
    }

    /**
    * Returns string which represents usage of this command.
    *
    * @return String which represents usage of this command.
    * @see #getUsage()
    */
    @Override
    public String toString() {
        return this.getUsage();
    }
}
