/**
 * This Java file was created 11.11.2013 22:22:42.
 */
package system.turboshell.command;

import java.io.File;
import java.io.InputStream;
import java.io.PrintStream;

import system.turboshell.command.parser.CommandStructure;
import system.turboshell.process.ShellProcess;

/**
 * Interface {@code ShellCommand} represent single command in this shell system. 
 * Each command has name and it's called with some arguments (parameters). Each 
 * shell command uses input stream for reading data from standard input or input 
 * file. Also uses print streams for writing data or error messages into 
 * standard output/error or output file.
 * 
 * <p>Shell commands can be piped between them using piped streams.</p>
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.12
 * 
 * @since 0.4.0
 */
public interface ShellCommand {
    /** Constant for success exit status. */
    public static final int EXIT_SUCCESS = 0;
    /** Constant for failure exit status. */
    public static final int EXIT_FAILURE = 1;

    /**
     * Returns name of this shell command.
     *
     * @return Name of this shell command.
     */
    public String name();

    /**
     * Returns manual page for this shell command.
     *
     * @return Manual page for this shell command.
     */
    public String getManual();

    /**
     * Returns usage of this shell command. Example of usage:
     *
     * <pre>
     *     cat: usage: cat ${usage}
     * </pre>
     *
     * @return Returns usage of this shell command.
     */
    public String getUsage();

    /**
     * Initializes shell command from specified command structure. In 
     * initialization method should be set arguments and streams. Also 
     * should be set position of command process.
     *
     * @param structure command structure for initialization of this command.
     * @throws ShellCommandException if some error occurs during initialization. 
     * This exception should be thrown by streams initialization.
     */
    public void init(CommandStructure structure) throws ShellCommandException;

    /**
     * Returns array of command arguments. This array should be independent from 
     * arguments in command so that the user can't change their content.
     *
     * @return Array of command arguments.
     */
    public String[] getArguments();

    /**
     * Returns input stream for reading data by shell command.
     *
     * @return Input stream for reading data by shell command.
     */
    public InputStream getInputStream();

    /**
     * Sets input stream for shell command which could reads data from it.
     *
     * @param input input stream for shell command.
     */
    public void setInputStream(InputStream input);

    /**
     * Sets input file for which will be opened file input stream from which 
     * shell command could reads data.
     *
     * @param filePath path for the input file.
     * @throws ShellCommandException if some error occurs with opening stream 
     * for file with entered path.
     */
    public void setInputFile(String filePath) throws ShellCommandException;

    /**
     * Sets input file for which will be opened file input stream from which 
     * shell command could reads data.
     *
     * @param file input file.
     * @throws ShellCommandException if some error occurs with opening stream 
     * for entered file.
     */
    public void setInputFile(File file) throws ShellCommandException;

    /**
     * Returns print stream for writing data by shell command.
     *
     * @return Print stream for writing data by shell command.
     */
    public PrintStream getOutputStream();

    /**
     * Sets print stream for shell command which could writes data into it.
     *
     * @param output print stream for shell command.
     */
    public void setOutputStream(PrintStream output);

    /**
     * Sets output file for which will be opened file output stream which could 
     * used by shell command for writing data.
     *
     * @param filePath path for the output file.
     * @param append information whether will be data appended into output file.
     * @throws ShellCommandException if some error occurs with opening stream 
     * for file with entered path.
     */
    public void setOutputFile(String filePath, boolean append) throws ShellCommandException;

    /**
     * Sets output file for which will be opened file output stream which could 
     * used by shell command for writing data.
     *
     * @param file output file.
     * @param append information whether will be data appended into output file.
     * @throws ShellCommandException if some error occurs with opening stream 
     * for entered file.
     */
    public void setOutputFile(File file, boolean append) throws ShellCommandException;

    /**
     * Returns print stream for writing errors which occurs during shell 
     * command execution.
     *
     * @return Print stream for writing shell command errors.
     */
    public PrintStream getErrorStream();

    /**
     * Sets print stream for shell command which could writes errors into it.
     *
     * @param error print stream for shell command.
     */
    public void setErrorStream(PrintStream error);

    /**
     * Sets output file for which will be opened file output stream which could 
     * used by shell command for writing error messages.
     *
     * @param filePath path for the output file for error messages.
     * @param append information whether will be data appended into file for 
     * error messages.
     * @throws ShellCommandException if some error occurs with opening stream 
     * for file with entered path.
     */
    public void setErrorFile(String filePath, boolean append) throws ShellCommandException;

    /**
     * Sets output file for which will be opened file output stream which could 
     * used by shell command for writing error messages.
     *
     * @param file output file for error messages.
     * @param append information whether will be data appended into file for 
     * error messages.
     * @throws ShellCommandException if some error occurs with opening stream 
     * for file with entered path.
     */
    public void setErrorFile(File file, boolean append) throws ShellCommandException;

    /**
     * Returns shell command which is piped with this shell command.
     *
     * @return Shell command which is piped with this one.
     */
    public ShellCommand getPipe();

    /**
     * Sets pipe into specified shell command. If this command reads from 
     * standard input stream and {@code command} writes into standard output 
     * then will be created pipe streams between these one commands.
     *
     * @param command command which will be piped with this one.
     * @throws ShellCommandException if some error occurs during creating pipe 
     * between commands.
     * 
     * @see system.turboshell.pipe.PipeInputStream
     * @see system.turboshell.pipe.PipeOutputStream
     */
    public void setPipe(ShellCommand command) throws ShellCommandException;

    /**
     * Returns shell process which performs this shell command. It's very not 
     * recommended use this implementation of this method:
     * <pre>
     *     public ShellProcess getProcess() {
     *         return new ShellProcess() {
     *             ... implementation
     *         }
     *     }
     * </pre>
     * 
     * because this method can be called multiple times and its expected to 
     * get same result of each call.
     * 
     * @return Shell process which performs this shell command.
     */
    public ShellProcess getProcess();

    /**
     * Registers and adds specified shell command listener.
     *
     * @param listener shell command listener which will be added.
     */
    public void addListener(ShellCommandListener listener);

    /**
     * Unregisters and removes specified shell command listener.
     *
     * @param listener shell command listener which will be removed.
     */
    public void removeListener(ShellCommandListener listener);

    /**
     * Returns all registered listeners for this shell command.
     *
     * @return Array of registered shell command listeners.
     */
    public ShellCommandListener[] getListeners();

    /**
     * Executes command. Command can use standard I/O or input and outputs into 
     * specified files. Also can read parameters for command which can be 
     * obtained by method {@link #getArguments()}. Command execution returns 
     * status of command execution for which can be used predefined constants 
     * {@code EXIT_SUCCESS} for successful execution and {@code EXIT_FAILURE} 
     * for execution with some kind of error.
     *
     * @return Return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     */
    public int execute();

    /**
     * Returns return status of command execution. It can be {@code 0} in case 
     * of successful execution or {@code >1} in case of error. Positive integer 
     * represents level of error ({@code 1} is lowest level of error).
     *
     * @return Return status of command execution. 
     */
    public int getExecutionReturnStatus();

    /**
     * Action after command execution. This method primary serves for closing 
     * opened streams.
     */
    public void done();
}
