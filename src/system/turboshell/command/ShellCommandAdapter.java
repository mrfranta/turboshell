/**
 * This Java file was created 15.11.2013 16:14:08.
 */
package system.turboshell.command;

/**
 * Abstract class {@code ShellCommandAdapter} implements {@link 
 * ShellCommandListener} and represents adapter for listeners. 
 * This adapter is appropriate for a simpler implementation of 
 * shell command listeners.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.4.12
 */
public class ShellCommandAdapter implements ShellCommandListener {
    /**
     * Action that is being invoked in case of command execution.
     *
     * @param command command which was executed.
     */
    // JDK1.5 compatible @Override
    public void commandExecuted(final ShellCommand command) {
    }

    /**
     * Action that is being invoked in case of command was killed.
     *
     * @param command command which was killed.
     */
    // JDK1.5 compatible @Override
    public void commandKilled(final ShellCommand command) {
    }

    /**
     * Action that is being invoked in case of command finished.
     *
     * @param command command which was finished.
     */
    // JDK1.5 compatible @Override
    public void commandFinished(final ShellCommand command) {
    }
}
