/**
 * Package {@code system.turboshell.command.history} contains command history 
 * for shell console.
 */
package system.turboshell.command.history;

