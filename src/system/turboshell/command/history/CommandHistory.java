/**
 * This Java file was created 10.10.2013 8:38:30.
 */
package system.turboshell.command.history;

import system.utils.StringUtilities;

/**
 * Class {@code CommandHistory} represents command history for shell console.
 * This implementation is based on linked stack.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.2.0
 */
public class CommandHistory {
    /** Head of command history. */
    private Command head;
    /** Tail of command history. */
    private Command tail;
    /** Iterator which points to actual command. */
    private Command iterator;

    /** Number of commands in command history. */
    private int size;

    /**
     * Class {@code Command} represents one command in command history.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.3
     * 
     * @since 0.2.0
     */
    private class Command {
        /** Command string. */
        private String command;
        /** Pointer to previous command. */
        private Command prev;
        /** Pointer to next command. */
        private Command next;

        /**
         * Constructs command in history.
         */
        public Command() {
            this(null, null, null);
        }

        /**
         * Constructs command in history.
         *
         * @param command command string.
         * @param prev pointer to previous command.
         * @param next pointer to next command.
         */
        public Command(final String command,
                       final Command prev,
                       final Command next) {
            this.command = command;
            this.prev = prev;
            this.next = next;
        }

        /**
         * Returns pointer to previous command in history.
         *
         * @return Pointer to previous command in history.
         */
        public Command getPrev() {
            return this.prev;
        }

        /**
         * Sets pointer to previous command in history.
         *
         * @param next pointer to previous command in history.
         */
        public void setPrev(final Command prev) {
            this.prev = prev;
        }

        /**
         * Returns pointer to next command in history.
         *
         * @return Pointer to next command in history.
         */
        public Command getNext() {
            return this.next;
        }

        /**
         * Sets pointer to next command in history.
         *
         * @param next pointer to next command in history.
         */
        public void setNext(final Command next) {
            this.next = next;
        }

        /**
         * Returns command string.
         *
         * @return Command string.
         */
        public String getCommand() {
            return this.command;
        }
    }

    /**
     * Constructs command history.
     */
    public CommandHistory() {
        this.head = new Command();
        this.tail = new Command();
        this.iterator = null;

        this.head.setNext(this.tail);
        this.tail.setPrev(this.head);
    }

    /**
     * Returns number of commands in command history.
     *
     * @return Number of commands in history.
     */
    public int getSize() {
        return this.size;
    }

    /**
     * Returns information whether command history is empty.
     *
     * @return {@code true} whether is command history empty; 
     *         {@code false} otherwise. 
     */
    public boolean isEmpty() {
        return (this.size > 0);
    }

    /**
     * Adds new command into command history. It also moves history iterator 
     * to history head.
     *
     * @param command command which will be added.
     */
    public void addCommand(final String command) {
        if (command == null) {
            return;
        }

        String whiteSpaceLessCommand = command.replaceAll(" +", "");
        if (StringUtilities.isEmpty(whiteSpaceLessCommand)) {
            return;
        }

        Command second = this.head.getNext();
        Command first = new Command(command, this.head, second);
        second.setPrev(first);
        this.head.setNext(first);

        this.moveIteratorToHead();

        this.size++;
    }

    /**
     * Returns actual command.
     *
     * @return Actual command.
     */
    public String getIteratorCommand() {
        if (this.size == 0) {
            return null;
        }

        return this.iterator.getCommand();
    }

    /**
     * Moves history iterator to history head.
     */
    public void moveIteratorToHead() {
        this.iterator = this.head;
    }

    /**
     * Moves history iterator forward.
     *
     * @return Actual command.
     */
    public String moveIteratorForward() {
        if (this.iterator == null) {
            return null;
        }

        Command next = this.iterator.getNext();
        if (next != null && next.getCommand() != null) {
            this.iterator = next;
        }

        return this.getIteratorCommand();
    }

    /**
     * Moves history iterator backward.
     *
     * @return Actual command.
     */
    public String moveIteratorBackward() {
        if (this.iterator == null) {
            return null;
        }

        Command prev = this.iterator.getPrev();
        if (prev != null && prev.getCommand() != null) {
            this.iterator = prev;
        }

        return this.getIteratorCommand();
    }
}
