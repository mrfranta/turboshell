/**
 * This Java file was created 6.12.2013 22:04:30.
 */
package system.turboshell.command;

/**
 * Class {@code InvalidOptionException} represents exception for invalid option 
 * in shell command.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.0
 */
public class InvalidOptionException extends ShellCommandException {
    /** 
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a href=
     * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
     * details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 1L;

    /** 
     * Constructs exception for invalid option in shell command.
     *
     * @param command exception creator.
     * @param option invalid option.
     */
    public InvalidOptionException(final ShellCommand command,
                                  final char option) {
        this(command, "" + option);
    }

    /** 
     * Constructs exception for invalid option in shell command.
     *
     * @param command exception creator.
     * @param option invalid option.
     */
    public InvalidOptionException(final ShellCommand command,
                                  final String option) {
        super("invalid option '" + option + "'\n"
                + "Try '" + command.name() + " --help' for more information.");
    }
}
