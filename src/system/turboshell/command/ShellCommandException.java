/**
 * This Java file was created 28.10.2013 20:22:16.
 */
package system.turboshell.command;

/**
 * Class {@code ShellCommandException} represents exception for shell command.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @author pkopac (Petr Kopač)
 * @version 1.4
 * 
 * @since 0.4.0
 */
public class ShellCommandException extends Exception {
    /** 
     * Determines if a de-serialized file is compatible with this class.
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Oracle docs for <a href=
     * "http://download.oracle.com/javase/1.5.0/docs/guide/serialization/index.html">
     * details</a>.
     * 
     * Not necessary to include in first version of the class, but included here
     * as a reminder of its importance.
     */
    private static final long serialVersionUID = 4L;

    /** 
     * Constructs exception for shell command with specified message.
     *
     * @param message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     */
    public ShellCommandException(final String message) {
        super(message);
    }

    /** 
     * Constructs exception for shell command with specified message.
     *
     * @param command exception creator.
     * @param message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     */
    public ShellCommandException(final ShellCommand command,
                                 final String message) {
        super(command.name() + ": " + message);
    }

    /** 
     * Constructs exception for shell command with specified cause.
     *
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method). (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellCommandException(final Throwable cause) {
        super(cause);
    }

    /** 
     * Constructs exception for shell command with specified message and cause.
     *
     * @param  message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method).  (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellCommandException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /** 
     * Constructs exception for shell command with specified message.
     *
     * @param command exception creator.
     * @param message the detail message (which is saved for later retrieval
     * by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method).  (A <tt>null</tt> value is permitted, and 
     * indicates that the cause is nonexistent or unknown.)
     */
    public ShellCommandException(final ShellCommand command,
                                 final String message,
                                 final Throwable cause) {
        super(command.name() + ": " + message, cause);
    }
}
