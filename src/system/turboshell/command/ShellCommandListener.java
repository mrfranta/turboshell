/**
 * This Java file was created 13.11.2013 17:28:32.
 */
package system.turboshell.command;

/**
 * Interface {@code ShellCommandListener} serves for listening of changes in 
 * shell command. This interface allows listen to command execution, completion 
 * or killing.
 * 
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.4.6
 */
public interface ShellCommandListener {
    /**
     * Action that is being invoked in case of command execution.
     *
     * @param command command which was executed.
     */
    public void commandExecuted(ShellCommand command);

    /**
     * Action that is being invoked in case of command was killed.
     *
     * @param command command which was killed.
     */
    public void commandKilled(ShellCommand command);

    /**
     * Action that is being invoked in case of command finished.
     *
     * @param command command which was finished.
     */
    public void commandFinished(ShellCommand command);
}
