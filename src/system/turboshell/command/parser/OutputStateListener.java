/**
 * This Java file was created 25.10.2013 15:03:17.
 */
package system.turboshell.command.parser;

import system.automaton.ErrorState;
import system.automaton.State;

/**
 * Class {@code OutputStateListener} is listener for {@code O} state in finite 
 * automaton for commands.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.1.8
 */
public class OutputStateListener extends CommandsStateAdapter {
    /**
     * Action that is being invoked in case of using transition to leave owner 
     * of this listener and entry into specified state. If the character is 
     * equal '|' than this method do nothing. Otherwise tells commands parser 
     * that next parameter will be output.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void leavingState(final State targetState, final char character) {
        if (character == '|') {
            return;
        }

        if (this.getState() != targetState && !(targetState instanceof ErrorState)) {
            this.getParser().setOutput();
        }
    }
}
