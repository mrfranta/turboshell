/**
 * This Java file was created 24.10.2013 20:52:54.
 */
package system.turboshell.command.parser;

import system.automaton.ErrorState;
import system.automaton.State;

/**
 * Class {@code CommandStateListener} is listener for {@code C} state in finite 
 * automaton for commands.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.1.8
 */
public class CommandStateListener extends CommandsStateAdapter {
    /** Buffer for loading command. */
    private StringBuffer command;

    /**
     * Constructs listener
     */
    public CommandStateListener() {
        this.command = new StringBuffer();
    }

    /**
     * Action that is being invoked in case of using transition to entry 
     * into owner of this listener from specified state. It appends 
     * character into buffer for loading command.
     *
     * @param sourceState state from which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void entryState(final State sourceState, final char character) {
        this.command.append(character);
    }

    /**
     * Action that is being invoked in case of using transition to leave owner 
     * of this listener and entry into specified state. It add loaded command 
     * into commands parser.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void leavingState(final State targetState, final char character) {
        if (targetState != this.getState() && !(targetState instanceof ErrorState)) {
            String cmd = this.removeEscapes(this.command.toString());
            this.command.setLength(0);

            CommandStructure command = new CommandStructure(cmd);
            this.getParser().addCommand(command);
        }
    }
}
