/**
 * This Java file was created 23.10.2013 20:28:41.
 */
package system.turboshell.command.parser;

import java.util.ArrayList;
import java.util.List;

import system.turboshell.command.ShellCommand;

/**
 * Class {@code CommandStructure} represents structure of command which was 
 * parsed from XML file for commands by commands parser. Commands parser 
 * stores all parsed data in this structures which are used for initialization 
 * of {@link ShellCommand shell command}.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.12
 * 
 * @since 0.1.8
 */
public class CommandStructure {
    /** Name of command. */
    private final String name;
    /** Command parameters. */
    private final List<String> parameters;

    /** List of input files. */
    private final List<String> inputs;
    /** List of output files. */
    private final List<CommandOutput> outputs;

    /** Information whether will be this command run in background. */
    private boolean background;

    /**
     * Constructs structure of command with specified name.
     *
     * @param name command name.
     * @throws NullPointerException if command name is {@code null}.
     */
    public CommandStructure(final String name) {
        if (name == null) {
            throw new NullPointerException("Command name cannot be null");
        }

        this.name = name;
        this.parameters = new ArrayList<String>();
        this.inputs = new ArrayList<String>();
        this.outputs = new ArrayList<CommandOutput>();
        this.background = false;
    }

    /**
     * Returns name of command.
     *
     * @return Command name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Adds parameter into command structure.
     *
     * @param parameter parameter to add.
     */
    public void addParameter(final String parameter) {
        if (parameter == null) {
            return;
        }

        this.parameters.add(parameter);
    }

    /**
     * Removes parameter from command structure.
     *
     * @param parameter parameter to remove.
     */
    public void removeParameter(final String parameter) {
        if (parameter == null) {
            return;
        }

        this.parameters.remove(parameter);
    }

    /**
     * Returns array of command parameters.
     *
     * @return Array of command parameters.
     */
    public String[] getParameters() {
        String[] array = new String[this.parameters.size()];

        return this.parameters.toArray(array);
    }

    /**
     * Sets parameters for command structure.
     *
     * @param params parameters to set.
     */
    public void setParameters(final List<String> params) {
        this.parameters.clear();
        this.parameters.addAll(params);
    }

    /**
     * Adds command input into command structure.
     *
     * @param input input to add.
     */
    public void addInput(final String input) {
        if (input == null) {
            return;
        }

        String striped = CommandStructure.removeEscapes(input);
        this.inputs.add(striped);
    }

    /**
     * Removes command input from command structure.
     *
     * @param input input to remove.
     */
    public void removeInput(final String input) {
        if (input == null) {
            return;
        }

        this.inputs.remove(input);
    }

    /**
     * Returns last added command output.
     *
     * @return Last added command output.
     */
    public String getInput() {
        if (this.inputs.isEmpty()) {
            return null;
        }

        return this.inputs.get(this.inputs.size() - 1);
    }

    /**
     * Returns array of command inputs.
     *
     * @return Array of command inputs.
     */
    public String[] getInputs() {
        String[] array = new String[this.inputs.size()];

        return this.inputs.toArray(array);
    }

    /**
     * Adds command output into command structure.
     *
     * @param output path for output file.
     * @param append information whether will be data appended into output.
     */
    public void addOutput(final String output, final boolean append) {
        if (output == null) {
            return;
        }

        String striped = CommandStructure.removeEscapes(output);
        this.outputs.add(new CommandOutput(striped, append));
    }

    /**
     * Removes output from command structure.
     *
     * @param output output to remove.
     */
    public void removeOutput(final String output) {
        if (output == null || this.outputs.isEmpty()) {
            return;
        }

        CommandOutput toRemove = null;
        for (CommandOutput cmdOutput : this.outputs) {
            if (cmdOutput.getOutput().equals(output)) {
                toRemove = cmdOutput;
                break;
            }
        }

        if (toRemove != null) {
            return;
        }

        this.outputs.remove(toRemove);
    }

    /**
     * Returns last added command output.
     *
     * @return Last added command output.
     */
    public CommandOutput getOutput() {
        if (this.outputs.isEmpty()) {
            return null;
        }

        return this.outputs.get(this.outputs.size() - 1);
    }

    /**
     * Returns array of command outputs.
     *
     * @return Array of command outputs.
     */
    public CommandOutput[] getOutputs() {
        CommandOutput[] array = new CommandOutput[this.outputs.size()];

        return this.outputs.toArray(array);
    }

    /**
     * Returns array of output files (paths for files).
     *
     * @return Array of output files.
     */
    public String[] getOutputFiles() {
        String[] files = new String[this.outputs.size()];

        int i = 0;
        for (CommandOutput output : this.outputs) {
            files[i++] = output.getOutput();
        }

        return files;
    }

    /**
     * Sets whether commands will run in background or foreground.
     *
     * @param background information whether commands will run in 
     * background or foreground.
     */
    public void setBackground(final boolean background) {
        this.background = background;
    }

    /**
     * Returns information whether commands will run in background 
     * or foreground.
     *
     * @return Information whether commands will run in background 
     * or foreground.
     */
    public boolean isBackground() {
        return this.background;
    }

    /**
     * Returns full class name for command. Class name is used by java 
     * reflection package for dynamically instancing of commands.
     * 
     * @return Full class name of command.
     */
    public String getCommandClassName() {
        StringBuffer sb = new StringBuffer(this.name.length());
        String[] words = this.name.toLowerCase().split("\\s");
        for (int i = 0, l = words.length; i < l; ++i) {
            sb.append(Character.toUpperCase(words[i].charAt(0)));
            sb.append(words[i].substring(1));
        }

        String clazzName = "system.turboshell.command.cmd." + sb.toString();

        return clazzName;
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + (this.background ? 1231 : 1237);
        result = prime * result + ((this.inputs == null) ? 0 : this.inputs.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + ((this.outputs == null) ? 0 : this.outputs.hashCode());
        result = prime * result + ((this.parameters == null) ? 0 : this.parameters.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CommandStructure)) {
            return false;
        }

        CommandStructure other = (CommandStructure) obj;
        if (this.background != other.background) {
            return false;
        }

        if (this.inputs == null) {
            if (other.inputs != null) {
                return false;
            }
        }
        else if (!this.inputs.equals(other.inputs)) {
            return false;
        }

        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        }
        else if (!this.name.equals(other.name)) {
            return false;
        }

        if (this.outputs == null) {
            if (other.outputs != null) {
                return false;
            }
        }
        else if (!this.outputs.equals(other.outputs)) {
            return false;
        }

        if (this.parameters == null) {
            if (other.parameters != null) {
                return false;
            }
        }
        else if (!this.parameters.equals(other.parameters)) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of command structure that has the format:
     * 
     * <pre>
     *     ${name} ${parameters} [< ${input}]... [${output}]...
     * </pre>
     *
     * @return String value of command structure.
     */
    @Override
    public String toString() {
        String command = this.getName();

        for (String parameter : this.parameters) {
            command += " " + parameter;
        }

        for (String input : this.inputs) {
            command += " < " + input;
        }

        for (CommandOutput output : this.outputs) {
            command += " " + output;
        }

        return command;
    }

    /**
     * Returns string value of entered command structures. If array of command 
     * structures is {@code null} or empty then this method returns empty 
     * string.
     * 
     * <p>String is in format:</p>
     * <pre>
     *     ${name} ${parameters} [< ${input}]... [${output}]...[ | ${name} ${parameters} [< ${input}]... [${output}]...]...[ &]
     * </pre>
     *
     * @param commandStructures array of command structures.
     * @return String value of entered command structures.
     */
    public static String toString(final CommandStructure[] commandStructures) {
        if (commandStructures == null || commandStructures.length == 0) {
            return "";
        }

        StringBuffer sb = new StringBuffer();
        sb.append(commandStructures[0].toString());
        for (int i = 1; i < commandStructures.length; i++) {
            sb.append(" | ");
            sb.append(commandStructures[i].toString());
        }

        if (commandStructures[0].isBackground()) {
            sb.append(" &");
        }

        return sb.toString();
    }

    /**
     * Removes redundant backslashes from specified string.
     *
     * @param str string which contains redundant backslashes.
     * @return String without redundant backslashes.
     */
    public static String removeEscapes(final String str) {
        int len = str.length();
        StringBuffer sb = new StringBuffer(len);
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            if (c == '\\') {
                if ((i + 1) < len) {
                    sb.append(str.charAt(++i));
                }
                else {
                    break;
                }
            }
            else {
                sb.append(c);
            }
        }

        return sb.toString();
    }
}
