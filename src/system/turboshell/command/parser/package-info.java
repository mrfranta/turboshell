/**
 * Package {@code system.turboshell.command.parser} contains parser for 
 * commands which uses finite automaton. This automaton contains a lot of 
 * state listeners which are implemented in this package. Also contains 
 * structure of command which is used for command initialization.
 */
package system.turboshell.command.parser;

