/**
 * This Java file was created 25.10.2013 14:29:52.
 */
package system.turboshell.command.parser;

import system.automaton.ErrorState;
import system.automaton.State;

/**
 * Class {@code InputStateListener} is listener for {@code I} state in finite 
 * automaton for commands.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.2
 * 
 * @since 0.1.8
 */
public class InputStateListener extends CommandsStateAdapter {
    /**
     * Action that is being invoked in case of using transition to leave owner 
     * of this listener and entry into specified state. It tells commands parser 
     * that next parameter will be input.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void leavingState(final State targetState, final char character) {
        if (this.getState() != targetState && !(targetState instanceof ErrorState)) {
            this.getParser().setInput();
        }
    }
}
