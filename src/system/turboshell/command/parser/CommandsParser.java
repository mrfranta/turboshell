/**
 * This Java file was created 23.10.2013 20:18:21.
 */
package system.turboshell.command.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.SAXException;

import system.automaton.ErrorState;
import system.automaton.FiniteAutomaton;
import system.automaton.State;
import system.automaton.StateListener;
import system.automaton.xml.XMLFiniteAutomatonLoader;

/**
 * Class {@code CommandsParser} parses commands in string into command 
 * structures which are used for initialization of shell commands. This parser 
 * uses modified finite automaton for parsing commands. Configuration of this 
 * finite automaton is read from XML file using JAXB technology.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.6
 * 
 * @since 0.1.8
 */
public class CommandsParser {
    /** Finite automaton for parsing commands. */
    private FiniteAutomaton automaton;

    /** List of parsed command structures. */
    private List<CommandStructure> commands;
    /** Information whether next parsed parameter will be input. */
    private boolean input;
    /** Information whether next parsed parameter will be output. */
    private boolean output;
    /** Information whether next parsed parameter will be appending output. */
    private boolean append;

    /** Name of automaton for parsing commands. */
    private static final String AUTOMATON_NAME = "Commands";

    /**
     * Class {@code ParserFiniteAutomaton} is finite automaton for parsing 
     * commands from string.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.5
     * 
     * @since 0.1.8
     */
    private static class ParserFiniteAutomaton extends FiniteAutomaton {
        /** Error message for non-existing transition. */
        public static final String ERROR_MSG = "syntax error near unexpected token „%c“";

        /**
         * Constructs finite automaton with specified initial state and name.
         *
         * @param name name of finite automaton.
         * @param initialState name of initial state of finite automaton.
         */
        public ParserFiniteAutomaton(final String name,
                                     final String initialState) {
            super(name, initialState);
        }

        /**
         * Changes actual state of finite automaton. This method also invokes 
         * listeners in actual and target states. It also sets error message 
         * into non-existing transition error state.
         *
         * @param targetState target state which will be actual. If target state 
         * is {@code null}, it will be set initial state as actual.
         * @param character character through which will be changed actual state.
         */
        @Override
        protected void changeActualState(final State targetState,
                                         final char character) {
            super.changeActualState(targetState, character);

            ErrorState errorState = ParserFiniteAutomaton.INVALID_TRANSITION;
            if (targetState == errorState) {
                errorState.setErrorMessage(String.format(
                        ParserFiniteAutomaton.ERROR_MSG, character));
            }
        }
    }

    /**
     * Class {@code ParserFiniteAutomatonLoader} serves for loading parser 
     * finite automaton from XML file which contains configuration of parser 
     * finite automaton using JAXB technology.
     *
     * @author Mr.FrAnTA (Michal Dékány)
     * @version 1.5
     * 
     * @since 0.1.8
     */
    private static class ParserFiniteAutomatonLoader extends XMLFiniteAutomatonLoader {
        /**
         * Constructs loader for finite automaton from XML files.
         *
         * @throws SAXException if an error occurs while loading the XSD file or creating a 
         * XML validator.
         */
        public ParserFiniteAutomatonLoader() throws SAXException {
            super();
        }

        /**
         * Creates parser finite automaton.
         *
         * @param name name of finite automaton.
         * @param initialState name of initial state.
         * @return Created parser finite automaton.
         */
        @Override
        protected FiniteAutomaton createFiniteAutomaton(final String name,
                                                        final String initialState) {
            return new ParserFiniteAutomaton(name, initialState);
        }
    }

    /**
     * Constructs parser for parsing commands from string. Parser uses finite 
     * automaton which must be loaded.
     *
     * @throws Exception if some error occurs in finite automaton loading.
     */
    public CommandsParser() throws Exception {
        ParserFiniteAutomatonLoader loader = new ParserFiniteAutomatonLoader();

        this.automaton = loader.loadAutomaton(CommandsParser.AUTOMATON_NAME);
        State[] states = this.automaton.getStates();
        for (State state : states) {
            StateListener[] listeners = state.getListeners();
            for (StateListener listener : listeners) {
                if (listener instanceof CommandsStateAdapter) {
                    ((CommandsStateAdapter) listener).setParser(this);
                }
            }
        }

        this.commands = new ArrayList<CommandStructure>();
        this.input = false;
        this.output = false;
    }

    /**
     * Adds parsed command (structure).
     *
     * @param command parsed command (structure) to add.
     */
    protected void addCommand(final CommandStructure command) {
        if (command == null) {
            return;
        }

        this.commands.add(command);
    }

    /**
     * Returns array of parsed command structures.
     *
     * @return Array of parsed command structures.
     */
    public CommandStructure[] getCommands() {
        CommandStructure[] array = new CommandStructure[this.commands.size()];

        return this.commands.toArray(array);
    }

    /**
     * Sets information that next parsed parameter will be input.
     */
    protected void setInput() {
        this.input = true;
    }

    /**
     * Sets information that next parsed parameter will be output.
     */
    protected void setOutput() {
        this.output = true;
    }

    /**
     * Sets information that next parsed parameter will be appended output.
     */
    protected void setAppend() {
        this.append = true;
    }

    /**
     * Adds parsed parameter into actual command structure.
     *
     * @param parameter parsed parameter which will be added.
     */
    protected void addParameter(final String parameter) {
        CommandStructure command = this.commands.get(this.commands.size() - 1);

        if (this.input) {
            command.addInput(parameter);
            this.input = false;

            return;
        }

        if (this.output) {
            command.addOutput(parameter, this.append);
            this.output = false;
            this.append = false;
            return;
        }

        command.addParameter(parameter);
    }

    /**
     * Sets information whether all parsed commands will run in background.
     */
    protected void setBackground() {
        for (CommandStructure command : this.commands) {
            command.setBackground(true);
        }
    }

    /**
     * Process string which contains one or more commands. Commands parsers 
     * will use finite automaton for parsing command into command structures.
     *
     * @param command command which will be processed (parsed) by commands 
     * parser.
     * @return Array of parsed command structures.
     * @throws CommandsParserException if entered command contains syntax error.
     */
    public CommandStructure[] process(final String command) throws CommandsParserException {
        this.commands.clear();

        boolean result = this.automaton.process(command);
        if (!result) {
            State actual = this.automaton.getActual();
            if (!actual.isFinal()) {
                char last = command.charAt(command.length() - 1);

                throw new CommandsParserException(String.format(
                        ParserFiniteAutomaton.ERROR_MSG, last));
            }

            throw new CommandsParserException(this.automaton.getErrorMessage());
        }

        return this.getCommands();
    }
}
