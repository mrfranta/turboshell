/**
 * This Java file was created 25.10.2013 15:38:02.
 */
package system.turboshell.command.parser;

import system.automaton.ErrorState;
import system.automaton.State;

/**
 * Class {@code ParameterStateListener} is listener for {@code P} state in finite 
 * automaton for commands.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.1.8
 */
public class ParameterStateListener extends CommandsStateAdapter {
    /** Buffer for loading parameter. */
    private StringBuffer parameter;

    /**
     * Constructs listener.
     */
    public ParameterStateListener() {
        this.parameter = new StringBuffer();
    }

    /**
     * Action that is being invoked in case of using transition to entry 
     * into owner of this listener from specified state. It appends 
     * character into buffer for loading parameter.
     *
     * @param sourceState state from which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void entryState(final State sourceState, final char character) {
        this.parameter.append(character);
    }

    /**
     * Action that is being invoked in case of using transition to leave owner 
     * of this listener and entry into specified state. It add loaded parameter 
     * into commands parser.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void leavingState(final State targetState, final char character) {
        if (targetState != this.getState() && !(targetState instanceof ErrorState)) {
            String param = this.removeEscapes(this.parameter.toString());
            this.parameter.setLength(0);

            this.getParser().addParameter(param);
        }
    }
}
