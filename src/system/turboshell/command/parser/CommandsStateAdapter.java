/**
 * This Java file was created 25.10.2013 3:33:44.
 */
package system.turboshell.command.parser;

import system.automaton.StateAdapter;

/**
 * Abstract class {@code CommandsStateAdapter} extends {@link StateAdapter} and 
 * represents adapter for listeners of states of commands parser. This adapter 
 * is appropriate for a simpler implementation these listeners.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.3
 * 
 * @since 0.1.8
 */
public abstract class CommandsStateAdapter extends StateAdapter {
    /** 
     * Commands parser which can be used by listeners (children of this adapter). 
     */
    private CommandsParser parser;

    /**
     * Returns commands parser for this adapter.
     *
     * @return Commands parser for this adapter.
     */
    public CommandsParser getParser() {
        return this.parser;
    }

    /**
     * Sets command parser for this adapter.
     *
     * @param parser commands parser to set.
     * @throws NullPointerException if commands parser is {@code null}.
     */
    public void setParser(final CommandsParser parser) {
        if (parser == null) {
            throw new NullPointerException("Parser cannot be null");
        }

        this.parser = parser;
    }

    /**
     * Removes escape sequence for quotes from specified string.
     *
     * @param str string for which will be removed escape sequence 
     * for quotes.
     * @return String without escape sequence for quotes.
     */
    protected String removeEscapes(final String str) {
        String removed = str.replaceAll("\\\\\"", "\"");
        removed = removed.replaceAll("\\\\\'", "'");

        return removed;
    }
}
