/**
 * This Java file was created 28.11.2013 6:57:19.
 */
package system.turboshell.command.parser;

import system.automaton.ErrorState;
import system.automaton.State;

/**
 * Class {@code OutputAppendStateListener} is listener for {@code OA} state in finite 
 * automaton for commands.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.2
 * 
 * @since 0.6.0
 */
public class OutputAppendStateListener extends CommandsStateAdapter {
    /**
     * Action that is being invoked in case of using transition to leave owner 
     * of this listener and entry into specified state. It tells commands parser 
     * that next parameter will be appended output.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void leavingState(final State targetState, final char character) {
        if (this.getState() != targetState && !(targetState instanceof ErrorState)) {
            this.getParser().setAppend();
        }
    }
}
