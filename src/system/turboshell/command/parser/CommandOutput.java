/**
 * This Java file was created 28.11.2013 6:47:19.
 */
package system.turboshell.command.parser;

/**
 * Class {@code CommandOutput} represents output file for command.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.2
 * 
 * @since 0.6.0
 */
public class CommandOutput {
    /** Path for output file. */
    private String output;
    /** Information whether will be data appended into output. */
    private boolean append;

    /**
     * Constructs output for command.
     *
     * @param output path for output file.
     * @param append information whether will be data appended into output.
     */
    public CommandOutput(final String output, final boolean append) {
        this.output = output;
        this.append = append;
    }

    /**
     * Returns path for output file.
     *
     * @return Path for output file.
     */
    public String getOutput() {
        return this.output;
    }

    /**
     * Returns information whether will be data appended into output.
     *
     * @return Information whether will be data appended into output.
     */
    public boolean isAppend() {
        return this.append;
    }

    /**
     * Sets whether will be data appended into output.
     *
     * @param append information whether will be data appended into output.
     */
    public void setAppend(final boolean append) {
        this.append = append;
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link java.util.HashMap}.
     *
     * @return A hash code value for this object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + (this.append ? 1231 : 1237);
        result = prime * result + ((this.output == null) ? 0 : this.output.hashCode());

        return result;
    }

    /**
     * Indicates whether some object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; 
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof CommandOutput)) {
            return false;
        }

        CommandOutput other = (CommandOutput) obj;
        if (this.append != other.append) {
            return false;
        }

        if (this.output == null) {
            if (other.output != null) {
                return false;
            }
        }
        else if (!this.output.equals(other.output)) {
            return false;
        }

        return true;
    }

    /**
     * Returns string value of command output that has the format:
     * 
     * <p>For append:</p>
     * <pre>
     *     >> [output]
     * </pre>
     * 
     * <p>Otherwise:</p>
     * <pre>
     *     > [output]
     * </pre>
     *
     * @return String value of command output.
     */
    @Override
    public String toString() {
        String str = ">";
        if (this.append) {
            str = ">>";
        }

        return str + " " + this.output;
    }
}
