/**
 * This Java file was created 4.12.2013 4:48:19.
 */
package system.turboshell.command.parser;

import system.automaton.State;

/**
 * Class {@code BackgroundStateListener} is listener for {@code BG} state in finite 
 * automaton for commands.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.2
 * 
 * @since 0.7.7
 */
public class BackgroundStateListener extends CommandsStateAdapter {
    /**
     * Action that is being invoked in case of using transition to entry 
     * into owner of this listener from specified state. It tells commands 
     * parser that all commands will run in background.
     *
     * @param sourceState state from which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void entryState(final State sourceState, final char character) {
        this.getParser().setBackground();
    }
}
