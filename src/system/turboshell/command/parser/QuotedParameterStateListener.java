/**
 * This Java file was created 25.10.2013 15:38:02.
 */
package system.turboshell.command.parser;

import system.automaton.ErrorState;
import system.automaton.State;

/**
 * Class {@code QuotedParameterStateListener} is listener for {@code DQP} and 
 * {@code SQP} states in finite automaton for commands.
 *
 * @author Mr.FrAnTA (Michal Dékány)
 * @version 1.4
 * 
 * @since 0.1.8
 */
public class QuotedParameterStateListener extends CommandsStateAdapter {
    /** Buffer for loading command. */
    private StringBuffer parameter;
    /** Information whether command contains escape sequence. */
    private boolean escape;

    /**
     * Constructs listener.
     */
    public QuotedParameterStateListener() {
        this.parameter = new StringBuffer();
        this.escape = false;
    }

    /**
     * Action that is being invoked in case of using transition to entry 
     * into owner of this listener from specified state. It appends 
     * character into buffer for loading parameter. If parameter contained 
     * escape sequence, it appends '\' before character into buffer.
     *
     * @param sourceState state from which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void entryState(final State sourceState, final char character) {
        if (this.escape) {
            this.parameter.append('\\');
            this.parameter.append(character);
            this.escape = false;
        }

        if (sourceState == this.getState()) {
            this.parameter.append(character);
        }
    }

    /**
     * Action that is being invoked in case of using transition to leave owner 
     * of this listener and entry into specified state. If the character is 
     * equal '\' than this method do nothing. Otherwise adds parameter into 
     * commands parser.
     *
     * @param targetState target state to which the transition leads.
     * @param character character used to the transition.
     */
    @Override
    public void leavingState(final State targetState, final char character) {
        if (character == '\\') {
            this.escape = true;
            return;
        }

        if (targetState != this.getState() && !(targetState instanceof ErrorState)) {
            String param = this.removeEscapes(this.parameter.toString());
            this.parameter.setLength(0);

            this.getParser().addParameter(param);
        }
    }
}
