NAME
	echo - display a line of text

SYNOPSIS
	echo [OPTION]... [STRING]...

DESCRIPTION
	Echo the STRING(s) to standard output.

	-n
		do not output the trailing newline

	-e
		enable interpretation of backslash escapes

	-E
		disable interpretation of backslash escapes (default)

	--help
		display this help and exit


	If -e is in effect, the following sequences are recognized:

	\(0)NNN  the character whose ASCII code is NNN is 000-177 (octal)

	\\     backslash
	\b     backspace
	\f     form feed
	\n     new line
	\r     carriage return
	\t     horizontal tab

AUTHORS
	Written by Petr Kopač and Michal Dékány.