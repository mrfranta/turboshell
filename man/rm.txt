NAME
	rm - remove files or directories

SYNOPSIS
	rm [OPTION]... FILE... removes files

DESCRIPTION
	rm removes each specified file. By default, it does not remove directories.

	If the -I or --interactive=once option is given, and there are more than one file or the -r, -R, or --recursive are given, then rm prompts the user for whether to proceed with the entire operation. If the response is not affirmative, the entire command is aborted.

	Otherwise, if a file is unwritable, and the -f or --force option is not given, or the -i or --interactive=always option is given, rm prompts the user for whether to remove the file. If the response is not affirmative, the file is skipped.

OPTIONS
	Remove the FILE(s).

	-f, --force
		ignore nonexistent files, never prompt

	-i
		prompt before every removal

	-I
		prompt once before removing more than one file, or when removing recursively. Less intrusive than -i, while still giving protection against most mistakes

	--interactive[=WHEN]
		prompt according to WHEN: never, once (-I), or always (-i). Without WHEN, prompt always

	-r, -R, --recursive
		remove directories and their contents recursively

	-v, --verbose
		explain what is being done

	--help
		display this help and exit

	--version
		output version information and exit

	By default, rm does not remove directories. Use the --recursive (-r or -R) option to remove each listed directory, too, along with all of its contents.

	Note that if you use rm to remove a file, it might be possible to recover some of its contents, given sufficient expertise and/or time. For greater assurance that the contents are truly unrecoverable, consider using shred.

AUTHOR
	Written by Michal Dékány.