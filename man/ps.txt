NAME
	ps - report a snapshot of the current processes.

SYNOPSIS
	ps [options]

DESCRIPTION
	-p, --sort-pid
		sorts the processes according to their PIDs
		
	-n, --sort-name
		sorts the processes according to their namess

	ps displays information about a selection of the active processes.

AUTHOR
	Written by Petr Kopač.