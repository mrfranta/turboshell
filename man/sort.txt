NAME
	sort - sort lines of text files

SYNOPSIS
	sort [OPTION]... [FILE]...

DESCRIPTION
	Write sorted concatenation of all FILE(s) to standard output.
	
	Mandatory arguments to long options are mandatory for short options too. Ordering options:

	-b, --ignore-leading-blanks
		ignore leading blanks

	-f, --ignore-case
		fold lower case to upper case characters

	-r, --reverse
		reverse the result of comparisons

	--help 
		display this help and exit

	With no FILE, or when FILE is -, read standard input.

AUTHOR
	Written by Petr Kopač.