NAME
	kill - kill process

SYNOPSIS
	kill PID...

DESCRIPTION
	Kill processes with entered PID's.

AUTHOR
	Written by Michal Dékány.