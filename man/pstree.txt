NAME
	pstree - display a tree of processes

SYNOPSIS
	pstree [OPTION]... [PID]

DESCRIPTION
	pstree shows (running) processes as a tree. The tree is rooted at either pid or vmm (init) if PID is omitted.

OPTIONS
	-a, --show-all
		Show all processes in process tree. It may contains FINISHED processes. Also show process status (CREATED, READY, RUNNING or FINISHED) in parentheses after each process name.

	-p, --show-pid
		Show PIDs. PIDs are shown as decimal numbers in parentheses after each process name.

	-n, --sort-pid
		Sort processes with the same ancestor by PID instead of by name. (Numeric sort.)

	-s, --show-status
		Show process status (CREATED, READY, RUNNING or FINISHED) in parentheses after each process name.

	--help
		Display this help and exit

	--version
		Output version information and exit

AUTHOR
	Written Michal Dékány.